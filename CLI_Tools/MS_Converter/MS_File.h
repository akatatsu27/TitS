#ifndef MS_FILE_H
#define MS_FILE_H

#include "baseTypes.h"

typedef enum MS_AI_TYPE : u16
{
	MS_AI_TYPE_Zero = 0,
	MS_AI_TYPE_One = 1,
	MS_AI_TYPE_Two = 2,
	MS_AI_TYPE_Ten = 10,
	MS_AI_TYPE_Thirteen = 13,
	MS_AI_TYPE_Fourteen = 14,
} MS_AI_TYPE;

typedef enum MS_GENDER : u8
{
	MS_GENDER_Female = 0,
	MS_GENDER_Male = 1,
} MS_GENDER;

typedef enum STATUS_RESISTANCE : u32
{
	STATUS_RESISTANCE_NONE =	   0b0000_0000_0000_0000_0000_0000_0000_0000,
	STATUS_RESISTANCE_POISON =	   0b0000_0000_0000_0000_0000_0000_0000_0001,
	STATUS_RESISTANCE_FREEZE =	   0b0000_0000_0000_0000_0000_0000_0000_0010,
	STATUS_RESISTANCE_PETRIFY =	   0b0000_0000_0000_0000_0000_0000_0000_0100,
	STATUS_RESISTANCE_SLEEP =	   0b0000_0000_0000_0000_0000_0000_0000_1000,
	STATUS_RESISTANCE_MUTE =	   0b0000_0000_0000_0000_0000_0000_0001_0000,
	STATUS_RESISTANCE_BLIND =	   0b0000_0000_0000_0000_0000_0000_0010_0000,
	STATUS_RESISTANCE_SEAL =	   0b0000_0000_0000_0000_0000_0000_0100_0000,
	STATUS_RESISTANCE_CONFUSE =	   0b0000_0000_0000_0000_0000_0000_1000_0000,
	STATUS_RESISTANCE_FAINT =	   0b0000_0000_0000_0000_0000_0001_0000_0000,
	STATUS_RESISTANCE_DEATHBLOW =  0b0000_0000_0000_0000_0000_0010_0000_0000,
	STATUS_RESISTANCE_400 =		   0b0000_0000_0000_0000_0000_0100_0000_0000,
	STATUS_RESISTANCE_RAGE =	   0b0000_0000_0000_0000_0000_1000_0000_0000,
	STATUS_RESISTANCE_1000 =	   0b0000_0000_0000_0000_0001_0000_0000_0000,
	STATUS_RESISTANCE_2000 =	   0b0000_0000_0000_0000_0010_0000_0000_0000,
	STATUS_RESISTANCE_4000 =	   0b0000_0000_0000_0000_0100_0000_0000_0000,
	STATUS_RESISTANCE_MOV =		   0b0000_0000_0000_0000_1000_0000_0000_0000,
	STATUS_RESISTANCE_10_000 =	   0b0000_0000_0000_0001_0000_0000_0000_0000,
	STATUS_RESISTANCE_STR =		   0b0000_0000_0000_0010_0000_0000_0000_0000,
	STATUS_RESISTANCE__40_000 =	   0b0000_0000_0000_0100_0000_0000_0000_0000,
	STATUS_RESISTANCE_DEF =		   0b0000_0000_0000_1000_0000_0000_0000_0000,
	STATUS_RESISTANCE_100_000 =	   0b0000_0000_0001_0000_0000_0000_0000_0000,
	STATUS_RESISTANCE_SPD =		   0b0000_0000_0010_0000_0000_0000_0000_0000,
	STATUS_RESISTANCE_400_000 =	   0b0000_0000_0100_0000_0000_0000_0000_0000,
	STATUS_RESISTANCE_ADF =		   0b0000_0000_1000_0000_0000_0000_0000_0000,
	STATUS_RESISTANCE_1_000_000  = 0b0000_0001_0000_0000_0000_0000_0000_0000,
	STATUS_RESISTANCE_2_000_000  = 0b0000_0010_0000_0000_0000_0000_0000_0000,
	STATUS_RESISTANCE_4_000_000  = 0b0000_0100_0000_0000_0000_0000_0000_0000,
	STATUS_RESISTANCE_8_000_000  = 0b0000_1000_0000_0000_0000_0000_0000_0000,
	STATUS_RESISTANCE_10_000_000 = 0b0001_0000_0000_0000_0000_0000_0000_0000,
	STATUS_RESISTANCE_20_000_000 = 0b0010_0000_0000_0000_0000_0000_0000_0000,
	STATUS_RESISTANCE_40_000_000 = 0b0100_0000_0000_0000_0000_0000_0000_0000,
	STATUS_RESISTANCE_80_000_000 = 0b1000_0000_0000_0000_0000_0000_0000_0000,
} STATUS_RESISTANCE;

typedef enum AbilityCondition : u8
{
	ABILLITY_CONDITION_NULL = 0,
	ABILLITY_CONDITION_ATTACK1 = 1,
	ABILLITY_CONDITION_ATTACK2 = 2,
	ABILLITY_CONDITION_PINCH = 3,
	ABILLITY_CONDITION_ATTACK3 = 4,
	ABILLITY_CONDITION_6 = 6,
	ABILLITY_CONDITION_7 = 7,
	ABILLITY_CONDITION_8 = 8,
	ABILLITY_CONDITION_10 = 10,
	ABILLITY_CONDITION_DEATH = 11,
	ABILLITY_CONDITION_HEAL = 13,
	ABILLITY_CONDITION_REVIVE = 14,
	ABILLITY_CONDITION_IMPEDE = 15,
	ABILLITY_CONDITION_CURE = 19,
	ABILLITY_CONDITION_BUFF = 20,
	/// <summary>
	/// Beta Drone - Anti-X Barrier
	/// </summary>
	ABILLITY_CONDITION_21 = 21,
	/// <summary>
	/// Reverie 2 - Functional Decline
	/// </summary>
	ABILLITY_CONDITION_HP_LIMIT = 22,
	/// <summary>
	/// Reverie 2 - Cooling
	/// </summary>
	ABILLITY_CONDITION_AFTER_SKILL = 23,
	ABILLITY_CONDITION_24 = 24,
	/// <summary>
	/// When targeted by arts?
	/// </summary>
	ABILLITY_CONDITION_25 = 25,
	/// <summary>
	/// Weissman undefined skill
	/// </summary>
	ABILLITY_CONDITION_26 = 26,
	/// <summary>
	/// Angel SP absorb
	/// </summary>
	ABILLITY_CONDITION_27 = 27,
} AbilityCondition;

typedef enum AbilityAITarget : u8
{
	ABILITY_AI_TARGET_0 = 0,
	ABILITY_AI_TARGET_SELF = 1,
	ABILITY_AI_TARGET_FEMALE = 2,
	ABILITY_AI_TARGET_MALE = 3,
	/// <summary>
	/// According to Raven description
	/// </summary>
	ABILITY_AI_TARGET_WEAKEST = 5,
	ABILITY_AI_TARGET_7 = 7,
	ABILITY_AI_TARGET_FOE = 8,
	ABILITY_AI_TARGET_9 = 9,
	ABILITY_AI_TARGET_10 = 10,
	ABILITY_AI_TARGET_CASTING = 11,
	ABILITY_AI_TARGET_12 = 12,
	ABILITY_AI_TARGET_ALLY = 13,
	ABILITY_AI_TARGET_SURROUNDING = 14,
	ABILITY_AI_TARGET_REVERIE_LASER = 15,
	ABILITY_AI_TARGET_DEAD_ALLY = 17,
	ABILITY_AI_TARGET_ALL = 18,
	ABILITY_AI_TARGET_CURE = 19,

	/// <summary>
	/// Angel
	/// </summary>
	ABILITYAITARGET_20 = 20,
	/// <summary>
	/// Ragnard and Dragion
	/// </summary>
	ABILITYAITARGET_21 = 21,
	/// <summary>
	/// Ragnard, combined with target 16
	/// </summary>
	ABILITY_AI_TARGET_22 = 22,
	ABILITY_AI_TARGET_23 = 23,
	ABILITY_AI_TARGET_24 = 24,
	ABILITY_AI_TARGET_25 = 25,
	ABILITY_AI_TARGET_26 = 26,
	ABILITY_AI_TARGET_255 = 255,
} AbilityAITarget;

typedef struct AbilityAI
{
	AbilityCondition Condition;
	u8 Probability;
	AbilityAITarget Target;
	u8 TargetCondition;
	u8 ArtChant_AS_Effect_Index;
	u8 AS_Effect_Index;
	/// <summary>
	/// Values below 1000 refer to abilities in T_MAGIC, and values starting from 1000 refer to abilities in the MS file
	/// </summary>
	u16 AbilityDataIndex;
	u16 Unk_08;
	u16 Unk_0A;
	u32 Unk_0C;
	u16 Unk_10;
	u16 Unk_12;
	u32 Unk_14;
} AbilityAI;

typedef struct MS_FixedData
{
	u16 AS_Index;
	u16 AS_dir;
	u16 Level;
	u32 HPMax;
	u32 HPInitial;
	u16 EPMax;
	u16 EPInitial;
	u16 CPMax;
	u16 CPInitial;
	u16 SPD;
	u16 MoveSpd;
	u16 MOV;
	u16 STR;
	u16 DEF;
	u16 ATS;
	u16 ADF;
	u16 DEX;
	u16 AGL;
	u16 RNG;
	u16 Unk_2A;
	u16 EXP;
	u16 Unk_2E;
	MS_AI_TYPE AI_Type;
	u8 Unk_32[6];
	u8 Flags;
	u8 DeathFlags;
	u8 UnderAttackFlags;
	u8 Unk_3B[5];
	MS_GENDER Gender;
	u8 Unk_41[9];
	u16 ActorSize;
	u8 Unk_4C[0xA];
	u16 SymbolTextureFileIndex;
	u16 SymbolTextureFileDir;
	STATUS_RESISTANCE StatusResistance;
	u8 Unk_5E[0xB];
	u16 EarthElementalEfficacy;
	u16 WaterElementalEfficacy;
	u16 FireElementalEfficacy;
	u16 WindElementalEfficacy;
	u16 TimeElementalEfficacy;
	u16 SpaceElementalEfficacy;
	u16 MirageElementalEfficacy;
	u8 EarthSepith;
	u8 WaterSepith;
	u8 FireSepith;
	u8 WindSepith;
	u8 TimeSepith;
	u8 SpaceSepith;
	u8 MirageSepith;
	u8 Unk_7E[6];
	u16 Equip[5];
	u16 Orb[4];
	AbilityAI NormalAttack;
	u8 ZeroPadding[8];
} MS_FixedData;

typedef struct AbilityAITable
{
	u8 Count;
	AbilityAI Entries[];
	
} AbilityAITable;

typedef struct MS_AbilityData
{
	u16 ID;
	AbilityFlagEnum AbilityFlags;
	ElementEnum Element;
	AbilityTarget Target;
	EffectEnum Effect1;
	EffectEnum Effect2;
	u16 TargetParam1;
	u16 TargetParam2;
	u16 ChantDuration;
	u16 CooldownDuration;
	u16 Cost;
	u16 Unk_12;
	u16 Effect1Power;
	u16 Effect1Duration;
	u16 Effect2Power;
	u16 Effect2Duration;
} MS_AbilityData;

typedef struct MS_AbilityDataTable
{
	u8 Count;
	MS_AbilityData Entries[];
}

typedef struct MS_Name
{
	u8 UnkBeforeName[4];
	/// <summary>
	/// Shift-JIS, null terminated.
	/// </summary>
	u8 Name[];
} MS_Name;

typedef struct MS_Description
{
	/// <summary>
	/// Shift-JIS, null terminated.
	/// </summary>
	u8 Description[];
} MS_Description;

/*MS_FILE LAYOUT
MS_FIXED
AbilityAITable Arts
AbilityAITable Crafts
AbilityAITable SCrafts
MS_AbilityDataTable Abilities
MS_Name
MS_Description
*/

#endif MS_FILE_H