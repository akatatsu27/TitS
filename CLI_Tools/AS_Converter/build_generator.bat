@ECHO OFF

if not exist build\ mkdir build
cl /X /Fobuild\ src\AS_Instruction_gen.c kernel32.lib shell32.lib ^
/I..\..\Layers ^
/I..\..\Layers\Arena ^
/I..\..\Layers\Base ^
/I..\..\Layers\Intrinsics ^
/I..\..\Layers\Math ^
/I..\..\Layers\std ^
/I..\..\Layers\String ^
/I..\..\Layers\thirdparty ^
/I..\..\Layers\thirdparty\metadesk ^
/I..\..\Layers\thirdparty\AMD ^
/I..\..\Layers\thirdparty\INTEL ^
/I..\..\Layers\Windows ^
/I..\..\Layers\Synchronization ^
/Zi /nologo /Gm- /Oi- ^
/GS- /Gs9999999 ^
/link /subsystem:console /NODEFAULTLIB /INCREMENTAL:NO /STACK:0x100000,0x100000