#line 1 "src\\AS_Converter.c"
#line 1 "C:\\Users\\Harry\\git\\TitS\\Layers\\Windows\\Windows.h"



#line 1 "C:\\Users\\Harry\\git\\TitS\\Layers\\std\\string.h"




#line 1 "C:\\Users\\Harry\\git\\TitS\\Layers\\std\\stddef.h"





typedef unsigned long long size_t;
typedef long long ptrdiff_t;
typedef unsigned short wchar_t;


	
#line 13 "C:\\Users\\Harry\\git\\TitS\\Layers\\std\\stddef.h"
#line 6 "C:\\Users\\Harry\\git\\TitS\\Layers\\std\\string.h"

size_t
strlen(const char *str)
{
        const char *s;

        for (s = str; *s; ++s)
                ;
        return (s - str);
}

void memcpy(void* restrict dest, void* restrict src, size_t srcSize) 
{
	for(size_t i = 0; i < srcSize; i++)
	{
		*((char*)dest + i) = *((char*)src + i);
	}
}

void* memmove(void* dest, const void* src, size_t num)
{
	if(num == 0)
		return dest;
	
	if(*(size_t*)&src < *(size_t*)&dest)
	{
		
		for(size_t i = num; i > 0; i--)
		{
			*((char*)dest + i - 1) = *((char*)src + i - 1);
		}
	}
	else
	{
		
		for(size_t i = 0; i < num; i++)
		{
			*((char*)dest + i) = *((char*)src + i);
		}
	}
	
	return dest;
}

void* memset (void * ptr, int value, size_t num )
{
	for (size_t i = 0; i < num; i++)
	{
		*((unsigned char*)ptr + i) = (unsigned char)value;
	}
	
	return ptr;
}

#line 61 "C:\\Users\\Harry\\git\\TitS\\Layers\\std\\string.h"
#line 5 "C:\\Users\\Harry\\git\\TitS\\Layers\\Windows\\Windows.h"
#line 1 "C:\\Users\\Harry\\git\\TitS\\Layers\\Base\\BASE_Types.h"




#line 1 "C:\\Users\\Harry\\git\\TitS\\Layers\\Base\\../std/stdint.h"




#line 1 "C:\\Users\\Harry\\git\\TitS\\Layers\\Base\\BASE_Misc.h"



#line 1 "C:\\Users\\Harry\\git\\TitS\\Layers\\Base\\BASE_Types.h"












































































#line 5 "C:\\Users\\Harry\\git\\TitS\\Layers\\Base\\BASE_Misc.h"





#pragma section(".rdonly", read)






#line 18 "C:\\Users\\Harry\\git\\TitS\\Layers\\Base\\BASE_Misc.h"

#line 20 "C:\\Users\\Harry\\git\\TitS\\Layers\\Base\\BASE_Misc.h"
#line 6 "C:\\Users\\Harry\\git\\TitS\\Layers\\Base\\../std/stdint.h"

typedef long long intmax_t;
typedef unsigned long long uintmax_t;


typedef signed char        int8_t;
typedef short              int16_t;
typedef int                int32_t;
typedef long long          int64_t;
typedef unsigned char      uint8_t;
typedef unsigned short     uint16_t;
typedef unsigned int       uint32_t;
typedef unsigned long long uint64_t;
typedef long long int64_t;

int static_assert_int8_t[((sizeof(int8_t) == 1))?(1):(-1)];
int static_assert_int16_t[((sizeof(int16_t) == 2))?(1):(-1)];
int static_assert_int32_t[((sizeof(int32_t) == 4))?(1):(-1)];
int static_assert_int64_t[((sizeof(int64_t) == 8))?(1):(-1)];
int static_assert_uint8_t[((sizeof(uint8_t) == 1))?(1):(-1)];
int static_assert_uint16_t[((sizeof(uint16_t) == 2))?(1):(-1)];
int static_assert_uint32_t[((sizeof(uint32_t) == 4))?(1):(-1)];
int static_assert_uint64_t[((sizeof(uint64_t) == 8))?(1):(-1)];


typedef int8_t int_least8_t;
typedef uint8_t uint_least8_t;
typedef int16_t int_least16_t;
typedef uint16_t uint_least16_t;
typedef int32_t int_least32_t;
typedef uint32_t uint_least32_t;
typedef int64_t int_least64_t;
typedef uint64_t uint_least64_t;
typedef int8_t int_fast8_t;
typedef uint8_t uint_fast8_t;
typedef int16_t int_fast16_t;
typedef uint16_t uint_fast16_t;
typedef int32_t int_fast32_t;
typedef uint32_t uint_fast32_t;
typedef int64_t int_fast64_t;
typedef uint64_t uint_fast64_t;
typedef int64_t intptr_t;
typedef uint64_t uintptr_t;

#line 51 "C:\\Users\\Harry\\git\\TitS\\Layers\\Base\\../std/stdint.h"
#line 6 "C:\\Users\\Harry\\git\\TitS\\Layers\\Base\\BASE_Types.h"
























typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef int8_t s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;

typedef u8 BYTE;
typedef BYTE BOOLEAN;

typedef u8 CHAR;
typedef u16 WCHAR;
typedef WCHAR wchar;
typedef u16 WORD;
typedef s16 SHORT;
typedef u16 USHORT;
typedef u32 DWORD;
typedef DWORD* DWORD_PTR;
typedef u32 UINT;
typedef s32 LONG;
typedef u32 ULONG;
typedef DWORD *LPDWORD;
typedef u32 BOOL;
typedef u32 b32;
typedef u64 QWORD;
typedef QWORD ULONGLONG;
typedef s64 LONGLONG;
typedef void* HANDLE;
typedef u64 ULONG_PTR;
typedef  const CHAR *LPCSTR;
typedef const CHAR* LPCTSTR;
typedef const void *LPCVOID;
typedef void* LPVOID;
typedef void* PVOID;
typedef size_t SIZE_T;
typedef WCHAR* LPWSTR;
typedef const WCHAR *LPCWSTR;
typedef WCHAR* LPCWCH;
typedef BOOL* LPBOOL;
typedef CHAR* LPSTR;
typedef const CHAR *LPCCH, *PCCH;
typedef s64 LONG_PTR, *PLONG_PTR;
typedef ULONGLONG DWORDLONG, *PDWORDLONG;

#line 77 "C:\\Users\\Harry\\git\\TitS\\Layers\\Base\\BASE_Types.h"
#line 6 "C:\\Users\\Harry\\git\\TitS\\Layers\\Windows\\Windows.h"




























































































HANDLE  GetStdHandle(
   DWORD nStdHandle
);

typedef union _LARGE_INTEGER {
  struct {
    DWORD LowPart;
    LONG  HighPart;
  } DUMMYSTRUCTNAME;
  struct {
    DWORD LowPart;
    LONG  HighPart;
  } u;
  LONGLONG QuadPart;
} LARGE_INTEGER, *PLARGE_INTEGER;

typedef struct _FILETIME {
  DWORD dwLowDateTime;
  DWORD dwHighDateTime;
} FILETIME, *PFILETIME, *LPFILETIME;

typedef struct _WIN32_FIND_DATAW {
  DWORD    dwFileAttributes;
  FILETIME ftCreationTime;
  FILETIME ftLastAccessTime;
  FILETIME ftLastWriteTime;
  DWORD    nFileSizeHigh;
  DWORD    nFileSizeLow;
  DWORD    dwReserved0;
  DWORD    dwReserved1;
  WCHAR    cFileName[260];
  WCHAR    cAlternateFileName[14];
  DWORD    dwFileType; 
  DWORD    dwCreatorType; 
  WORD     wFinderFlags; 
} WIN32_FIND_DATAW, *PWIN32_FIND_DATAW, *LPWIN32_FIND_DATAW;

DWORD GetLastError();

size_t
strlenW(const wchar_t *str)
{
        const wchar_t *s;
		size_t c = 0;
        for (s = str; *s; ++s)
                c += 2;
        return c;
}

int WideCharToMultiByte(
  UINT                               CodePage,
  DWORD                              dwFlags,
   LPCWCH lpWideCharStr,
  int                                cchWideChar,
  LPSTR                              lpMultiByteStr,
  int                                cbMultiByte,
  LPCCH                              lpDefaultChar,
  LPBOOL                             lpUsedDefaultChar
);

typedef struct _OVERLAPPED {
  ULONG_PTR Internal;
  ULONG_PTR InternalHigh;
  union {
    struct {
      DWORD Offset;
      DWORD OffsetHigh;
    } DUMMYSTRUCTNAME;
    PVOID Pointer;
  } DUMMYUNIONNAME;
  HANDLE    hEvent;
} OVERLAPPED, *LPOVERLAPPED;

typedef struct _SECURITY_ATTRIBUTES {
  DWORD  nLength;
  LPVOID lpSecurityDescriptor;
  BOOL   bInheritHandle;
} SECURITY_ATTRIBUTES, *PSECURITY_ATTRIBUTES, *LPSECURITY_ATTRIBUTES;

HANDLE
CreateFileA(
     LPCSTR lpFileName,
     DWORD dwDesiredAccess,
     DWORD dwShareMode,
     LPSECURITY_ATTRIBUTES lpSecurityAttributes,
     DWORD dwCreationDisposition,
     DWORD dwFlagsAndAttributes,
     HANDLE hTemplateFile
    );

HANDLE CreateFileW(
  LPCWSTR               lpFileName,
  DWORD                 dwDesiredAccess,
  DWORD                 dwShareMode,
  LPSECURITY_ATTRIBUTES lpSecurityAttributes,
  DWORD                 dwCreationDisposition,
  DWORD                 dwFlagsAndAttributes,
  HANDLE                hTemplateFile
);

BOOL GetFileSizeEx(
  HANDLE         hFile,
  PLARGE_INTEGER lpFileSize
);

BOOL
WriteFile(
     HANDLE hFile,
     LPCVOID lpBuffer,
     DWORD nNumberOfBytesToWrite,
     LPDWORD lpNumberOfBytesWritten,
     LPOVERLAPPED lpOverlapped
    );

BOOL ReadFile(
  HANDLE       hFile,
  LPVOID       lpBuffer,
  DWORD        nNumberOfBytesToRead,
  LPDWORD      lpNumberOfBytesRead,
  LPOVERLAPPED lpOverlapped
);

HANDLE FindFirstFileW(
  LPCWSTR            lpFileName,
  LPWIN32_FIND_DATAW lpFindFileData
);

BOOL FindNextFileW(
  HANDLE             hFindFile,
  LPWIN32_FIND_DATAW lpFindFileData
);

BOOL FindClose(
  HANDLE hFindFile
);

LPVOID
VirtualAlloc(
     LPVOID lpAddress,
     SIZE_T dwSize,
     DWORD flAllocationType,
     DWORD flProtect
    );

LPWSTR * CommandLineToArgvW( LPCWSTR lpCmdLine,  int* pNumArgs);

LPWSTR
GetCommandLineW(
    void
    );







                                    

                                    

                                    



typedef struct _COORD {
  SHORT X;
  SHORT Y;
} COORD, *PCOORD;
	
typedef struct _CONSOLE_FONT_INFOEX {
  ULONG cbSize;
  DWORD nFont;
  COORD dwFontSize;
  UINT  FontFamily;
  UINT  FontWeight;
  WCHAR FaceName[32];
} CONSOLE_FONT_INFOEX, *PCONSOLE_FONT_INFOEX;

typedef struct _SYSTEM_INFO {
    union {
        DWORD dwOemId;          
        struct {
            WORD wProcessorArchitecture;
            WORD wReserved;
        } DUMMYSTRUCTNAME;
    } DUMMYUNIONNAME;
    DWORD dwPageSize;
    LPVOID lpMinimumApplicationAddress;
    LPVOID lpMaximumApplicationAddress;
    DWORD_PTR dwActiveProcessorMask;
    DWORD dwNumberOfProcessors;
    DWORD dwProcessorType;
    DWORD dwAllocationGranularity;
    WORD wProcessorLevel;
    WORD wProcessorRevision;
} SYSTEM_INFO, *LPSYSTEM_INFO;

typedef struct _MEMORYSTATUSEX {
    DWORD dwLength;
    DWORD dwMemoryLoad;
    DWORDLONG ullTotalPhys;
    DWORDLONG ullAvailPhys;
    DWORDLONG ullTotalPageFile;
    DWORDLONG ullAvailPageFile;
    DWORDLONG ullTotalVirtual;
    DWORDLONG ullAvailVirtual;
    DWORDLONG ullAvailExtendedVirtual;
} MEMORYSTATUSEX, *LPMEMORYSTATUSEX;





void

GetSystemInfo(
     LPSYSTEM_INFO lpSystemInfo
    );

typedef void (*LPTHREAD_START_ROUTINE)(LPVOID);

HANDLE  CreateThread(
     LPSECURITY_ATTRIBUTES lpThreadAttributes,
         SIZE_T dwStackSize,
         LPTHREAD_START_ROUTINE lpStartAddress,
     LPVOID lpParameter,
         DWORD dwCreationFlags,
    LPDWORD lpThreadId
);



DWORD  WaitForSingleObject(
    HANDLE hHandle,
    DWORD dwMilliseconds
);

DWORD  WaitForMultipleObjects(
    DWORD nCount,
    const HANDLE *lpHandles,
    BOOL bWaitAll,
    DWORD dwMilliseconds
);

HANDLE  CreateMutexA(
    LPSECURITY_ATTRIBUTES lpMutexAttributes,
        BOOL bInitialOwner,
    LPCTSTR lpName
);

typedef struct _IMAGE_TLS_DIRECTORY64 {
    ULONGLONG StartAddressOfRawData;
    ULONGLONG EndAddressOfRawData;
    ULONGLONG AddressOfIndex;         
    ULONGLONG AddressOfCallBacks;     
    DWORD SizeOfZeroFill;
    union {
        DWORD Characteristics;
        struct {
            DWORD Reserved0 : 20;
            DWORD Alignment : 4;
            DWORD Reserved1 : 8;
        } DUMMYSTRUCTNAME;
    } DUMMYUNIONNAME;

} IMAGE_TLS_DIRECTORY64;

typedef IMAGE_TLS_DIRECTORY64 * PIMAGE_TLS_DIRECTORY64;


typedef void
( *PIMAGE_TLS_CALLBACK) (
    PVOID DllHandle,
    DWORD Reason,
    PVOID Reserved
    );

#line 377 "C:\\Users\\Harry\\git\\TitS\\Layers\\Windows\\Windows.h"
#line 2 "src\\AS_Converter.c"

#line 1 "C:\\Users\\Harry\\git\\TitS\\Layers\\std\\stdarg.h"





typedef char* va_list;



    void __cdecl __va_start(va_list* , ...);

    
    



    

	

#line 22 "C:\\Users\\Harry\\git\\TitS\\Layers\\std\\stdarg.h"





#line 28 "C:\\Users\\Harry\\git\\TitS\\Layers\\std\\stdarg.h"
#line 4 "src\\AS_Converter.c"
#line 1 "C:\\Users\\Harry\\git\\TitS\\Layers\\std\\stdio.h"







#line 1 "C:\\Users\\Harry\\git\\TitS\\Layers\\thirdparty\\metadesk\\md_basic_types.h"







#line 9 "C:\\Users\\Harry\\git\\TitS\\Layers\\thirdparty\\metadesk\\md_basic_types.h"




typedef int8_t   MD_i8;
typedef int16_t  MD_i16;
typedef int32_t  MD_i32;
typedef int64_t  MD_i64;
typedef uint8_t  MD_u8;
typedef uint16_t MD_u16;
typedef uint32_t MD_u32;
typedef uint64_t MD_u64;
typedef float    MD_f32;
typedef double   MD_f64;

#line 25 "C:\\Users\\Harry\\git\\TitS\\Layers\\thirdparty\\metadesk\\md_basic_types.h"

typedef MD_i8  MD_b8;
typedef MD_i16 MD_b16;
typedef MD_i32 MD_b32;
typedef MD_i64 MD_b64;



typedef struct MD_String8 MD_String8;
struct MD_String8
{
    MD_u8 *str;
    union
	{
		MD_u64 size;
		MD_u64 length;
		MD_u64 count;
	};
};

typedef struct MD_String16 MD_String16;
struct MD_String16
{
    MD_u16 *str;
    union
	{
		MD_u64 size;
		MD_u64 length;
		MD_u64 count;
	};
};

typedef struct MD_String32 MD_String32;
struct MD_String32
{
    MD_u32 *str;
    MD_u64 size;
};

typedef struct MD_String8Node MD_String8Node;
struct MD_String8Node
{
    MD_String8Node *next;
    MD_String8 string;
};

typedef struct MD_String8List MD_String8List;
struct MD_String8List
{
    MD_u64 node_count;
    MD_u64 total_size;
    MD_String8Node *first;
    MD_String8Node *last;
};

typedef struct MD_StringJoin MD_StringJoin;
struct MD_StringJoin
{
    MD_String8 pre;
    MD_String8 mid;
    MD_String8 post;
};

#line 89 "C:\\Users\\Harry\\git\\TitS\\Layers\\thirdparty\\metadesk\\md_basic_types.h"
#line 9 "C:\\Users\\Harry\\git\\TitS\\Layers\\std\\stdio.h"
#line 1 "C:\\Users\\Harry\\git\\TitS\\Layers\\thirdparty\\metadesk\\md_stb_sprintf.h"





























































































































































#line 159 "C:\\Users\\Harry\\git\\TitS\\Layers\\thirdparty\\metadesk\\md_stb_sprintf.h"



#line 163 "C:\\Users\\Harry\\git\\TitS\\Layers\\thirdparty\\metadesk\\md_stb_sprintf.h"



#line 167 "C:\\Users\\Harry\\git\\TitS\\Layers\\thirdparty\\metadesk\\md_stb_sprintf.h"











#line 179 "C:\\Users\\Harry\\git\\TitS\\Layers\\thirdparty\\metadesk\\md_stb_sprintf.h"
#line 180 "C:\\Users\\Harry\\git\\TitS\\Layers\\thirdparty\\metadesk\\md_stb_sprintf.h"






#line 187 "C:\\Users\\Harry\\git\\TitS\\Layers\\thirdparty\\metadesk\\md_stb_sprintf.h"
typedef char *STBSP_SPRINTFCB(const char *buf, void *user, int len);





 int vsprintf(char *buf, char const *fmt, va_list va);
 int vsnprintf(char *buf, int count, char const *fmt, va_list va);
 int sprintf(char *buf, char const *fmt, ...);
 int snprintf(char *buf, int count, char const *fmt, ...);

 int vsprintfcb(STBSP_SPRINTFCB *callback, void *user, char *buf, char const *fmt, va_list va);
 void set_separators(char comma, char period);

#line 202 "C:\\Users\\Harry\\git\\TitS\\Layers\\thirdparty\\metadesk\\md_stb_sprintf.h"



#line 1 "C:\\Users\\Harry\\git\\TitS\\Layers\\std\\stdlib.h"




#line 1 "C:\\Users\\Harry\\git\\TitS\\Layers\\std\\errno.h"




__declspec(thread) int __errno;












#line 19 "C:\\Users\\Harry\\git\\TitS\\Layers\\std\\errno.h"
#line 6 "C:\\Users\\Harry\\git\\TitS\\Layers\\std\\stdlib.h"



typedef struct __u64double
{
	union
	{
		unsigned long long asU64;
		double asDouble;
	};
} __u64double;

int isdigit(char c)
{
	return (c >= '0' && c <= '9');
}

int iswhitespace(char c)
{
	return c == ' ' || c == '\f' || c == '\n' || c == '\r' || c == '\t' || c == '\v';
}

double atof(const char *s)
{
	__errno = 0;
	double a = 0.0;
	int e = 0;
	char c = *s;
	int outersign = 1;
  
	
	while(iswhitespace(c))
	{
		s += 1;
		c = *s;
	}
  
	
	if(c == '+')
	{
		s += 1;
		c = *s;
	}
	else if (c == '-')
	{
		outersign = -1;
		s += 1;
		c = *s;
	}
	
	
	if (isdigit(c))
	{
		
		while (isdigit(c))
		{
			a = a*10.0 + (c - '0');
			s += 1;
			c = *s;
		}
		
		if (c == '.')
		{
			s += 1;
			c = *s;
			
			while (isdigit(c))
			{
				a = a*10.0 + (c - '0');
				s += 1;
				c = *s;
			
				e = e-1;
			}
		}
		
		if (c == 'e' || c == 'E') 
		{
			int sign = 1;
			int i = 0;
			s += 1;
			c = *s;
			if (c == '+')
			{
				s += 1;
				c = *s;
			}
			else if (c == '-')
			{
				s += 1;
				c = *s;
				sign = -1;
			}
			
			while (isdigit(c))
			{
				i = i*10 + (c - '0');
				s += 1;
				c = *s;
			}
			e += i*sign;
		}
		
		while (e > 0) {
			a *= 10.0;
			e--;
		}
		while (e < 0) {
			a *= 0.1;
			e++;
		}
		
		a = a * outersign;
	}
	else if (c == 'i' || c == 'I')
	{
		
		s += 1;
		c = *s;
		if(c != 'n' || c != 'N')
		{
			__errno = 42;
			return 0;
		}
		s += 1;
		c = *s;
		if(c != 'f' || c != 'f')
		{
			__errno = 42;
			return 0;
		}
		
		if(outersign == 1)
		{
			__u64double inf;
			inf.asU64 = 0x7FF0000000000000;
			return inf.asDouble;
		}
		else
		{
			__u64double inf;
			inf.asU64 = 0xFFF0000000000000;
			return inf.asDouble;
		}
	}
	else if (c == 'n' || c == 'N')
	{
		
		s += 1;
		c = *s;
		if(c != 'a' && c != 'A')
		{
			__errno = 42;
			return 0;
		}
		
		s += 1;
		c = *s;
		if(c != 'n' && c != 'N')
		{
			__errno = 42;
			return 0;
		}
		
		__u64double whydidyouinputnan;
		whydidyouinputnan.asU64 = 0x7FFFFFFFFFFFFFFF;
		
		return whydidyouinputnan.asDouble;
	}
	else
	{
		__errno = 42;
		return 0;
	}
	return a;
}
#line 183 "C:\\Users\\Harry\\git\\TitS\\Layers\\std\\stdlib.h"
#line 206 "C:\\Users\\Harry\\git\\TitS\\Layers\\thirdparty\\metadesk\\md_stb_sprintf.h"










#line 217 "C:\\Users\\Harry\\git\\TitS\\Layers\\thirdparty\\metadesk\\md_stb_sprintf.h"







#line 225 "C:\\Users\\Harry\\git\\TitS\\Layers\\thirdparty\\metadesk\\md_stb_sprintf.h"
#line 226 "C:\\Users\\Harry\\git\\TitS\\Layers\\thirdparty\\metadesk\\md_stb_sprintf.h"




#line 231 "C:\\Users\\Harry\\git\\TitS\\Layers\\thirdparty\\metadesk\\md_stb_sprintf.h"
#line 232 "C:\\Users\\Harry\\git\\TitS\\Layers\\thirdparty\\metadesk\\md_stb_sprintf.h"





#line 238 "C:\\Users\\Harry\\git\\TitS\\Layers\\thirdparty\\metadesk\\md_stb_sprintf.h"



static signed int stbsp__real_to_str(char const **start, unsigned int *len, char *out, signed int *decimal_pos, double value, unsigned int frac_digits);
static signed int stbsp__real_to_parts(signed __int64 *bits, signed int *expo, double value);

#line 245 "C:\\Users\\Harry\\git\\TitS\\Layers\\thirdparty\\metadesk\\md_stb_sprintf.h"

static char stbsp__period = '.';
static char stbsp__comma = ',';
static struct
{
    short temp; 
    char pair[201];
} stbsp__digitpair =
{
    0,
    "00010203040506070809101112131415161718192021222324"
        "25262728293031323334353637383940414243444546474849"
        "50515253545556575859606162636465666768697071727374"
        "75767778798081828384858687888990919293949596979899"
};

 void set_separators(char pcomma, char pperiod)
{
    stbsp__period = pperiod;
    stbsp__comma = pcomma;
}















static void stbsp__lead_sign(unsigned int fl, char *sign)
{
    sign[0] = 0;
    if (fl & 128) {
        sign[0] = 1;
        sign[1] = '-';
    } else if (fl & 4) {
        sign[0] = 1;
        sign[1] = ' ';
    } else if (fl & 2) {
        sign[0] = 1;
        sign[1] = '+';
    }
}

 int vsprintfcb(STBSP_SPRINTFCB *callback, void *user, char *buf, char const *fmt, va_list va)
{
    static char hex[] = "0123456789abcdefxp";
    static char hexu[] = "0123456789ABCDEFXP";
    char *bf;
    char const *f;
    int tlen = 0;
    
    bf = buf;
    f = fmt;
    for (;;) {
        signed int fw, pr, tz;
        unsigned int fl;
        
        


























        
        
        for (;;) {
            while (((unsigned __int64)f) & 3) {
                schk1:
                if (f[0] == '%')
                    goto scandd;
                schk2:
                if (f[0] == 0)
                    goto endfmt;
                { if (callback) { { int len = (int)(bf - buf); if ((len + (1)) >= 512) { tlen += len; if (0 == (bf = buf = callback(buf, user, len))) goto done; } }; } };
                *bf++ = f[0];
                ++f;
            }
            for (;;) {
                
                
                
                unsigned int v, c;
                v = *(unsigned int *)f;
                c = (~v) & 0x80808080;
                if (((v ^ 0x25252525) - 0x01010101) & c)
                    goto schk1;
                if ((v - 0x01010101) & c)
                    goto schk2;
                if (callback)
                    if ((512 - (int)(bf - buf)) < 4)
                    goto schk1;








                {
                    *(unsigned int *)bf = v;
                }
                bf += 4;
                f += 4;
            }
        }
        scandd:
        
        ++f;
        
        
        fw = 0;
        pr = -1;
        fl = 0;
        tz = 0;
        
        
        for (;;) {
            switch (f[0]) {
                
                case '-':
                fl |= 1;
                ++f;
                continue;
                
                case '+':
                fl |= 2;
                ++f;
                continue;
                
                case ' ':
                fl |= 4;
                ++f;
                continue;
                
                case '#':
                fl |= 8;
                ++f;
                continue;
                
                case '\'':
                fl |= 64;
                ++f;
                continue;
                
                case '$':
                if (fl & 256) {
                    if (fl & 2048) {
                        fl |= 4096;
                    } else {
                        fl |= 2048;
                    }
                } else {
                    fl |= 256;
                }
                ++f;
                continue;
                
                case '_':
                fl |= 1024;
                ++f;
                continue;
                
                case '0':
                fl |= 16;
                ++f;
                goto flags_done;
                default: goto flags_done;
            }
        }
        flags_done:
        
        
        if (f[0] == '*') {
            fw = ((sizeof(unsigned int) > sizeof(__int64) || (sizeof(unsigned int) & (sizeof(unsigned int) - 1)) != 0) ? **(unsigned int**)((va += sizeof(__int64)) - sizeof(__int64)) : *(unsigned int* )((va += sizeof(__int64)) - sizeof(__int64)));
            ++f;
        } else {
            while ((f[0] >= '0') && (f[0] <= '9')) {
                fw = fw * 10 + f[0] - '0';
                f++;
            }
        }
        
        if (f[0] == '.') {
            ++f;
            if (f[0] == '*') {
                pr = ((sizeof(unsigned int) > sizeof(__int64) || (sizeof(unsigned int) & (sizeof(unsigned int) - 1)) != 0) ? **(unsigned int**)((va += sizeof(__int64)) - sizeof(__int64)) : *(unsigned int* )((va += sizeof(__int64)) - sizeof(__int64)));
                ++f;
            } else {
                pr = 0;
                while ((f[0] >= '0') && (f[0] <= '9')) {
                    pr = pr * 10 + f[0] - '0';
                    f++;
                }
            }
        }
        
        
        switch (f[0]) {
            
            case 'h':
            fl |= 512;
            ++f;
            if (f[0] == 'h')
                ++f;  
            break;
            
            case 'l':
            fl |= ((sizeof(long) == 8) ? 32 : 0);
            ++f;
            if (f[0] == 'l') {
                fl |= 32;
                ++f;
            }
            break;
            
            case 'j':
            fl |= (sizeof(size_t) == 8) ? 32 : 0;
            ++f;
            break;
            
            case 'z':
            fl |= (sizeof(ptrdiff_t) == 8) ? 32 : 0;
            ++f;
            break;
            case 't':
            fl |= (sizeof(ptrdiff_t) == 8) ? 32 : 0;
            ++f;
            break;
            
            case 'I':
            if ((f[1] == '6') && (f[2] == '4')) {
                fl |= 32;
                f += 3;
            } else if ((f[1] == '3') && (f[2] == '2')) {
                f += 3;
            } else {
                fl |= ((sizeof(void *) == 8) ? 32 : 0);
                ++f;
            }
            break;
            default: break;
        }
        
        
        switch (f[0]) {

            char num[512];
            char lead[8];
            char tail[8];
            char *s;
            char const *h;
            unsigned int l, n, cs;
            unsigned __int64 n64;

            double fv;
#line 531 "C:\\Users\\Harry\\git\\TitS\\Layers\\thirdparty\\metadesk\\md_stb_sprintf.h"
            signed int dp;
            char const *sn;
            
            case 's':
            
            s = ((sizeof(char *) > sizeof(__int64) || (sizeof(char *) & (sizeof(char *) - 1)) != 0) ? **(char ***)((va += sizeof(__int64)) - sizeof(__int64)) : *(char ** )((va += sizeof(__int64)) - sizeof(__int64)));
            if (s == 0)
                s = (char *)"null";
            
            sn = s;
            for (;;) {
                if ((((unsigned __int64)sn) & 3) == 0)
                    break;
                lchk:
                if (sn[0] == 0)
                    goto ld;
                ++sn;
            }
            n = 0xffffffff;
            if (pr >= 0) {
                n = (unsigned int)(sn - s);
                if (n >= (unsigned int)pr)
                    goto ld;
                n = ((unsigned int)(pr - n)) >> 2;
            }
            while (n) {
                unsigned int v = *(unsigned int *)sn;
                if ((v - 0x01010101) & (~v) & 0x80808080UL)
                    goto lchk;
                sn += 4;
                --n;
            }
            goto lchk;
            ld:
            
            l = (unsigned int)(sn - s);
            
            if (l > (unsigned int)pr)
                l = pr;
            lead[0] = 0;
            tail[0] = 0;
            pr = 0;
            dp = 0;
            cs = 0;
            
            goto scopy;
            
            
            
            case 'S': 
            {
                
                MD_String8 str = ((sizeof(MD_String8) > sizeof(__int64) || (sizeof(MD_String8) & (sizeof(MD_String8) - 1)) != 0) ? **(MD_String8**)((va += sizeof(__int64)) - sizeof(__int64)) : *(MD_String8* )((va += sizeof(__int64)) - sizeof(__int64)));
                
                
                s = (char *)str.str;
                sn = (const char *)(str.str + str.size);
                l = (int)str.size;
                
                
                lead[0] = 0;
                tail[0] = 0;
                pr = 0;
                dp = 0;
                cs = 0;
                
                goto scopy;
            }break;
            
            
            
            case 'c': 
            
            s = num + 512 - 1;
            *s = (char)((sizeof(int) > sizeof(__int64) || (sizeof(int) & (sizeof(int) - 1)) != 0) ? **(int**)((va += sizeof(__int64)) - sizeof(__int64)) : *(int* )((va += sizeof(__int64)) - sizeof(__int64)));
            l = 1;
            lead[0] = 0;
            tail[0] = 0;
            pr = 0;
            dp = 0;
            cs = 0;
            goto scopy;
            
            case 'n': 
            {
                int *d = ((sizeof(int *) > sizeof(__int64) || (sizeof(int *) & (sizeof(int *) - 1)) != 0) ? **(int ***)((va += sizeof(__int64)) - sizeof(__int64)) : *(int ** )((va += sizeof(__int64)) - sizeof(__int64)));
                *d = tlen + (int)(bf - buf);
            } break;
            


















            case 'A': 
            case 'a': 
            h = (f[0] == 'A') ? hexu : hex;
            fv = ((sizeof(double) > sizeof(__int64) || (sizeof(double) & (sizeof(double) - 1)) != 0) ? **(double**)((va += sizeof(__int64)) - sizeof(__int64)) : *(double* )((va += sizeof(__int64)) - sizeof(__int64)));
            if (pr == -1)
                pr = 6; 
            
            if (stbsp__real_to_parts((signed __int64 *)&n64, &dp, fv))
                fl |= 128;
            
            s = num + 64;
            
            stbsp__lead_sign(fl, lead);
            
            if (dp == -1023)
                dp = (n64) ? -1022 : 0;
            else
                n64 |= (((unsigned __int64)1) << 52);
            n64 <<= (64 - 56);
            if (pr < 15)
                n64 += ((((unsigned __int64)8) << 56) >> (pr * 4));
            
            




            lead[1 + lead[0]] = '0';
            lead[2 + lead[0]] = 'x';
            lead[0] += 2;
#line 669 "C:\\Users\\Harry\\git\\TitS\\Layers\\thirdparty\\metadesk\\md_stb_sprintf.h"
            *s++ = h[(n64 >> 60) & 15];
            n64 <<= 4;
            if (pr)
                *s++ = stbsp__period;
            sn = s;
            
            
            n = pr;
            if (n > 13)
                n = 13;
            if (pr > (signed int)n)
                tz = pr - n;
            pr = 0;
            while (n--) {
                *s++ = h[(n64 >> 60) & 15];
                n64 <<= 4;
            }
            
            
            tail[1] = h[17];
            if (dp < 0) {
                tail[2] = '-';
                dp = -dp;
            } else
                tail[2] = '+';
            n = (dp >= 1000) ? 6 : ((dp >= 100) ? 5 : ((dp >= 10) ? 4 : 3));
            tail[0] = (char)n;
            for (;;) {
                tail[n] = '0' + dp % 10;
                if (n <= 3)
                    break;
                --n;
                dp /= 10;
            }
            
            dp = (int)(s - sn);
            l = (int)(s - (num + 64));
            s = num + 64;
            cs = 1 + (3 << 24);
            goto scopy;
            
            case 'G': 
            case 'g': 
            h = (f[0] == 'G') ? hexu : hex;
            fv = ((sizeof(double) > sizeof(__int64) || (sizeof(double) & (sizeof(double) - 1)) != 0) ? **(double**)((va += sizeof(__int64)) - sizeof(__int64)) : *(double* )((va += sizeof(__int64)) - sizeof(__int64)));
            if (pr == -1)
                pr = 6;
            else if (pr == 0)
                pr = 1; 
            
            if (stbsp__real_to_str(&sn, &l, num, &dp, fv, (pr - 1) | 0x80000000))
                fl |= 128;
            
            
            n = pr;
            if (l > (unsigned int)pr)
                l = pr;
            while ((l > 1) && (pr) && (sn[l - 1] == '0')) {
                --pr;
                --l;
            }
            
            
            if ((dp <= -4) || (dp > (signed int)n)) {
                if (pr > (signed int)l)
                    pr = l - 1;
                else if (pr)
                    --pr; 
                goto doexpfromg;
            }
            
            if (dp > 0) {
                pr = (dp < (signed int)l) ? l - dp : 0;
            } else {
                pr = -dp + ((pr > (signed int)l) ? (signed int) l : pr);
            }
            goto dofloatfromg;
            
            case 'E': 
            case 'e': 
            h = (f[0] == 'E') ? hexu : hex;
            fv = ((sizeof(double) > sizeof(__int64) || (sizeof(double) & (sizeof(double) - 1)) != 0) ? **(double**)((va += sizeof(__int64)) - sizeof(__int64)) : *(double* )((va += sizeof(__int64)) - sizeof(__int64)));
            if (pr == -1)
                pr = 6; 
            
            if (stbsp__real_to_str(&sn, &l, num, &dp, fv, pr | 0x80000000))
                fl |= 128;
            doexpfromg:
            tail[0] = 0;
            stbsp__lead_sign(fl, lead);
            if (dp == 0x7000) {
                s = (char *)sn;
                cs = 0;
                pr = 0;
                goto scopy;
            }
            s = num + 64;
            
            *s++ = sn[0];
            
            if (pr)
                *s++ = stbsp__period;
            
            
            if ((l - 1) > (unsigned int)pr)
                l = pr + 1;
            for (n = 1; n < l; n++)
                *s++ = sn[n];
            
            tz = pr - (l - 1);
            pr = 0;
            
            tail[1] = h[0xe];
            dp -= 1;
            if (dp < 0) {
                tail[2] = '-';
                dp = -dp;
            } else
                tail[2] = '+';



            n = (dp >= 100) ? 5 : 4;
#line 793 "C:\\Users\\Harry\\git\\TitS\\Layers\\thirdparty\\metadesk\\md_stb_sprintf.h"
            tail[0] = (char)n;
            for (;;) {
                tail[n] = '0' + dp % 10;
                if (n <= 3)
                    break;
                --n;
                dp /= 10;
            }
            cs = 1 + (3 << 24); 
            goto flt_lead;
            
            case 'f': 
            fv = ((sizeof(double) > sizeof(__int64) || (sizeof(double) & (sizeof(double) - 1)) != 0) ? **(double**)((va += sizeof(__int64)) - sizeof(__int64)) : *(double* )((va += sizeof(__int64)) - sizeof(__int64)));
            doafloat:
            
            if (fl & 256) {
                double divisor;
                divisor = 1000.0f;
                if (fl & 2048)
                    divisor = 1024.0;
                while (fl < 0x4000000) {
                    if ((fv < divisor) && (fv > -divisor))
                        break;
                    fv /= divisor;
                    fl += 0x1000000;
                }
            }
            if (pr == -1)
                pr = 6; 
            
            if (stbsp__real_to_str(&sn, &l, num, &dp, fv, pr))
                fl |= 128;
            dofloatfromg:
            tail[0] = 0;
            stbsp__lead_sign(fl, lead);
            if (dp == 0x7000) {
                s = (char *)sn;
                cs = 0;
                pr = 0;
                goto scopy;
            }
            s = num + 64;
            
            
            if (dp <= 0) {
                signed int i;
                
                *s++ = '0';
                if (pr)
                    *s++ = stbsp__period;
                n = -dp;
                if ((signed int)n > pr)
                    n = pr;
                i = n;
                while (i) {
                    if ((((unsigned __int64)s) & 3) == 0)
                        break;
                    *s++ = '0';
                    --i;
                }
                while (i >= 4) {
                    *(unsigned int *)s = 0x30303030;
                    s += 4;
                    i -= 4;
                }
                while (i) {
                    *s++ = '0';
                    --i;
                }
                if ((signed int)(l + n) > pr)
                    l = pr - n;
                i = l;
                while (i) {
                    *s++ = *sn++;
                    --i;
                }
                tz = pr - (n + l);
                cs = 1 + (3 << 24); 
            } else {
                cs = (fl & 64) ? ((600 - (unsigned int)dp) % 3) : 0;
                if ((unsigned int)dp >= l) {
                    
                    n = 0;
                    for (;;) {
                        if ((fl & 64) && (++cs == 4)) {
                            cs = 0;
                            *s++ = stbsp__comma;
                        } else {
                            *s++ = sn[n];
                            ++n;
                            if (n >= l)
                                break;
                        }
                    }
                    if (n < (unsigned int)dp) {
                        n = dp - n;
                        if ((fl & 64) == 0) {
                            while (n) {
                                if ((((unsigned __int64)s) & 3) == 0)
                                    break;
                                *s++ = '0';
                                --n;
                            }
                            while (n >= 4) {
                                *(unsigned int *)s = 0x30303030;
                                s += 4;
                                n -= 4;
                            }
                        }
                        while (n) {
                            if ((fl & 64) && (++cs == 4)) {
                                cs = 0;
                                *s++ = stbsp__comma;
                            } else {
                                *s++ = '0';
                                --n;
                            }
                        }
                    }
                    cs = (int)(s - (num + 64)) + (3 << 24); 
                    if (pr) {
                        *s++ = stbsp__period;
                        tz = pr;
                    }
                } else {
                    
                    n = 0;
                    for (;;) {
                        if ((fl & 64) && (++cs == 4)) {
                            cs = 0;
                            *s++ = stbsp__comma;
                        } else {
                            *s++ = sn[n];
                            ++n;
                            if (n >= (unsigned int)dp)
                                break;
                        }
                    }
                    cs = (int)(s - (num + 64)) + (3 << 24); 
                    if (pr)
                        *s++ = stbsp__period;
                    if ((l - dp) > (unsigned int)pr)
                        l = pr + dp;
                    while (n < l) {
                        *s++ = sn[n];
                        ++n;
                    }
                    tz = pr - (l - dp);
                }
            }
            pr = 0;
            
            
            if (fl & 256) {
                char idx;
                idx = 1;
                if (fl & 1024)
                    idx = 0;
                tail[0] = idx;
                tail[1] = ' ';
                {
                    if (fl >> 24) { 
                        if (fl & 2048)
                            tail[idx + 1] = "_KMGT"[fl >> 24];
                        else
                            tail[idx + 1] = "_kMGT"[fl >> 24];
                        idx++;
                        
                        if (fl & 2048 && !(fl & 4096)) {
                            tail[idx + 1] = 'i';
                            idx++;
                        }
                        tail[0] = idx;
                    }
                }
            };
            
            flt_lead:
            
            l = (unsigned int)(s - (num + 64));
            s = num + 64;
            goto scopy;
#line 976 "C:\\Users\\Harry\\git\\TitS\\Layers\\thirdparty\\metadesk\\md_stb_sprintf.h"
            
            case 'B': 
            case 'b': 
            h = (f[0] == 'B') ? hexu : hex;
            lead[0] = 0;
            if (fl & 8) {
                lead[0] = 2;
                lead[1] = '0';
                lead[2] = h[0xb];
            }
            l = (8 << 4) | (1 << 8);
            goto radixnum;
            
            case 'o': 
            h = hexu;
            lead[0] = 0;
            if (fl & 8) {
                lead[0] = 1;
                lead[1] = '0';
            }
            l = (3 << 4) | (3 << 8);
            goto radixnum;
            
            case 'p': 
            fl |= (sizeof(void *) == 8) ? 32 : 0;
            pr = sizeof(void *) * 2;
            fl &= ~16; 
            
            
            case 'X': 
            case 'x': 
            h = (f[0] == 'X') ? hexu : hex;
            l = (4 << 4) | (4 << 8);
            lead[0] = 0;
            if (fl & 8) {
                lead[0] = 2;
                lead[1] = '0';
                lead[2] = h[16];
            }
            radixnum:
            
            if (fl & 32)
                n64 = ((sizeof(unsigned __int64) > sizeof(__int64) || (sizeof(unsigned __int64) & (sizeof(unsigned __int64) - 1)) != 0) ? **(unsigned __int64**)((va += sizeof(__int64)) - sizeof(__int64)) : *(unsigned __int64* )((va += sizeof(__int64)) - sizeof(__int64)));
            else
                n64 = ((sizeof(unsigned int) > sizeof(__int64) || (sizeof(unsigned int) & (sizeof(unsigned int) - 1)) != 0) ? **(unsigned int**)((va += sizeof(__int64)) - sizeof(__int64)) : *(unsigned int* )((va += sizeof(__int64)) - sizeof(__int64)));
            
            s = num + 512;
            dp = 0;
            
            tail[0] = 0;
            if (n64 == 0) {
                lead[0] = 0;
                if (pr == 0) {
                    l = 0;
                    cs = (((l >> 4) & 15)) << 24;
                    goto scopy;
                }
            }
            
            for (;;) {
                *--s = h[n64 & ((1 << (l >> 8)) - 1)];
                n64 >>= (l >> 8);
                if (!((n64) || ((signed int)((num + 512) - s) < pr)))
                    break;
                if (fl & 64) {
                    ++l;
                    if ((l & 15) == ((l >> 4) & 15)) {
                        l &= ~15;
                        *--s = stbsp__comma;
                    }
                }
            };
            
            cs = (unsigned int)((num + 512) - s) + ((((l >> 4) & 15)) << 24);
            
            l = (unsigned int)((num + 512) - s);
            
            goto scopy;
            
            case 'u': 
            case 'i':
            case 'd': 
            
            if (fl & 32) {
                signed __int64 i64 = ((sizeof(signed __int64) > sizeof(__int64) || (sizeof(signed __int64) & (sizeof(signed __int64) - 1)) != 0) ? **(signed __int64**)((va += sizeof(__int64)) - sizeof(__int64)) : *(signed __int64* )((va += sizeof(__int64)) - sizeof(__int64)));
                n64 = (unsigned __int64)i64;
                if ((f[0] != 'u') && (i64 < 0)) {
                    n64 = (unsigned __int64)-i64;
                    fl |= 128;
                }
            } else {
                signed int i = ((sizeof(signed int) > sizeof(__int64) || (sizeof(signed int) & (sizeof(signed int) - 1)) != 0) ? **(signed int**)((va += sizeof(__int64)) - sizeof(__int64)) : *(signed int* )((va += sizeof(__int64)) - sizeof(__int64)));
                n64 = (unsigned int)i;
                if ((f[0] != 'u') && (i < 0)) {
                    n64 = (unsigned int)-i;
                    fl |= 128;
                }
            }
            

            if (fl & 256) {
                if (n64 < 1024)
                    pr = 0;
                else if (pr == -1)
                    pr = 1;
                fv = (double)(signed __int64)n64;
                goto doafloat;
            }
#line 1085 "C:\\Users\\Harry\\git\\TitS\\Layers\\thirdparty\\metadesk\\md_stb_sprintf.h"
            
            
            s = num + 512;
            l = 0;
            
            for (;;) {
                
                char *o = s - 8;
                if (n64 >= 100000000) {
                    n = (unsigned int)(n64 % 100000000);
                    n64 /= 100000000;
                } else {
                    n = (unsigned int)n64;
                    n64 = 0;
                }
                if ((fl & 64) == 0) {
                    do {
                        s -= 2;
                        *(unsigned short *)s = *(unsigned short *)&stbsp__digitpair.pair[(n % 100) * 2];
                        n /= 100;
                    } while (n);
                }
                while (n) {
                    if ((fl & 64) && (l++ == 3)) {
                        l = 0;
                        *--s = stbsp__comma;
                        --o;
                    } else {
                        *--s = (char)(n % 10) + '0';
                        n /= 10;
                    }
                }
                if (n64 == 0) {
                    if ((s[0] == '0') && (s != (num + 512)))
                        ++s;
                    break;
                }
                while (s != o)
                    if ((fl & 64) && (l++ == 3)) {
                    l = 0;
                    *--s = stbsp__comma;
                    --o;
                } else {
                    *--s = '0';
                }
            }
            
            tail[0] = 0;
            stbsp__lead_sign(fl, lead);
            
            
            l = (unsigned int)((num + 512) - s);
            if (l == 0) {
                *--s = '0';
                l = 1;
            }
            cs = l + (3 << 24);
            if (pr < 0)
                pr = 0;
            
            scopy:
            
            if (pr < (signed int)l)
                pr = l;
            n = pr + lead[0] + tail[0] + tz;
            if (fw < (signed int)n)
                fw = n;
            fw -= n;
            pr -= l;
            
            
            if ((fl & 1) == 0) {
                if (fl & 16) 
                {
                    pr = (fw > pr) ? fw : pr;
                    fw = 0;
                } else {
                    fl &= ~64; 
                }
            }
            
            
            if (fw + pr) {
                signed int i;
                unsigned int c;
                
                
                if ((fl & 1) == 0)
                    while (fw > 0) {
                    i = fw; if (callback) { int lg = 512 - (int)(bf - buf); if (i > lg) i = lg; };
                    fw -= i;
                    while (i) {
                        if ((((unsigned __int64)bf) & 3) == 0)
                            break;
                        *bf++ = ' ';
                        --i;
                    }
                    while (i >= 4) {
                        *(unsigned int *)bf = 0x20202020;
                        bf += 4;
                        i -= 4;
                    }
                    while (i) {
                        *bf++ = ' ';
                        --i;
                    }
                    { if (callback) { { int len = (int)(bf - buf); if ((len + (1)) >= 512) { tlen += len; if (0 == (bf = buf = callback(buf, user, len))) goto done; } }; } };
                }
                
                
                sn = lead + 1;
                while (lead[0]) {
                    i = lead[0]; if (callback) { int lg = 512 - (int)(bf - buf); if (i > lg) i = lg; };
                    lead[0] -= (char)i;
                    while (i) {
                        *bf++ = *sn++;
                        --i;
                    }
                    { if (callback) { { int len = (int)(bf - buf); if ((len + (1)) >= 512) { tlen += len; if (0 == (bf = buf = callback(buf, user, len))) goto done; } }; } };
                }
                
                
                c = cs >> 24;
                cs &= 0xffffff;
                cs = (fl & 64) ? ((unsigned int)(c - ((pr + cs) % (c + 1)))) : 0;
                while (pr > 0) {
                    i = pr; if (callback) { int lg = 512 - (int)(bf - buf); if (i > lg) i = lg; };
                    pr -= i;
                    if ((fl & 64) == 0) {
                        while (i) {
                            if ((((unsigned __int64)bf) & 3) == 0)
                                break;
                            *bf++ = '0';
                            --i;
                        }
                        while (i >= 4) {
                            *(unsigned int *)bf = 0x30303030;
                            bf += 4;
                            i -= 4;
                        }
                    }
                    while (i) {
                        if ((fl & 64) && (cs++ == c)) {
                            cs = 0;
                            *bf++ = stbsp__comma;
                        } else
                            *bf++ = '0';
                        --i;
                    }
                    { if (callback) { { int len = (int)(bf - buf); if ((len + (1)) >= 512) { tlen += len; if (0 == (bf = buf = callback(buf, user, len))) goto done; } }; } };
                }
            }
            
            
            sn = lead + 1;
            while (lead[0]) {
                signed int i;
                i = lead[0]; if (callback) { int lg = 512 - (int)(bf - buf); if (i > lg) i = lg; };
                lead[0] -= (char)i;
                while (i) {
                    *bf++ = *sn++;
                    --i;
                }
                { if (callback) { { int len = (int)(bf - buf); if ((len + (1)) >= 512) { tlen += len; if (0 == (bf = buf = callback(buf, user, len))) goto done; } }; } };
            }
            
            
            n = l;
            while (n) {
                signed int i;
                i = n; if (callback) { int lg = 512 - (int)(bf - buf); if (i > lg) i = lg; };
                n -= i;
                while (i >= 4) { *(unsigned int volatile *)bf = *(unsigned int volatile *)s; bf += 4; s += 4; i -= 4; }
#line 1263 "C:\\Users\\Harry\\git\\TitS\\Layers\\thirdparty\\metadesk\\md_stb_sprintf.h"
                    while (i) {
                    *bf++ = *s++;
                    --i;
                }
                { if (callback) { { int len = (int)(bf - buf); if ((len + (1)) >= 512) { tlen += len; if (0 == (bf = buf = callback(buf, user, len))) goto done; } }; } };
            }
            
            
            while (tz) {
                signed int i;
                i = tz; if (callback) { int lg = 512 - (int)(bf - buf); if (i > lg) i = lg; };
                tz -= i;
                while (i) {
                    if ((((unsigned __int64)bf) & 3) == 0)
                        break;
                    *bf++ = '0';
                    --i;
                }
                while (i >= 4) {
                    *(unsigned int *)bf = 0x30303030;
                    bf += 4;
                    i -= 4;
                }
                while (i) {
                    *bf++ = '0';
                    --i;
                }
                { if (callback) { { int len = (int)(bf - buf); if ((len + (1)) >= 512) { tlen += len; if (0 == (bf = buf = callback(buf, user, len))) goto done; } }; } };
            }
            
            
            sn = tail + 1;
            while (tail[0]) {
                signed int i;
                i = tail[0]; if (callback) { int lg = 512 - (int)(bf - buf); if (i > lg) i = lg; };
                tail[0] -= (char)i;
                while (i) {
                    *bf++ = *sn++;
                    --i;
                }
                { if (callback) { { int len = (int)(bf - buf); if ((len + (1)) >= 512) { tlen += len; if (0 == (bf = buf = callback(buf, user, len))) goto done; } }; } };
            }
            
            
            if (fl & 1)
                if (fw > 0) {
                while (fw) {
                    signed int i;
                    i = fw; if (callback) { int lg = 512 - (int)(bf - buf); if (i > lg) i = lg; };
                    fw -= i;
                    while (i) {
                        if ((((unsigned __int64)bf) & 3) == 0)
                            break;
                        *bf++ = ' ';
                        --i;
                    }
                    while (i >= 4) {
                        *(unsigned int *)bf = 0x20202020;
                        bf += 4;
                        i -= 4;
                    }
                    while (i--)
                        *bf++ = ' ';
                    { if (callback) { { int len = (int)(bf - buf); if ((len + (1)) >= 512) { tlen += len; if (0 == (bf = buf = callback(buf, user, len))) goto done; } }; } };
                }
            }
            break;
            
            default: 
            s = num + 512 - 1;
            *s = f[0];
            l = 1;
            fw = fl = 0;
            lead[0] = 0;
            tail[0] = 0;
            pr = 0;
            dp = 0;
            cs = 0;
            goto scopy;
        }
        ++f;
    }
    endfmt:
    
    if (!callback)
        *bf = 0;
    else
        { { int len = (int)(bf - buf); if ((len + (512 - 1)) >= 512) { tlen += len; if (0 == (bf = buf = callback(buf, user, len))) goto done; } }; };
    
    done:
    return tlen + (int)(bf - buf);
}




















 int sprintf(char *buf, char const *fmt, ...)
{
    int result;
    va_list va;
    ((void)(__va_start(&va, fmt)));
    result = vsprintfcb(0, 0, buf, fmt, va);
    ((void)(va = (va_list)0));
    return result;
}

typedef struct stbsp__context {
    char *buf;
    int count;
    int length;
    char tmp[512];
} stbsp__context;

static char *stbsp__clamp_callback(const char *buf, void *user, int len)
{
    stbsp__context *c = (stbsp__context *)user;
    c->length += len;
    
    if (len > c->count)
        len = c->count;
    
    if (len) {
        if (buf != c->buf) {
            const char *s, *se;
            char *d;
            d = c->buf;
            s = buf;
            se = buf + len;
            do {
                *d++ = *s++;
            } while (s < se);
        }
        c->buf += len;
        c->count -= len;
    }
    
    if (c->count <= 0)
        return c->tmp;
    return (c->count >= 512) ? c->buf : c->tmp; 
}

static char * stbsp__count_clamp_callback( const char * buf, void * user, int len )
{
    stbsp__context * c = (stbsp__context*)user;
    (void) sizeof(buf);
    
    c->length += len;
    return c->tmp; 
}

 int vsnprintf( char * buf, int count, char const * fmt, va_list va )
{
    stbsp__context c;
    
    if ( (count == 0) && !buf )
    {
        c.length = 0;
        
        vsprintfcb( stbsp__count_clamp_callback, &c, c.tmp, fmt, va );
    }
    else
    {
        int l;
        
        c.buf = buf;
        c.count = count;
        c.length = 0;
        
        vsprintfcb( stbsp__clamp_callback, &c, stbsp__clamp_callback(0,&c,0), fmt, va );
        
        
        l = (int)( c.buf - buf );
        if ( l >= count ) 
            l = count - 1;
        buf[l] = 0;
    }
    
    return c.length;
}

 int snprintf(char *buf, int count, char const *fmt, ...)
{
    int result;
    va_list va;
    ((void)(__va_start(&va, fmt)));
    
    result = vsnprintf(buf, count, fmt, va);
    ((void)(va = (va_list)0));
    
    return result;
}

 int vsprintf(char *buf, char const *fmt, va_list va)
{
    return vsprintfcb(0, 0, buf, fmt, va);
}















static signed int stbsp__real_to_parts(signed __int64 *bits, signed int *expo, double value)
{
    double d;
    signed __int64 b = 0;
    
    
    d = value;
    
    { int cn; for (cn = 0; cn < 8; cn++) ((char *)&b)[cn] = ((char *)&d)[cn]; };
    
    *bits = b & ((((unsigned __int64)1) << 52) - 1);
    *expo = (signed int)(((b >> 52) & 2047) - 1023);
    
    return (signed int)((unsigned __int64) b >> 63);
}

static double const stbsp__bot[23] = {
    1e+000, 1e+001, 1e+002, 1e+003, 1e+004, 1e+005, 1e+006, 1e+007, 1e+008, 1e+009, 1e+010, 1e+011,
    1e+012, 1e+013, 1e+014, 1e+015, 1e+016, 1e+017, 1e+018, 1e+019, 1e+020, 1e+021, 1e+022
};
static double const stbsp__negbot[22] = {
    1e-001, 1e-002, 1e-003, 1e-004, 1e-005, 1e-006, 1e-007, 1e-008, 1e-009, 1e-010, 1e-011,
    1e-012, 1e-013, 1e-014, 1e-015, 1e-016, 1e-017, 1e-018, 1e-019, 1e-020, 1e-021, 1e-022
};
static double const stbsp__negboterr[22] = {
    -5.551115123125783e-018,  -2.0816681711721684e-019, -2.0816681711721686e-020, -4.7921736023859299e-021, -8.1803053914031305e-022, 4.5251888174113741e-023,
    4.5251888174113739e-024,  -2.0922560830128471e-025, -6.2281591457779853e-026, -3.6432197315497743e-027, 6.0503030718060191e-028,  2.0113352370744385e-029,
    -3.0373745563400371e-030, 1.1806906454401013e-032,  -7.7705399876661076e-032, 2.0902213275965398e-033,  -7.1542424054621921e-034, -7.1542424054621926e-035,
    2.4754073164739869e-036,  5.4846728545790429e-037,  9.2462547772103625e-038,  -4.8596774326570872e-039
};
static double const stbsp__top[13] = {
    1e+023, 1e+046, 1e+069, 1e+092, 1e+115, 1e+138, 1e+161, 1e+184, 1e+207, 1e+230, 1e+253, 1e+276, 1e+299
};
static double const stbsp__negtop[13] = {
    1e-023, 1e-046, 1e-069, 1e-092, 1e-115, 1e-138, 1e-161, 1e-184, 1e-207, 1e-230, 1e-253, 1e-276, 1e-299
};
static double const stbsp__toperr[13] = {
    8388608,
    6.8601809640529717e+028,
    -7.253143638152921e+052,
    -4.3377296974619174e+075,
    -1.5559416129466825e+098,
    -3.2841562489204913e+121,
    -3.7745893248228135e+144,
    -1.7356668416969134e+167,
    -3.8893577551088374e+190,
    -9.9566444326005119e+213,
    6.3641293062232429e+236,
    -5.2069140800249813e+259,
    -5.2504760255204387e+282
};
static double const stbsp__negtoperr[13] = {
    3.9565301985100693e-040,  -2.299904345391321e-063,  3.6506201437945798e-086,  1.1875228833981544e-109,
    -5.0644902316928607e-132, -6.7156837247865426e-155, -2.812077463003139e-178,  -5.7778912386589953e-201,
    7.4997100559334532e-224,  -4.6439668915134491e-247, -6.3691100762962136e-270, -9.436808465446358e-293,
    8.0970921678014997e-317
};

























#line 1573 "C:\\Users\\Harry\\git\\TitS\\Layers\\thirdparty\\metadesk\\md_stb_sprintf.h"
static unsigned __int64 const stbsp__powten[20] = {
    1,
    10,
    100,
    1000,
    10000,
    100000,
    1000000,
    10000000,
    100000000,
    1000000000,
    10000000000ULL,
    100000000000ULL,
    1000000000000ULL,
    10000000000000ULL,
    100000000000000ULL,
    1000000000000000ULL,
    10000000000000000ULL,
    100000000000000000ULL,
    1000000000000000000ULL,
    10000000000000000000ULL
};

#line 1597 "C:\\Users\\Harry\\git\\TitS\\Layers\\thirdparty\\metadesk\\md_stb_sprintf.h"








































static void stbsp__raise_to_power10(double *ohi, double *olo, double d, signed int power) 
{
    double ph, pl;
    if ((power >= 0) && (power <= 22)) {
        { double ahi = 0, alo, bhi = 0, blo; signed __int64 bt; ph = d * stbsp__bot[power]; { int cn; for (cn = 0; cn < 8; cn++) ((char *)&bt)[cn] = ((char *)&d)[cn]; }; bt &= ((~(unsigned __int64)0) << 27); { int cn; for (cn = 0; cn < 8; cn++) ((char *)&ahi)[cn] = ((char *)&bt)[cn]; }; alo = d - ahi; { int cn; for (cn = 0; cn < 8; cn++) ((char *)&bt)[cn] = ((char *)&stbsp__bot[power])[cn]; }; bt &= ((~(unsigned __int64)0) << 27); { int cn; for (cn = 0; cn < 8; cn++) ((char *)&bhi)[cn] = ((char *)&bt)[cn]; }; blo = stbsp__bot[power] - bhi; pl = ((ahi * bhi - ph) + ahi * blo + alo * bhi) + alo * blo; };
    } else {
        signed int e, et, eb;
        double p2h, p2l;
        
        e = power;
        if (power < 0)
            e = -e;
        et = (e * 0x2c9) >> 14; 
        if (et > 13)
            et = 13;
        eb = e - (et * 23);
        
        ph = d;
        pl = 0.0;
        if (power < 0) {
            if (eb) {
                --eb;
                { double ahi = 0, alo, bhi = 0, blo; signed __int64 bt; ph = d * stbsp__negbot[eb]; { int cn; for (cn = 0; cn < 8; cn++) ((char *)&bt)[cn] = ((char *)&d)[cn]; }; bt &= ((~(unsigned __int64)0) << 27); { int cn; for (cn = 0; cn < 8; cn++) ((char *)&ahi)[cn] = ((char *)&bt)[cn]; }; alo = d - ahi; { int cn; for (cn = 0; cn < 8; cn++) ((char *)&bt)[cn] = ((char *)&stbsp__negbot[eb])[cn]; }; bt &= ((~(unsigned __int64)0) << 27); { int cn; for (cn = 0; cn < 8; cn++) ((char *)&bhi)[cn] = ((char *)&bt)[cn]; }; blo = stbsp__negbot[eb] - bhi; pl = ((ahi * bhi - ph) + ahi * blo + alo * bhi) + alo * blo; };
                pl = pl + (d * stbsp__negboterr[eb]);;
            }
            if (et) {
                { double s; s = ph + pl; pl = pl - (s - ph); ph = s; };
                --et;
                { double ahi = 0, alo, bhi = 0, blo; signed __int64 bt; p2h = ph * stbsp__negtop[et]; { int cn; for (cn = 0; cn < 8; cn++) ((char *)&bt)[cn] = ((char *)&ph)[cn]; }; bt &= ((~(unsigned __int64)0) << 27); { int cn; for (cn = 0; cn < 8; cn++) ((char *)&ahi)[cn] = ((char *)&bt)[cn]; }; alo = ph - ahi; { int cn; for (cn = 0; cn < 8; cn++) ((char *)&bt)[cn] = ((char *)&stbsp__negtop[et])[cn]; }; bt &= ((~(unsigned __int64)0) << 27); { int cn; for (cn = 0; cn < 8; cn++) ((char *)&bhi)[cn] = ((char *)&bt)[cn]; }; blo = stbsp__negtop[et] - bhi; p2l = ((ahi * bhi - p2h) + ahi * blo + alo * bhi) + alo * blo; };
                p2l = p2l + (ph * stbsp__negtoperr[et] + pl * stbsp__negtop[et]);;
                ph = p2h;
                pl = p2l;
            }
        } else {
            if (eb) {
                e = eb;
                if (eb > 22)
                    eb = 22;
                e -= eb;
                { double ahi = 0, alo, bhi = 0, blo; signed __int64 bt; ph = d * stbsp__bot[eb]; { int cn; for (cn = 0; cn < 8; cn++) ((char *)&bt)[cn] = ((char *)&d)[cn]; }; bt &= ((~(unsigned __int64)0) << 27); { int cn; for (cn = 0; cn < 8; cn++) ((char *)&ahi)[cn] = ((char *)&bt)[cn]; }; alo = d - ahi; { int cn; for (cn = 0; cn < 8; cn++) ((char *)&bt)[cn] = ((char *)&stbsp__bot[eb])[cn]; }; bt &= ((~(unsigned __int64)0) << 27); { int cn; for (cn = 0; cn < 8; cn++) ((char *)&bhi)[cn] = ((char *)&bt)[cn]; }; blo = stbsp__bot[eb] - bhi; pl = ((ahi * bhi - ph) + ahi * blo + alo * bhi) + alo * blo; };
                if (e) {
                    { double s; s = ph + pl; pl = pl - (s - ph); ph = s; };
                    { double ahi = 0, alo, bhi = 0, blo; signed __int64 bt; p2h = ph * stbsp__bot[e]; { int cn; for (cn = 0; cn < 8; cn++) ((char *)&bt)[cn] = ((char *)&ph)[cn]; }; bt &= ((~(unsigned __int64)0) << 27); { int cn; for (cn = 0; cn < 8; cn++) ((char *)&ahi)[cn] = ((char *)&bt)[cn]; }; alo = ph - ahi; { int cn; for (cn = 0; cn < 8; cn++) ((char *)&bt)[cn] = ((char *)&stbsp__bot[e])[cn]; }; bt &= ((~(unsigned __int64)0) << 27); { int cn; for (cn = 0; cn < 8; cn++) ((char *)&bhi)[cn] = ((char *)&bt)[cn]; }; blo = stbsp__bot[e] - bhi; p2l = ((ahi * bhi - p2h) + ahi * blo + alo * bhi) + alo * blo; };
                    p2l = p2l + (stbsp__bot[e] * pl);;
                    ph = p2h;
                    pl = p2l;
                }
            }
            if (et) {
                { double s; s = ph + pl; pl = pl - (s - ph); ph = s; };
                --et;
                { double ahi = 0, alo, bhi = 0, blo; signed __int64 bt; p2h = ph * stbsp__top[et]; { int cn; for (cn = 0; cn < 8; cn++) ((char *)&bt)[cn] = ((char *)&ph)[cn]; }; bt &= ((~(unsigned __int64)0) << 27); { int cn; for (cn = 0; cn < 8; cn++) ((char *)&ahi)[cn] = ((char *)&bt)[cn]; }; alo = ph - ahi; { int cn; for (cn = 0; cn < 8; cn++) ((char *)&bt)[cn] = ((char *)&stbsp__top[et])[cn]; }; bt &= ((~(unsigned __int64)0) << 27); { int cn; for (cn = 0; cn < 8; cn++) ((char *)&bhi)[cn] = ((char *)&bt)[cn]; }; blo = stbsp__top[et] - bhi; p2l = ((ahi * bhi - p2h) + ahi * blo + alo * bhi) + alo * blo; };
                p2l = p2l + (ph * stbsp__toperr[et] + pl * stbsp__top[et]);;
                ph = p2h;
                pl = p2l;
            }
        }
    }
    { double s; s = ph + pl; pl = pl - (s - ph); ph = s; };
    *ohi = ph;
    *olo = pl;
}





static signed int stbsp__real_to_str(char const **start, unsigned int *len, char *out, signed int *decimal_pos, double value, unsigned int frac_digits)
{
    double d;
    signed __int64 bits = 0;
    signed int expo, e, ng, tens;
    
    d = value;
    { int cn; for (cn = 0; cn < 8; cn++) ((char *)&bits)[cn] = ((char *)&d)[cn]; };
    expo = (signed int)((bits >> 52) & 2047);
    ng = (signed int)((unsigned __int64) bits >> 63);
    if (ng)
        d = -d;
    
    if (expo == 2047) 
    {
        *start = (bits & ((((unsigned __int64)1) << 52) - 1)) ? "NaN" : "Inf";
        *decimal_pos = 0x7000;
        *len = 3;
        return ng;
    }
    
    if (expo == 0) 
    {
        if (((unsigned __int64) bits << 1) == 0) 
        {
            *decimal_pos = 1;
            *start = out;
            out[0] = '0';
            *len = 1;
            return ng;
        }
        
        {
            signed __int64 v = ((unsigned __int64)1) << 51;
            while ((bits & v) == 0) {
                --expo;
                v >>= 1;
            }
        }
    }
    
    
    {
        double ph, pl;
        
        
        tens = expo - 1023;
        tens = (tens < 0) ? ((tens * 617) / 2048) : (((tens * 1233) / 4096) + 1);
        
        
        stbsp__raise_to_power10(&ph, &pl, d, 18 - tens);
        
        
        { double ahi = 0, alo, vh, t; bits = (signed __int64)ph; vh = (double)bits; ahi = (ph - vh); t = (ahi - ph); alo = (ph - (ahi - t)) - (vh + t); bits += (signed __int64)(ahi + alo + pl); };
        
        
        if (((unsigned __int64)bits) >= (1000000000000000000ULL))
            ++tens;
    }
    
    
    frac_digits = (frac_digits & 0x80000000) ? ((frac_digits & 0x7ffffff) + 1) : (tens + frac_digits);
    if ((frac_digits < 24)) {
        unsigned int dg = 1;
        if ((unsigned __int64)bits >= stbsp__powten[9])
            dg = 10;
        while ((unsigned __int64)bits >= stbsp__powten[dg]) {
            ++dg;
            if (dg == 20)
                goto noround;
        }
        if (frac_digits < dg) {
            unsigned __int64 r;
            
            e = dg - frac_digits;
            if ((unsigned int)e >= 24)
                goto noround;
            r = stbsp__powten[e];
            bits = bits + (r / 2);
            if ((unsigned __int64)bits >= stbsp__powten[dg])
                ++tens;
            bits /= r;
        }
        noround:;
    }
    
    
    if (bits) {
        unsigned int n;
        for (;;) {
            if (bits <= 0xffffffff)
                break;
            if (bits % 1000)
                goto donez;
            bits /= 1000;
        }
        n = (unsigned int)bits;
        while ((n % 1000) == 0)
            n /= 1000;
        bits = n;
        donez:;
    }
    
    
    out += 64;
    e = 0;
    for (;;) {
        unsigned int n;
        char *o = out - 8;
        
        if (bits >= 100000000) {
            n = (unsigned int)(bits % 100000000);
            bits /= 100000000;
        } else {
            n = (unsigned int)bits;
            bits = 0;
        }
        while (n) {
            out -= 2;
            *(unsigned short *)out = *(unsigned short *)&stbsp__digitpair.pair[(n % 100) * 2];
            n /= 100;
            e += 2;
        }
        if (bits == 0) {
            if ((e) && (out[0] == '0')) {
                ++out;
                --e;
            }
            break;
        }
        while (out != o) {
            *--out = '0';
            ++e;
        }
    }
    
    *decimal_pos = tens;
    *start = out;
    *len = e;
    return ng;
}








#line 1854 "C:\\Users\\Harry\\git\\TitS\\Layers\\thirdparty\\metadesk\\md_stb_sprintf.h"









#line 1864 "C:\\Users\\Harry\\git\\TitS\\Layers\\thirdparty\\metadesk\\md_stb_sprintf.h"










































#line 10 "C:\\Users\\Harry\\git\\TitS\\Layers\\std\\stdio.h"

#line 12 "C:\\Users\\Harry\\git\\TitS\\Layers\\std\\stdio.h"
#line 5 "src\\AS_Converter.c"




#line 1 "C:\\Users\\Harry\\git\\TitS\\Layers\\Synchronization\\SYN.h"





#line 7 "C:\\Users\\Harry\\git\\TitS\\Layers\\Synchronization\\SYN.h"




ULONG _tls_index = 0;



#pragma section(".CRT$XDA",    long, read) 
#pragma section(".CRT$XDZ",    long, read) 

#pragma section(".CRT$XLA",    long, read) 
#pragma section(".CRT$XLC",    long, read) 
#pragma section(".CRT$XLD",    long, read) 
#pragma section(".CRT$XLZ",    long, read) 

#pragma section(".rdata$T",    long, read)

#pragma comment(linker, "/merge:.CRT=.rdata")






#pragma data_seg(".tls")


__declspec(allocate(".tls"))
#line 37 "C:\\Users\\Harry\\git\\TitS\\Layers\\Synchronization\\SYN.h"
char _tls_start = 0;

#pragma data_seg(".tls$ZZZ")


__declspec(allocate(".tls$ZZZ"))
#line 44 "C:\\Users\\Harry\\git\\TitS\\Layers\\Synchronization\\SYN.h"
char _tls_end = 0;

#pragma data_seg()







__declspec(allocate(".CRT$XLA")) PIMAGE_TLS_CALLBACK __xl_a = 0;






__declspec(allocate(".CRT$XLZ")) PIMAGE_TLS_CALLBACK __xl_z = 0;

__declspec(allocate(".rdata$T"))
extern const IMAGE_TLS_DIRECTORY64 _tls_used =
{
        (ULONGLONG) &_tls_start,        
        (ULONGLONG) &_tls_end,          
        (ULONGLONG) &_tls_index,        
        (ULONGLONG) (&__xl_a+1),        
        (ULONG) 0,                      
        (ULONG) 0                       

};

#line 76 "C:\\Users\\Harry\\git\\TitS\\Layers\\Synchronization\\SYN.h"
#line 10 "src\\AS_Converter.c"

#line 1 "C:\\Users\\Harry\\git\\TitS\\Layers\\Arena\\Arena.c"
#line 1 "C:\\Users\\Harry\\git\\TitS\\Layers\\Arena\\Arena.h"






#line 1 "C:\\Users\\Harry\\git\\TitS\\Layers\\Math\\MATH_Basic.h"












#line 14 "C:\\Users\\Harry\\git\\TitS\\Layers\\Math\\MATH_Basic.h"
#line 8 "C:\\Users\\Harry\\git\\TitS\\Layers\\Arena\\Arena.h"

































#line 42 "C:\\Users\\Harry\\git\\TitS\\Layers\\Arena\\Arena.h"



#line 46 "C:\\Users\\Harry\\git\\TitS\\Layers\\Arena\\Arena.h"



#line 50 "C:\\Users\\Harry\\git\\TitS\\Layers\\Arena\\Arena.h"


#line 53 "C:\\Users\\Harry\\git\\TitS\\Layers\\Arena\\Arena.h"



#line 57 "C:\\Users\\Harry\\git\\TitS\\Layers\\Arena\\Arena.h"



#line 61 "C:\\Users\\Harry\\git\\TitS\\Layers\\Arena\\Arena.h"



#line 65 "C:\\Users\\Harry\\git\\TitS\\Layers\\Arena\\Arena.h"



typedef struct Arena Arena;
struct Arena
{
    Arena* prev;
    Arena* current;
    u64 base_pos;
    u64 pos;
    u64 cmt;
    u64 cap;
    u64 align;
};

typedef struct ArenaTemp ArenaTemp;
struct ArenaTemp
{
    Arena *arena;
    u64 pos;
};

static Arena*       ArenaAlloc(void);
static void         ArenaRelease(Arena *arena);

static void*        ArenaPush(Arena *arena, u64 size);
static void         ArenaPutBack(Arena *arena, u64 size);
static void         ArenaSetAlign(Arena *arena, u64 boundary);
static void         ArenaPushAlign(Arena *arena, u64 boundary);
static void         ArenaClear(Arena *arena);


static ArenaTemp ArenaBeginTemp(Arena *arena);
static void         ArenaEndTemp(ArenaTemp temp);

static ArenaTemp GetScratch(Arena **conflicts, u64 count);












#line 114 "C:\\Users\\Harry\\git\\TitS\\Layers\\Arena\\Arena.h"

#line 116 "C:\\Users\\Harry\\git\\TitS\\Layers\\Arena\\Arena.h"
#line 2 "C:\\Users\\Harry\\git\\TitS\\Layers\\Arena\\Arena.c"



static void*
MEM_Reserve(MD_u64 size)
{
    void *result = VirtualAlloc(0, size, 0x00002000, 0x04);
    return(result);
}

static b32
MEM_Commit(void *ptr, MD_u64 size)
{
    MD_b32 result = (VirtualAlloc(ptr, size, 0x00001000, 0x04) != 0);
    return(result);
}

static void
MEM_Decommit(void *ptr, MD_u64 size)
{
    VirtualFree(ptr, size, 0x00004000);
}

static void
MEM_Release(void *ptr, MD_u64 size)
{
    (void)size;
    VirtualFree(ptr, 0, 0x00008000);
}


__declspec(thread) Arena *ARN_thread_scratch_pool[2llu] = {0, 0};

static Arena*
ArenaAlloc__Size(MD_u64 cmt, MD_u64 res)
{
    if (!(64 < cmt && cmt <= res)) { *(volatile u64 *)0 = 0; };
    u64 cmt_clamped = (((cmt)<(res))?(cmt):(res));
    Arena *result = 0;
    void *mem = MEM_Reserve(res);
    if (MEM_Commit(mem, cmt_clamped))
    {
        result = (Arena*)mem;
        result->prev = 0;
        result->current = result;
        result->base_pos = 0;
        result->pos = 64;
        result->cmt = cmt_clamped;
        result->cap = res;
        result->align = 8;
    }
    return(result);
}

static Arena*
ArenaAlloc(void)
{
    Arena *result = ArenaAlloc__Size((64 << 10),
		(64 << 20));
    return(result);
}

static void
ArenaRelease(Arena *arena)
{
    for (Arena *node = arena->current, *prev = 0;
         node != 0;
         node = prev)
    {
        prev = node->prev;
        MEM_Release(node, node->cap);
    }
}

static u64
ArenaGetPos(Arena *arena)
{
    Arena *current = arena->current;
    u64 result = current->base_pos + current->pos;
    return(result);
}

static void*
ArenaPush(Arena *arena, u64 size)
{
    
    Arena *current = arena->current;
    u64 align = arena->align;
    u64 pos = current->pos;
    u64 pos_aligned = (((pos)+((align)-1))&(~((align)-1)));
    u64 new_pos = pos_aligned + size;
    void *result = (u8*)current + pos_aligned;
    current->pos = new_pos;
    
    
    if (new_pos > current->cmt)
    {
        result = 0;
        current->pos = pos;
        
        
        if (new_pos > current->cap)
        {
            Arena *new_arena = 0;
            if (size > ((64 << 20) - 64)/2)
            {
                u64 big_size_unrounded = size + 64;
                u64 big_size = (((big_size_unrounded)+(((4 << 10))-1))&(~(((4 << 10))-1)));
                new_arena = ArenaAlloc__Size(big_size, big_size);
            }
            else
            {
                new_arena = ArenaAlloc();
            }
            
            
            if (new_arena != 0)
            {
                new_arena->base_pos = current->base_pos + current->cap;
                new_arena->prev = current;
                current = new_arena;
                pos_aligned = current->pos;
                new_pos = pos_aligned + size;
            }
        }
        
        
        if (new_pos <= current->cap)
        {
            
            
            if (new_pos > current->cmt)
            {
                u64 new_cmt_unclamped = (((new_pos)+(((64 << 10))-1))&(~(((64 << 10))-1)));
                u64 new_cmt = (((new_cmt_unclamped)<(current->cap))?(new_cmt_unclamped):(current->cap));
                u64 cmt_size = new_cmt - current->cmt;
                if (MEM_Commit((u8*)current + current->cmt, cmt_size))
                {
                    current->cmt = new_cmt;
                }
            }
            
            
            if (new_pos <= current->cmt)
            {
                result = (u8*)current + current->pos;
                current->pos = new_pos;
            }
        }
    }
    
    return(result);
}

static void
ArenaPopTo(Arena *arena, u64 pos)
{
    
    u64 pos_clamped = (((64)>(pos))?(64):(pos));
    (void)pos_clamped;
    {
        Arena *node = arena->current;
        for (Arena *prev = 0;
             node != 0 && node->base_pos >= pos;
             node = prev)
        {
            prev = node->prev;
            MEM_Release(node, node->cap);
        }
        arena->current = node;
    }
    
    
    {
        Arena *current = arena->current;
        u64 local_pos_unclamped = pos - current->base_pos;
        u64 local_pos = (((local_pos_unclamped)>(64))?(local_pos_unclamped):(64));
        current->pos = local_pos;
    }
}

static void
ArenaSetAutoAlign(Arena *arena, u64 align)
{
    arena->align = align;
}

static void
ArenaAbsorb(Arena *arena, Arena *sub_arena)
{
    Arena *current = arena->current;
    u64 base_pos_shift = current->base_pos + current->cap;
    for (Arena *node = sub_arena->current;
         node != 0;
         node = node->prev)
    {
        node->base_pos += base_pos_shift;
    }
    sub_arena->prev = arena->current;
    arena->current = sub_arena->current;
}

static void
ArenaPutBack(Arena *arena, u64 size)
{
    u64 pos = ArenaGetPos(arena);
    u64 new_pos = pos - size;
    u64 new_pos_clamped = (((64)>(new_pos))?(64):(new_pos));
    ArenaPopTo(arena, new_pos_clamped);
}

static void
ArenaSetAlign(Arena *arena, u64 boundary)
{
    ArenaSetAutoAlign(arena, boundary);
}

static void
ArenaPushAlign(Arena *arena, u64 boundary)
{
    u64 pos = ArenaGetPos(arena);
    u64 align_m1 = boundary - 1;
    u64 new_pos_aligned = (pos + align_m1)&(~align_m1);
    if (new_pos_aligned > pos)
    {
        u64 amt = new_pos_aligned - pos;
        (memset(ArenaPush(arena, amt),0,amt));
    }
}

static void
ArenaClear(Arena *arena)
{
    ArenaPopTo(arena, 64);
}

static ArenaTemp
ArenaBeginTemp(Arena *arena)
{
    ArenaTemp result;
    result.arena = arena;
    result.pos   = ArenaGetPos(arena);
    return(result);
}

static void
ArenaEndTemp(ArenaTemp temp)
{
    ArenaPopTo(temp.arena, temp.pos);
}


static Arena*
GetScratchArena(Arena **conflicts, u64 count)
{
    Arena **scratch_pool = ARN_thread_scratch_pool;
    if (scratch_pool[0] == 0)
    {
        Arena **arena_ptr = scratch_pool;
        for (u64 i = 0; i < 2llu; i += 1, arena_ptr += 1)
        {
            *arena_ptr = ArenaAlloc();
        }
    }
    Arena *result = 0;
    Arena **arena_ptr = scratch_pool;
    for (u64 i = 0; i < 2llu; i += 1, arena_ptr += 1)
    {
        Arena *arena = *arena_ptr;
        Arena **conflict_ptr = conflicts;
        for (u32 j = 0; j < count; j += 1, conflict_ptr += 1)
        {
            if (arena == *conflict_ptr)
            {
                arena = 0;
                break;
            }
        }
        if (arena != 0)
        {
            result = arena;
            break;
        }
    }
    return(result);
}

static ArenaTemp
GetScratch(Arena **conflicts, u64 count)
{
    Arena *arena = GetScratchArena(conflicts, count);
    ArenaTemp result = {0};
    if (arena != 0)
    {
        result = ArenaBeginTemp(arena);
    }
    return(result);
}

#line 302 "C:\\Users\\Harry\\git\\TitS\\Layers\\Arena\\Arena.c"
#line 12 "src\\AS_Converter.c"
#line 1 "C:\\Users\\Harry\\git\\TitS\\Layers\\String\\Str.c"
#line 1 "C:\\Users\\Harry\\git\\TitS\\Layers\\String\\Str.h"















#line 1 "C:\\Users\\Harry\\git\\TitS\\Layers\\Math\\math_basic.c"






int poweri (int x, unsigned int y)
{
    int temp;
    if (y == 0)
        return 1;

    temp = poweri (x, y / 2);
    if ((y % 2) == 0)
        return temp * temp;
    else
        return x * temp * temp;
}




float powerf (float x, int y)
{
    float temp;
    if (y == 0)
    return 1;
    temp = powerf (x, y / 2);
    if ((y % 2) == 0) {
        return temp * temp;
    } else {
        if (y > 0)
            return x * temp * temp;
        else
            return (temp * temp) / x;
    }
}




double powerd (double x, int y)
{
    double temp;
    if (y == 0)
    return 1;
    temp = powerd (x, y / 2);
    if ((y % 2) == 0) {
        return temp * temp;
    } else {
        if (y > 0)
            return x * temp * temp;
        else
            return (temp * temp) / x;
    }
}

#line 59 "C:\\Users\\Harry\\git\\TitS\\Layers\\Math\\math_basic.c"
#line 17 "C:\\Users\\Harry\\git\\TitS\\Layers\\String\\Str.h"



#line 21 "C:\\Users\\Harry\\git\\TitS\\Layers\\String\\Str.h"





typedef struct Str8
{
	char* str;
	union 
	{
		u64 size;
		u64 length;
		u64 count;
	};
} Str8;

typedef struct Str16
{
	wchar* str;
	union 
	{
		u64 size;
		u64 length;
		u64 count;
	};
} Str16;

#line 49 "C:\\Users\\Harry\\git\\TitS\\Layers\\String\\Str.h"

typedef Str8 Str8C; 

typedef struct Str8Node
{
	struct Str8Node* next;
	Str8 str;
} Str8Node;

typedef struct Str8CNode
{
	struct Str8CNode* next;
	Str8C str; 
} Str8CNode;


typedef Str16 Str16C; 

typedef struct Str16Node
{
	struct Str16Node* next;
	Str16 str;
} Str16Node;

typedef struct Str16CNode
{
	struct Str16CNode* next;
	Str16C str; 
} Str16CNode;

typedef struct Str8_atoiURes
{
	BOOLEAN success;
	u64 result;
} Str8_atoiURes;

typedef struct Str8_atoiSRes
{
	BOOLEAN success;
	s64 result;
} Str8_atoiSRes;

typedef struct Str8_atofRes
{
	int err;
	double result;
} Str8_atofRes;








int static_assert_string__structs_size_check[(sizeof(Str16) == sizeof(Str16C) && sizeof(Str8) == sizeof(Str8C) && sizeof(Str16) == sizeof(Str8))?(1):(-1)];

static Str8 Str8PushFV(Arena* arena, char *fmt, va_list args);
static Str8 Str8PushF(Arena* arena, char *fmt, ...);
static Str8C Str8CPushFV(Arena* arena, char *fmt, va_list args);
static Str8C Str8CPushF(Arena* arena, char *fmt, ...);
static u8 DigitCharToValue(char c);
static u8 HexDigitCharToValue(char c);
static Str8_atoiURes Str8_atoi_impl(Str8 str);
static Str8_atoiURes Str8_atoiU(Str8 str);
static Str8_atoiSRes Str8_atoiS(Str8 str);
static Str8_atofRes Str8_atof(Str8 str);
static b32 Str8Cmp(Str8 str1, Str8 str2);
static b32 Str16Cmp(Str16 str1, Str16 str2);
static b32 Str8CCmp(Str8C str1, Str8C str2);
static b32 Str16CCmp(Str16C str1, Str16C str2);
static b32 Str16Contains(Str16 str1, Str16 str2);
static b32 Str16StartsWith(Str16 str1, Str16 str2);
static wchar ToUpperW(wchar c);
static wchar ToLowerW(wchar c);
static b32 IsRepeatedWchar(Str16 str, wchar c);
static b32 IsDigitW(wchar c);
static Str16 SkipSubStr16(Str16 str, size_t skip);
static b32 Str16StartsWithCaseInsensitive(Str16 str1, Str16 str2);
static void EnqueueUniqueStr16(Arena* arena, Str16Node** list, Str16 str);
static void EnqueueUniqueStr16C(Arena* arena, Str16CNode** list, Str16C str);
static void EnqueueUniqueStr8(Arena* arena, Str8Node** list, Str8 str);
static void EnqueueUniqueStr8C(Arena* arena, Str8CNode** list, Str8C str);
static size_t Str16LastIndexOf(Str16 str, wchar thisOne);
static size_t Str16CLastIndexOf(Str16C str, WCHAR thisOne);

#line 136 "C:\\Users\\Harry\\git\\TitS\\Layers\\String\\Str.h"
#line 2 "C:\\Users\\Harry\\git\\TitS\\Layers\\String\\Str.c"

static Str8
Str8PushFV(Arena* arena, char *fmt, va_list args)
{
 Str8 result = {0};
 va_list args2;
 ((args2) = (args));
 u64 needed_bytes = vsnprintf(0, 0, fmt, args);
 result.str = (u8*)(ArenaPush((arena), sizeof(u8)*(needed_bytes)));
 result.size = needed_bytes - 1;
 vsnprintf((char*)result.str, needed_bytes, fmt, args2);
 return result;
}

static Str8
Str8PushF(Arena* arena, char *fmt, ...)
{
 Str8 result = {0};
 va_list args;
 ((void)(__va_start(&args, fmt)));
 result = Str8PushFV(arena, fmt, args);
 ((void)(args = (va_list)0));
 return result;
}

static Str8C
Str8CPushFV(Arena* arena, char *fmt, va_list args)
{
 Str8C result = {0};
 va_list args2;
 ((args2) = (args));
 u64 needed_bytes = vsnprintf(0, 0, fmt, args) + 1;
 result.str = (u8*)(ArenaPush((arena), sizeof(u8)*(needed_bytes)));
 result.size = needed_bytes - 1;
 vsnprintf((char*)result.str, needed_bytes, fmt, args2);
 return result;
}

static Str8C
Str8CPushF(Arena* arena, char *fmt, ...)
{
 Str8C result = {0};
 va_list args;
 ((void)(__va_start(&args, fmt)));
 result = Str8CPushFV(arena, fmt, args);
 ((void)(args = (va_list)0));
 return result;
}

static u8 DigitCharToValue(char c)
{
	if(c >= '0' && c <= '9')
		return c - '0';
	else
		return 0xFF;
}

static u8 HexDigitCharToValue(char c)
{
	if(c >= '0' && c <= '9')
		return c - '0';
	else if (c >= 'A' && c <= 'F')
		return c - 'A' + 10;
	else if (c >= 'a' && c <= 'f')
		return c - 'a' + 10;
	else
		return 0xFF;
}
static Str8_atoiURes
Str8_atoi_impl(Str8 str)
{
	const u64 MAXVAL = 0xFFFFFFFFFFFFFFFF;
	size_t biggestDigitIndex = 0;
	
	u64 retVal = 0;	
	enum {DECIMAL, BINARY, HEXADECIMAL} parse_mode = DECIMAL;
	if(str.str[biggestDigitIndex] == '0' && str.length - biggestDigitIndex >= 3)
	{
		switch(str.str[biggestDigitIndex + 1])
		{
		case 'b':
		case 'B':
			parse_mode = BINARY;
			biggestDigitIndex += 2;
			break;
		case 'x':
		case 'X':
			parse_mode = HEXADECIMAL;
			biggestDigitIndex += 2;
			break;
		default:
			break;
		}
	}
	
	size_t i = str.length - 1; 
	size_t curDigitPos = 0; 
	for(; i >= biggestDigitIndex; i -= 1)
	{
		if(str.str[i] == '_' && i != biggestDigitIndex && i != str.length - 1)
			continue;
		
		switch(parse_mode)
		{
		case DECIMAL: {
			u8 digit = DigitCharToValue(str.str[i]);
			if(digit == 0xFF)
				return (Str8_atoiURes){0, 0};
			else
			{
				s64 temp = poweri(10, curDigitPos) * digit; 
				if(temp > MAXVAL - retVal)
					return (Str8_atoiURes){0, 0}; 
				retVal += temp;
				curDigitPos += 1;
			}
			break;}
		case BINARY: {
			if (curDigitPos == 64) 
				return (Str8_atoiURes){0, 0};
			u8 digit = str.str[i] - '0';
			if(digit == 0 || digit == 1)
			{
				retVal += digit << curDigitPos;
				curDigitPos += 1;
			}
			else
			{
				return (Str8_atoiURes){0, 0};
			}
			break;}
		case HEXADECIMAL: {
			u8 digit = HexDigitCharToValue(str.str[i]);
			if(digit == 0xFF)
				return (Str8_atoiURes){0, 0};
			else
			{
				s64 temp = digit << (curDigitPos << 2);
				if(temp > MAXVAL - retVal)
					return (Str8_atoiURes){0, 0}; 
				retVal += temp;
				curDigitPos += 1;
			}
			break;}
		}
	}
	
	return (Str8_atoiURes){1, retVal};
}

static Str8_atoiURes
Str8_atoiU(Str8 str)
{	
	return Str8_atoi_impl(str);
}

static Str8_atoiSRes
Str8_atoiS(Str8 str)
{
	const s64 sMAXVAL = 9223372036854775807;
	if(str.length == 0)
		return (Str8_atoiSRes){0, 0};
	u8 sign = 1;
	size_t biggestDigitIndex = 0;	
	
	if(str.str[0] == '-')
	{
		sign = -1;
		biggestDigitIndex += 1;
	}
	Str8_atoiURes parse = Str8_atoi_impl((Str8){str.str + biggestDigitIndex, str.length - biggestDigitIndex});
	if(parse.success == 0 || parse.result > sMAXVAL)
		return (Str8_atoiSRes){0, 0};
	s64 retVal = (s32)parse.result;
	retVal = retVal * sign;
	return (Str8_atoiSRes){1, retVal};
}

static Str8_atofRes
Str8_atof(Str8 str)
{
	if(str.length = 0)
		return (Str8_atofRes){42, 0};
	char* s = str.str;
	u64 index = 0;
	double a = 0.0;
	int e = 0;
	char c = *s;
	int outersign = 1;
  
	
	









  
	
	if(c == '+')
	{
		index += 1;
		if(index >= str.length)
			return (Str8_atofRes){42, 0};
		c = *(s + index);
	}
	else if (c == '-')
	{
		outersign = -1;
		index += 1;
		if(index >= str.length)
			return (Str8_atofRes){42, 0};
		c = *(s + index);
	}
	
	
	if (isdigit(c))
	{
		
		while (isdigit(c))
		{
			a = a*10.0 + (c - '0');
			index += 1;
			if(index >= str.length)
				return (Str8_atofRes){0, a};
			c = *(s + index);
		}
		
		if (c == '.')
		{
			index += 1;
			if(index >= str.length)
				return (Str8_atofRes){42, 0};
			c = *(s + index);
			
			while (isdigit(c))
			{
				a = a*10.0 + (c - '0');
				index += 1;
				if(index >= str.length)
					return (Str8_atofRes){0, a};
				c = *(s + index);
			
				e = e-1;
			}
		}
		
		if (c == 'e' || c == 'E') 
		{
			int sign = 1;
			int i = 0;
			index += 1;
			if(index >= str.length)
				return (Str8_atofRes){42, 0};
			c = *(s + index);
			if (c == '+')
			{
				index += 1;
				if(index >= str.length)
					return (Str8_atofRes){42, 0};
				c = *(s + index);
			}
			else if (c == '-')
			{
				index += 1;
				if(index >= str.length)
					return (Str8_atofRes){42, 0};
				c = *(s + index);
				sign = -1;
			}
			
			while (isdigit(c))
			{
				i = i*10 + (c - '0');
				index += 1;
				if(index >= str.length)
					break;
				c = *(s + index);
			}
			e += i*sign;
		}
		
		while (e > 0) {
			a *= 10.0;
			e--;
		}
		while (e < 0) {
			a *= 0.1;
			e++;
		}
		
		a = a * outersign;
		
		return (Str8_atofRes){0, a};
	}
	else if (c == 'i' || c == 'I')
	{
		
		index += 1;
		if(index >= str.length)
			return (Str8_atofRes){42, 0};
		c = *(s + index);
		if(c != 'n' || c != 'N')
		{
			return (Str8_atofRes){42, 0};
		}
		index += 1;
		if(index >= str.length)
			return (Str8_atofRes){42, 0};
		c = *(s + index);
		if(c != 'f' || c != 'f')
		{
			return (Str8_atofRes){42, 0};
		}
		
		if(outersign == 1)
		{
			__u64double inf;
			inf.asU64 = 0x7FF0000000000000;
			return (Str8_atofRes){0, inf.asDouble};
		}
		else
		{
			__u64double inf;
			inf.asU64 = 0xFFF0000000000000;
			return (Str8_atofRes){0, inf.asDouble};
		}
	}
	else if (c == 'n' || c == 'N')
	{
		
		index += 1;
		if(index >= str.length)
			return (Str8_atofRes){42, 0};
		c = *(s + index);
		if(c != 'a' && c != 'A')
		{
			return (Str8_atofRes){42, 0};
		}
		
		index += 1;
		if(index >= str.length)
			return (Str8_atofRes){42, 0};
		c = *(s + index);
		if(c != 'n' && c != 'N')
		{
			return (Str8_atofRes){42, 0};
		}
		
		__u64double whydidyouinputnan;
		whydidyouinputnan.asU64 = 0x7FFFFFFFFFFFFFFF;
		
		return (Str8_atofRes){0, whydidyouinputnan.asDouble};
	}
	else
	{
		return (Str8_atofRes){42, 0};
	}
}

static b32
Str8Cmp(Str8 str1, Str8 str2)
{
	if (str1.length != str2.length)
		return 0;

	for (size_t i = 0; i < (str1.length); i++)
	{
		if (str1.str[i] != str2.str[i])
			return 0;
	}

	return 1;
}

static b32
Str16Cmp(Str16 str1, Str16 str2)
{
	if (str1.length != str2.length)
		return 0;

	for (size_t i = 0; i < (str1.length); i++)
	{
		if (str1.str[i] != str2.str[i])
			return 0;
	}

	return 1;
}
static b32
Str8CCmp(Str8C str1, Str8C str2)
{
	return Str8Cmp(*(Str8*)(&str1), *(Str8*)(&str2));
}

static b32
Str16CCmp(Str16C str1, Str16C str2)
{
	return Str16Cmp(*(Str16*)(&str1), *(Str16*)(&str2));
}

static b32
Str16Contains(Str16 str1, Str16 str2)
{
	return 0;
}

static b32
Str16StartsWith(Str16 str1, Str16 str2)
{
	if(str2.length > str1.length)
		return 0;
	
	for (size_t i = 0; i < (str2.length >> 1); i++)
	{
		if(str1.str[i] != str2.str[i])
			return 0;
	}
	
	return 1;
}

static wchar ToUpperW(wchar c)
{
	if(c >= u'a' && c <= u'z')
			return c - 0x20;
	else
		return c;
}

static wchar ToLowerW(wchar c)
{
	if(c >= u'A' && c <= u'Z')
			return c + 0x20;
	else
		return c;
}

static b32 IsRepeatedWchar(Str16 str, wchar c)
{
	for (size_t i = 0; i < str.length; i++)
	{
		if(str.str[i] != c)
			return 0;
	}
	return 1;
}

static b32 IsDigitW(wchar c)
{
	return c >= u'0' && c <= u'9';
}

static Str16 SkipSubStr16(Str16 str, size_t skip)
{
	return (Str16){.str = str.str + skip, .length = str.length - skip};
}

static b32
Str16StartsWithCaseInsensitive(Str16 str1, Str16 str2)
{
	if(str2.length > str1.length)
		return 0;
	
	for(size_t i = 0; i < str2.length; i++)
	{
		wchar cur1 = ToLowerW(str1.str[i]);
		wchar cur2 = ToLowerW(str2.str[i]);
		if(cur1 != cur2)
			return 0;
	}
	return 1;
}

static void EnqueueUniqueStr16(Arena* arena, Str16Node** list, Str16 str)
{
	
	for (Str16Node* node = *list; node != 0; node = node->next)
	{
		if (Str16Cmp(node->str, str))
		{
			return;
		}
	}
	
	Str16Node* last = *list;
	while(1)
	{
		if(last == 0)
		{
			
			Str16Node* node = ArenaPush(arena, sizeof(Str16Node));
			node->str = str;
			node->next = 0;
			*list = node;
			return;
		}
		
		if (Str16Cmp(last->str, str))
		{
			return;			
		}
		
		if(last->next == 0)
		{
			
			Str16Node* node = ArenaPush(arena, sizeof(Str16Node));
			node->str = str;
			node->next = 0;
			last->next = node;
			return;
		}
		else
		{
			last = last->next;
		}
	}
}

static void EnqueueUniqueStr16C(Arena* arena, Str16CNode** list, Str16C str)
{
	EnqueueUniqueStr16(arena, (Str16Node**)list, *(Str16*)(&str));
}

static void EnqueueUniqueStr8(Arena* arena, Str8Node** list, Str8 str)
{
	EnqueueUniqueStr16(arena, (Str16Node**)list, *(Str16*)&str);
}

static void EnqueueUniqueStr8C(Arena* arena, Str8CNode** list, Str8C str)
{
	EnqueueUniqueStr16(arena, (Str16Node**)list, *(Str16*)&str);
}

static size_t Str16LastIndexOf(Str16 str, wchar thisOne)
{
	size_t index = -1;
	for (size_t i = 0; i < (str.length); i++)
	{
		if(str.str[i] == thisOne)
			index = i;
	}
	
	return index;
}

static size_t Str16CLastIndexOf(Str16C str, WCHAR thisOne)
{
	return Str16LastIndexOf(*(Str16*)(&str), thisOne);
}
#line 13 "src\\AS_Converter.c"
#line 1 "C:\\Users\\Harry\\git\\TitS\\Layers\\String\\STR_Unicode.c"
#line 1 "C:\\Users\\Harry\\git\\TitS\\Layers\\String\\STR_Unicode.h"











typedef struct STR_DecodedCodepoint STR_DecodedCodepoint;
struct STR_DecodedCodepoint
{
    u32 codepoint;
    u32 advance;
};

static STR_DecodedCodepoint STR_DecodeCodepointFromUtf8(u8 *str, u64 max);
static STR_DecodedCodepoint STR_DecodeCodepointFromUtf16(u16 *out, u64 max);
static u32 STR_Utf8FromCodepoint(u8 *out, u32 codepoint);
static u32 STR_Utf16FromCodepoint(u16 *out, u32 codepoint);
static Str8 STR_S8FromS16(Arena *arena, Str16 in);
static Str16 STR_S16FromS8(Arena *arena, Str8 in);

static Str16 STR_Str16FromS8(Arena *arena, Str8 in);

#line 29 "C:\\Users\\Harry\\git\\TitS\\Layers\\String\\STR_Unicode.h"
#line 2 "C:\\Users\\Harry\\git\\TitS\\Layers\\String\\STR_Unicode.c"

static u8 STR_utf8_class[32] = {
    1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,2,2,2,2,3,3,4,5,
};

static STR_DecodedCodepoint
STR_DecodeCodepointFromUtf8(u8 *str, u64 max)
{










    
    STR_DecodedCodepoint result = {~((u32)0), 1};
    u8 byte = str[0];
    u8 byte_class = STR_utf8_class[byte >> 3];
    switch (byte_class)
    {
        case 1:
        {
            result.codepoint = byte;
        }break;
        
        case 2:
        {
            if (2 <= max)
            {
                u8 cont_byte = str[1];
                if (STR_utf8_class[cont_byte >> 3] == 0)
                {
                    result.codepoint = (byte & 0x1F) << 6;
                    result.codepoint |=  (cont_byte & 0x3F);
                    result.advance = 2;
                }
            }
        }break;
        
        case 3:
        {
            if (3 <= max)
            {
                u8 cont_byte[2] = {0};
                cont_byte[0] = str[1];
                cont_byte[1] = str[2];
                if (STR_utf8_class[cont_byte[0] >> 3] == 0 &&
                    STR_utf8_class[cont_byte[1] >> 3] == 0)
                {
                    result.codepoint = (byte & 0x0F) << 12;
                    result.codepoint |= ((cont_byte[0] & 0x3F) << 6);
                    result.codepoint |=  (cont_byte[1] & 0x3F);
                    result.advance = 3;
                }
            }
        }break;
        
        case 4:
        {
            if (4 <= max)
            {
                u8 cont_byte[3] = {0};
                cont_byte[0] = str[1];
                cont_byte[1] = str[2];
                cont_byte[2] = str[3];
                if (STR_utf8_class[cont_byte[0] >> 3] == 0 &&
                    STR_utf8_class[cont_byte[1] >> 3] == 0 &&
                    STR_utf8_class[cont_byte[2] >> 3] == 0)
                {
                    result.codepoint = (byte & 0x07) << 18;
                    result.codepoint |= ((cont_byte[0] & 0x3F) << 12);
                    result.codepoint |= ((cont_byte[1] & 0x3F) <<  6);
                    result.codepoint |=  (cont_byte[2] & 0x3F);
                    result.advance = 4;
                }
            }
        }break;
    }
    
    return(result);
}

static STR_DecodedCodepoint
STR_DecodeCodepointFromUtf16(u16 *out, u64 max)
{
    STR_DecodedCodepoint result = {~((u32)0), 1};
    result.codepoint = out[0];
    result.advance = 1;
    if (1 < max && 0xD800 <= out[0] && out[0] < 0xDC00 && 0xDC00 <= out[1] && out[1] < 0xE000)
    {
        result.codepoint = ((out[0] - 0xD800) << 10) | (out[1] - 0xDC00);
        result.advance = 2;
    }
    return(result);
}

static u32
STR_Utf8FromCodepoint(u8 *out, u32 codepoint)
{

    u32 advance = 0;
    if (codepoint <= 0x7F)
    {
        out[0] = (u8)codepoint;
        advance = 1;
    }
    else if (codepoint <= 0x7FF)
    {
        out[0] = (0x03 << 6) | ((codepoint >> 6) & 0x1F);
        out[1] = 0x80 | (codepoint & 0x3F);
        advance = 2;
    }
    else if (codepoint <= 0xFFFF)
    {
        out[0] = (0x07 << 5) | ((codepoint >> 12) & 0x0F);
        out[1] = 0x80 | ((codepoint >> 6) & 0x3F);
        out[2] = 0x80 | ( codepoint       & 0x3F);
        advance = 3;
    }
    else if (codepoint <= 0x10FFFF)
    {
        out[0] = (0x0F << 3) | ((codepoint >> 18) & 0x07);
        out[1] = 0x80 | ((codepoint >> 12) & 0x3F);
        out[2] = 0x80 | ((codepoint >>  6) & 0x3F);
        out[3] = 0x80 | ( codepoint        & 0x3F);
        advance = 4;
    }
    else
    {
        out[0] = '?';
        advance = 1;
    }
    return(advance);
}

static u32
STR_Utf16FromCodepoint(u16 *out, u32 codepoint)
{
    u32 advance = 1;
    if (codepoint == ~((u32)0))
    {
        out[0] = (u16)'?';
    }
    else if (codepoint < 0x10000)
    {
        out[0] = (u16)codepoint;
    }
    else
    {
        u64 v = codepoint - 0x10000;
        out[0] = (u16)(0xD800 + (v >> 10));
        out[1] = 0xDC00 + (v & 0x03FF);
        advance = 2;
    }
    return(advance);
}

static Str8
STR_S8FromS16(Arena *arena, Str16 in)
{
    u64 cap = in.size*3;
    u8 *str = (u8*)((memset((u8*)(ArenaPush((arena), sizeof(u8)*(cap + 1))),0,sizeof(u8)*(cap + 1))));
    u16 *ptr = in.str;
    u16 *opl = ptr + in.size;
    u64 size = 0;
    STR_DecodedCodepoint consume;
    for (;ptr < opl;)
    {
        consume = STR_DecodeCodepointFromUtf16(ptr, opl - ptr);
        ptr += consume.advance;
        size += STR_Utf8FromCodepoint(str + size, consume.codepoint);
    }
    str[size] = 0;
    ArenaPutBack(arena, cap - size); 
    return (Str8){ .str = str, .count = size};
}

static Str16
STR_S16FromS8(Arena *arena, Str8 in)
{
    u64 cap = in.size*2;
    u16 *str = (u16*)((memset((u16*)(ArenaPush((arena), sizeof(u16)*(cap + 1))),0,sizeof(u16)*(cap + 1))));
    u8 *ptr = in.str;
    u8 *opl = ptr + in.size;
    u64 size = 0;
    STR_DecodedCodepoint consume;
    for (;ptr < opl;)
    {
        consume = STR_DecodeCodepointFromUtf8(ptr, opl - ptr);
        ptr += consume.advance;
        size += STR_Utf16FromCodepoint(str + size, consume.codepoint);
    }
    str[size] = 0;
    ArenaPutBack(arena, 2*(cap - size)); 
    Str16 result = {0};
    result.str = str;
    result.size = size;
    return(result);
}
#line 14 "src\\AS_Converter.c"
#line 1 "C:\\Users\\Harry\\git\\TitS\\Layers\\Windows\\WIN_FileFuncs.c"
#line 1 "C:\\Users\\Harry\\git\\TitS\\Layers\\Windows\\WIN_FileFuncs.h"
















#line 1 "C:\\Users\\Harry\\git\\TitS\\Layers\\Windows\\WIN_Logging.h"

















#line 1 "C:\\Users\\Harry\\git\\TitS\\Layers\\Windows\\WIN.h"





#line 7 "C:\\Users\\Harry\\git\\TitS\\Layers\\Windows\\WIN.h"

#line 9 "C:\\Users\\Harry\\git\\TitS\\Layers\\Windows\\WIN.h"
#line 19 "C:\\Users\\Harry\\git\\TitS\\Layers\\Windows\\WIN_Logging.h"

typedef struct LOGHANDLE
{
	HANDLE hFile;
	HANDLE hMutex;
	
} LOGHANDLE;

static LOGHANDLE WIN_CreateLogHandle(HANDLE hFile);
static void WIN_CloseLogHandle(LOGHANDLE logger);
static void WIN_LogStr8(LOGHANDLE logger, char* fmt, ...);

#line 32 "C:\\Users\\Harry\\git\\TitS\\Layers\\Windows\\WIN_Logging.h"
#line 18 "C:\\Users\\Harry\\git\\TitS\\Layers\\Windows\\WIN_FileFuncs.h"



static wchar biggerPaths[] = u"\\\\?\\";
static wchar filesInDirectory[] = u"\\*";


static b32 WIN_CanAddPathPrefix(Str16C path);
static Str16C WIN_SanitizePath(Arena* arena, Str16C path);
static Str16C WIN_DirectorySearchPath(Arena* arena, Str16C path);
static Str16C WIN_DirectoryPlusFile(Arena* arena, Str16C dir, Str16C file);


static b32 WIN_EnumerateFiles(Arena* arena, LOGHANDLE logFile, Str16CNode** list, Str16C argPath);
static Str16C WIN_GetFileNameWithExtension(Str16C filePath);


static Str16C WIN_GetFileExtension(Str16C filePath);
static Str16 WIN_GetFileNameWithoutExtension(Str16C filePath);
static b32 WIN_FileExists(wchar* szPath);
#line 39 "C:\\Users\\Harry\\git\\TitS\\Layers\\Windows\\WIN_FileFuncs.h"
#line 2 "C:\\Users\\Harry\\git\\TitS\\Layers\\Windows\\WIN_FileFuncs.c"


static b32 CanAddPathPrefix(Str16C path)
{
	if (path.length <= 3)
		return 0;

	
	if (path.str[1] == u':' || (path.str[0] == u'\\' && path.str[0] == u'\\'))
	{
		if (path.length > 3 && path.str[2] == u'?') 
			return 0;
		else
			return 1;
	}
	else
		return 0;
}

static Str16C WIN_SanitizePath(Arena* arena, Str16C path)
{	
	if (!CanAddPathPrefix(path))
		return path;

	Str16 bigger = (Str16){ .str = biggerPaths, .length = ((sizeof(biggerPaths)-2) >> 1) };
	
	
	size_t pathBuffSize = (bigger.length * sizeof(wchar)) + (path.length * sizeof(wchar));
	WCHAR* extendedPath = ArenaPush(arena, pathBuffSize);
	memcpy(extendedPath, bigger.str, (bigger.length * sizeof(wchar)));
	memcpy((char*)extendedPath + (bigger.length * sizeof(wchar)), path.str, (path.length * sizeof(wchar)));
	Str16C newPath = (Str16C){ .str = extendedPath, .length = (pathBuffSize >> 1) };

	
	for (size_t i = (bigger.length + 2); i < newPath.length; i++)
	{
		if (newPath.str[i] == u'/')
			newPath.str[i] = u'\\';
	}
	return newPath;
}

static Str16C WIN_DirectorySearchPath(Arena* arena, Str16C path)
{
	if (path.str[path.length - 2] == u'/' || path.str[path.length - 2] == u'\\')
	{
		path.length = path.length - 1; 
	}

	Str16C fid = (Str16C){ .str = filesInDirectory, .length = (sizeof(filesInDirectory) >> 1) };
	if (CanAddPathPrefix(path))
	{
		Str16 bigger = (Str16){ .str = biggerPaths, .length = ((sizeof(biggerPaths)-2) >> 1) };
		
		size_t pathBuffSize = (bigger.length + path.length - 1 + fid.length) * sizeof(WCHAR);
		WCHAR* folderSearch = ArenaPush(arena, pathBuffSize);
		memcpy(folderSearch, bigger.str, bigger.length * sizeof(WCHAR));
		memcpy((char*)folderSearch + bigger.length * sizeof(WCHAR), path.str, (path.length - 1) * sizeof(WCHAR));
		memcpy((char*)folderSearch + (bigger.length + path.length - 1) * sizeof(WCHAR), fid.str, fid.length * sizeof(WCHAR));

		Str16C newPath = (Str16C){ .str = folderSearch, .length = (pathBuffSize >> 1) };
		
		for (size_t i = (bigger.length + 2); i < newPath.length; i++)
		{
			if (newPath.str[i] == u'/')
				newPath.str[i] = u'\\';
		}
		return newPath;
	}
	else
	{
		size_t pathBuffSize = (path.length - 1 + fid.length) * sizeof(WCHAR);
		WCHAR* folderSearch = ArenaPush(arena, pathBuffSize);
		memcpy((char*)folderSearch, path.str, (path.length - 1) * sizeof(WCHAR));
		memcpy((char*)folderSearch + (path.length -1) * sizeof(WCHAR), fid.str, (fid.length * sizeof(WCHAR)));

		Str16C newPath = (Str16C){ .str = folderSearch, .length = (pathBuffSize >> 1)};
		return newPath;
	}
}

static Str16C DirectoryPlusFile(Arena* arena, Str16C dir, Str16C file)
{
	
	if (dir.str[dir.length - 2] == u'/' || dir.str[dir.length - 2] == u'\\')
	{
		dir.length = dir.length - 1; 
	}

	size_t addedPathSize = (dir.length - 1 + 1 + file.length) * sizeof(WCHAR);
	wchar* addedPath = ArenaPush(arena, addedPathSize);
	memcpy(addedPath, dir.str, (dir.length - 1) * sizeof(WCHAR));
	addedPath[dir.length - 1] = u'\\';
	memcpy((char*)addedPath + (dir.length -1 + 1) * sizeof(WCHAR), file.str, file.length * sizeof(WCHAR));
	return (Str16C) { .str = addedPath, .length = (addedPathSize >> 1) };
}


static b32 WIN_EnumerateFiles(Arena* arena, LOGHANDLE logFile, Str16CNode** list, Str16C argPath)
{
	Str8 pathStr8 = STR_S8FromS16(arena, argPath);
	Str16C path = WIN_SanitizePath(arena, argPath);
	DWORD fileAttr = GetFileAttributesW(path.str);
	if (fileAttr == 0xFFFFFFFF)
	{	
		WIN_LogStr8(logFile, "%S is not a valid file or directory path\r\n", pathStr8);
		return 0;
	}
	else if (fileAttr & 0x00000010)
	{
		Str16C folderSearch = WIN_DirectorySearchPath(arena, path);
	
		
		
	
		WIN32_FIND_DATAW FindFileData;
	
		HANDLE hFind = FindFirstFileW(folderSearch.str, &FindFileData);
		if (hFind == ((HANDLE)(LONG_PTR)-1))
		{
			DWORD fileError = GetLastError();
			if (fileError == 0x2)
			{
				WIN_LogStr8(logFile, "Directory is empty\r\n");
			}
			else
			{
				WIN_LogStr8(logFile, "Directory search error\r\n");
			}
			return 0;
		}
	
		
		do
		{
			if ((FindFileData.dwFileAttributes & 0x00000010) == 0)
			{
				size_t fnameSize = strlenW(FindFileData.cFileName) + sizeof(WCHAR);
				
				
	
				Str16C dirPlusFile = DirectoryPlusFile(arena, path, (Str16C) { .str = FindFileData.cFileName, .length = (fnameSize >> 1) });
	
				EnqueueUniqueStr16C(arena, list, dirPlusFile);
			}
		} while (FindNextFileW(hFind, &FindFileData));
	
		FindClose(hFind);
	}
	else
	{
		EnqueueUniqueStr16C(arena, list, path);
	}
	
	return 1;
}	

static Str16C WIN_GetFileNameWithExtension(Str16C filePath)
{
	
	size_t fNameIndex = 0;
	for(size_t i = 0; i < (filePath.length); i++)
	{
		WCHAR cur = filePath.str[i];
		if(cur == u'\\' || cur == u'/')
			fNameIndex = i + 1;
	}
	
	return (Str16C){.str = (filePath.str + fNameIndex), .length = filePath.length - fNameIndex };
}


static Str16C WIN_GetFileExtension(Str16C filePath)
{
	size_t extIndex = Str16CLastIndexOf(filePath, u'.');
	if(extIndex == -1)
		return (Str16C){.str = 0, .length = 0};
	Str16C ext = (Str16C){.str = filePath.str + extIndex + 1, .length = filePath.length - extIndex - 1};
	return ext;
}

static Str16 WIN_GetFileNameWithoutExtension(Str16C filePath)
{
	Str16C fNameWithExt = WIN_GetFileNameWithExtension(filePath);
	size_t dotIndex = Str16CLastIndexOf(fNameWithExt, u'.');
	if(dotIndex == -1)
	{
		fNameWithExt.length = fNameWithExt.length -1;
		return *(Str16*)(&fNameWithExt);
	}
	else
		return (Str16){.str = fNameWithExt.str, .length = dotIndex };
}

static b32 WIN_FileExists(wchar* szPath)
{
  DWORD dwAttrib = GetFileAttributesW(szPath);

  return (dwAttrib != 0xFFFFFFFF && 
         !(dwAttrib & 0x00000010));
}
#line 15 "src\\AS_Converter.c"
#line 1 "C:\\Users\\Harry\\git\\TitS\\Layers\\Windows\\WIN_Logging.c"


static LOGHANDLE WIN_CreateLogHandle(HANDLE hFile)
{
	HANDLE hMutex = CreateMutexA(0, 0, 0);
	return (LOGHANDLE){ .hFile = hFile, hMutex = hMutex };	
}

static void WIN_CloseLogHandle(LOGHANDLE logger)
{
	CloseHandle(logger.hMutex);
}

static void WIN_LogStr8(LOGHANDLE logger, char* fmt, ...)
{
	ArenaTemp string_scratch = GetScratch(0, 0);
	va_list args;
	((void)(__va_start(&args, fmt)));
	Str8 str = Str8CPushFV(string_scratch.arena, fmt, args);
	((void)(args = (va_list)0));
	
	DWORD dwWaitResult = WaitForSingleObject(logger.hMutex, 0xFFFFFFFF);
	int numWritten;
	WriteFile(logger.hFile, str.str, str.size, &numWritten, 0);
	ReleaseMutex(logger.hMutex);
	ArenaEndTemp(string_scratch);
	return;
}
#line 16 "src\\AS_Converter.c"



#line 1 "C:\\Users\\Harry\\git\\TitS\\CLI_Tools\\AS_Converter\\src\\AS_Actor.c"



#line 1 "C:\\Users\\Harry\\git\\TitS\\CLI_Tools\\AS_Converter\\src\\BinaryReader.h"
















typedef struct ReadRes_u8{ BOOLEAN success; size_t pos; u8 val;} ParseRes_u8;;
typedef struct ReadRes_u16{ BOOLEAN success; size_t pos; u16 val;} ParseRes_u16;;
typedef struct ReadRes_u32{ BOOLEAN success; size_t pos; u32 val;} ParseRes_u32;;
typedef struct ReadRes_Str8{ BOOLEAN success; size_t pos; Str8 val;} ParseRes_Str8;;
typedef struct ReadRes_Str8C{ BOOLEAN success; size_t pos; Str8C val;} ParseRes_Str8C;;

typedef struct ReadRes_Str8CArray
{
	BOOLEAN success;
	Str8CNode* first;
} ReadRes_Str8CArray;

typedef struct BinaryReader
{
	void* file;
	size_t size;
	size_t pos;
} BinaryReader;

static inline ReadRes_u8 Peek_u8(BinaryReader* ctx, int offset)
{
	if(ctx->pos + offset - sizeof(u8) > ctx->size)
		return (ReadRes_u8){0, 0};
	
	u8 val = *(u8*)(ctx->file + ctx->pos + offset);
	size_t readPos = ctx->pos + offset;
	
	return (ReadRes_u8){.success = 1, .pos = readPos, .val = val};
}
static inline ReadRes_u8 Read_u8(BinaryReader* ctx)
{
	if(ctx->pos - sizeof(u8) > ctx->size)
		return (ReadRes_u8){0, 0};
	
	u8 val = *(u8*)(ctx->file + ctx->pos);
	size_t readPos = ctx->pos;
	ctx->pos += sizeof(u8);
	
	return (ReadRes_u8){.success = 1, .pos = readPos, .val = val};
}
static inline ReadRes_u16 Read_u16(BinaryReader* ctx)
{
	if(ctx->pos - sizeof(u16) > ctx->size)
		return (ReadRes_u16){0, 0};
	
	u16 val = *(u16*)(ctx->file + ctx->pos);
	size_t readPos = ctx->pos;
	ctx->pos += sizeof(u16);
	
	return (ReadRes_u16){.success = 1, .pos = readPos, .val = val};
}
static inline ReadRes_u32 Read_u32(BinaryReader* ctx)
{
	if(ctx->pos - sizeof(u32)> ctx->size)
		return (ReadRes_u32){0, 0};
	
	u32 val = *(u32*)(ctx->file + ctx->pos);
	size_t readPos = ctx->pos;
	ctx->pos += sizeof(u32);
	
	return (ReadRes_u32){.success = 1, .pos = readPos, .val = val};
}
static inline ReadRes_Str8C Read_Str8C(BinaryReader* ctx)
{
	size_t readPos = ctx->pos;
	char* str = ((char*)ctx->file + ctx->pos);
	size_t length = 0;
	ReadRes_u8 readByte;
	do
	{
		readByte = Read_u8(ctx);
		if(!readByte.success)
			break;
		
		length += 1;
	} while(readByte.val != 0);
	
	return (ReadRes_Str8C){.success = readByte.success, .pos = readPos, .val = (Str8C{str, length}};
}
static inline ReadRes_Str8CArray Read_Str8CArray(BinaryReader* ctx, StackArena* arena)
{	
	size_t readPos = ctx->pos;
	Str8CNode* start = 0;
	Str8CNode* end = 0;
	ReadRes_Str8C readStr8C;
	do
	{
		readStr8C = Read_Str8C(ctx);
		if(!readStr8C.success)
			break;
		
		Str8CNode* node = StackArena_Alloc(arena, sizeof(Str8CNode));
		
		node->str = readStr8C.val;
		node->next = 0;
		if(start == 0)
		{
			start = node;
		}
		if(end != 0)
		{
			end->next = node;
		}
		end = node;
	}
	
	return (ReadRes_Str8CArray){.success = readStr8C.success, .pos = readPos, .val = start};
}
#line 126 "C:\\Users\\Harry\\git\\TitS\\CLI_Tools\\AS_Converter\\src\\BinaryReader.h"
#line 5 "C:\\Users\\Harry\\git\\TitS\\CLI_Tools\\AS_Converter\\src\\AS_Actor.c"






























#pragma pack(push,1)
typedef struct chip_Binary
{
    uint16 index;
    uint16 datatable;
} chip_Binary;

typedef struct chip_entry_Binary
{
    chip_Binary ch;
    chip_Binary cp;
} chip_entry_Binary;

typedef struct AS_Actor_Header_Binary
{
	u16 ProcedureOffsetTableStart;
	u16 ProcedureOffsetTableEnd;
	u16 Bones3dOffset;
	chip_entry[] chip_entries;
} AS_Actor_Header_Binary;

typedef struct sprite_offset
{
    uint8 horizontal_offset;
    uint8 vertical_offset;
} sprite_offset;

typedef struct sprite_offset_table
{
	sprite_offset offsets[8];
}

typedef struct bones
{
    u8 unk00;
};
#pragma pack(pop)

static BOOLEAN ParseAS_ActorBinary(StackArena* arena, void* file, size_t size, LOGHANDLE logger)
{
	BOOLEAN hasErrors = 0;
	AS_Actor_Header_Binary* header = file;	
	if(size < sizeof(AS_Actor_Header_Binary)))
	{
		WIN_LogStr8("file smaller than header size");
		return 0;
	}
	
	if(header->ProcedureOffsetTableStart > size)
	{
		WIN_LogStr8("Out of bounds Procedure offset table start: %d", header->ProcedureOffsetTableStart);
		hasErrors = 1;
	}
	if(header->ProcedureOffsetTableEnd > size)
	{
		WIN_LogStr8("Out of bounds Procedure offset table end: %d", header->ProcedureOffsetTableEnd);
		hasErrors = 1;
	}
	if(header->Bones3dOffset > size)
	{
		WIN_LogStr8("Out of bounds Bones 3d offset: %d", header->Bones3dOffset);
		hasErrors = 1;
	}
	if(hasErrors)
	{
		return 0;
	}
	if(header->ProcedureOffsetTableStart > header->ProcedureOffsetTableEnd)
	{
		WIN_LogStr8("Procedure Offset table Start (%d) higher than End (%d)", header->ProcedureOffsetTableStart, header->ProcedureOffsetTableEnd);
		return 0;
	}
	if((header->ProcedureOffsetTableEnd - header->ProcedureOffsetTableStart) % 2 != 0)
	{
		WIN_LogStr8("Unalligned Procedure Offset table Start (%d) End (%d)", header->ProcedureOffsetTableStart, header->ProcedureOffsetTableEnd);
		return 0;
	}
	
	u64 numProcedures = header->ProcedureOffsetTableEnd - header->ProcedureOffsetTableStart / 2;
	if(numProcedures < 30
		|| numProcedures > 34)
	{
		WIN_LogStr8("Number of procedures (%d) is bigger than the maximum allowed (%d)", numProcedures, 34);
		return 0;
	}
	
	BinaryReader reader_ = {. file = file, .size = size, .pos = sizeof(AS_Actor_Header_Binary)};
	BinaryReader* reader = &reader_;
	s8 chcp_count = 0;
	do
	{
		ReadRes_u8 ch = Read_u8(reader);
		if(!ch.success)
		{
			WIN_LogStr8("End of file reached when reading ch %d", chcp + 1);
			return 0;
		}
		else if (ch.val == 0xFFFF)
		{
			break;
		}
		ReadRes_u8 cp = Read_u8(reader);
		if(!cp.success)
		{
			WIN_LogStr8("End of file reached when reading cp %d", chcp + 1);
			return 0;
		}
		else
		{
			chcp += 1
		}
	} while (1);
	
	ReadRes_Str8CArray model_names = Read_Str8CArray(reader, arena);
	if(!model_names.success)
	{
		WIN_LogStr8("End of file reached when reading model_names array");
		return 0;
	}
	
	bones* bones_3d = 0;
	ReadRes_Str8CArray bone_names = 0;
	if(header->Bones3dOffset != 0)
	{
		if(header->Bones3dOffset != reader->pos)
		{
			WIN_LogStr8("Bones 3d offset (%d) mismatch with offset defined in header (%d)", reader->pos, header->Bones3dOffset);
			return 0;
		}
		
		bones_3d = (bones*)((char*)reader->file + reader->pos);
		reader->pos += sizeof(bones);
		if(reader->pos > size)
		{
			WIN_LogStr8("End of file reached when reading Bones");
			return 0;
		}
		bone_names = Read_Str8CArray(reader, arena);
		if(!bone_names.success)
		{
			WIN_LogStr8("End of file reached when reading bone_names");
			return 0;
		}
	}
	
	if(header->ProcedureOffsetTableStart != reader->pos)
	{
		WIN_LogStr8("Procedure Offset Table Start (%d) mismatch with offset defined in header (%d)", reader->pos, header->ProcedureOffsetTableStart);
		return 0;
	}
	u16* procedureOffsets = (u16*)((char*)file + reader->pos);
	for(u64 i = 0; i < numProcedures; i++)
	{
		if(procedureOffsets[i] > size)
		{
			WIN_LogStr8("Procedure %d start outside the file bounds", i);
			return 0;
		}
	}	
	reader->pos += numProcedures * sizeof(u16);
	
	sprite_offset_table* sprite_offsets = (sprite_offset_table*)((char*)file + reader->pos);
	reader.pos += sizeof(sprite_offset_table);
	if(reader.pos > size)
	{
		WIN_LogStr8("End of file reached when reading sprite table");
		return 0;
	}
	
	BinaryParseContext* ctx = (BinaryParseContext*)((memset((BinaryParseContext*)(ArenaPush((arena), sizeof(BinaryParseContext)*(1))),0,sizeof(BinaryParseContext)*(1))));
		
	u16 instrCount = 0;
	Instruction* instructions = ArenaPush((0xFFFF - reader->pos) * sizeof(Instruction)); 
	
	while(reader->pos < size)
	{
		ReadRes_Instruction res = ReadInstruction(ctx, arena, reader);
		if(res.success = 0)
		{
			return 0;
		}
		else
		{
			instructions[instrCount] = res.val;
			instrCount += 1;
		}
	}
}

#line 225 "C:\\Users\\Harry\\git\\TitS\\CLI_Tools\\AS_Converter\\src\\AS_Actor.c"
#line 20 "src\\AS_Converter.c"

int _fltused;

CHAR usage[] =
"AS_Converter version 1.c\r\n\
Usage: [PATH] [-OPTIONAL_OUT]\r\n\
PATH: A directory or a file to convert AS*****._DT/ASMAG000._DT/ASITEM._DT/BS*****._DT to and from \".as\" pseudo-assembly files\r\n\
OPTIONAL_OUT: By default the program will create two directories \"out\" and \"outbin\"\r\n\
in the directory of this executable for the outputted .as and ._DT accordingly.\r\n\
Set this argument to change the folder in which the subdirectories will be created.\r\n";

typedef enum AS_FORMAT
{
	AS_NONE = 0,

	AS_DT_ACTOR = 1,
	AS_DT_MAG = 2,
	AS_DT_ITEM = 3,
	AS_DT_BS = 4,

	AS_SCRIPT_ACTOR = 5,
	AS_SCRIPT_MAG = 6,
	AS_SCRIPT_ITEM = 7,
	AS_SCRIPT_BS = 8,
} AS_FORMAT;

static b32 IsASActor(Str16 str)
{
	if(str.length < 7)
		return 0;
	
	WCHAR* leStr = str.str;
	if(leStr[0] != u'A')
		return 0;
	if(leStr[1] != u'S')
		return 0;
	if(!IsDigitW(leStr[2]))
		return 0;
	if(!IsDigitW(leStr[3]))
		return 0;
	if(!IsDigitW(leStr[4]))
		return 0;
	if(!IsDigitW(leStr[5]))
		return 0;
	if(!IsDigitW(leStr[6]))
		return 0;
	for (size_t i = 7; i < str.length; i++)
	{
		if(leStr[i] != u' ')
			return 0;
	}
	return 1;
}

static b32 IsASBS(Str16 str)
{
	if(str.length < 7)
		return 0;
	
	wchar* leStr = str.str;
	if(leStr[0] != u'B')
		return 0;
	if(leStr[1] != u'S')
		return 0;
	if(!IsDigitW(leStr[2]))
		return 0;
	if(!IsDigitW(leStr[3]))
		return 0;
	if(!IsDigitW(leStr[4]))
		return 0;
	if(!IsDigitW(leStr[5]))
		return 0;
	if(!IsDigitW(leStr[6]))
		return 0;
	for (size_t i = 7; i < str.length; i++)
	{
		if(leStr[i] != u' ')
			return 0;
	}
	return 1;
}

static b32 IsASMAG(Str16 str)
{
	Str16 asMag = (Str16){ .str = u"ASMAG000", .length = ((sizeof(u"ASMAG000")-2) >> 1) };
	if(Str16StartsWithCaseInsensitive(str, asMag) 
		&& IsRepeatedWchar(SkipSubStr16(str, asMag.length), u' '))
		return 1;
	else
		return 0;
}

static b32 IsASItem(Str16 str)
{
	Str16 asItem = (Str16){ .str = u"ASITEM", .length = ((sizeof(u"ASITEM")-2) >> 1) };
	if(Str16StartsWithCaseInsensitive(str, asItem)
		&& IsRepeatedWchar(SkipSubStr16(str, asItem.length), u' '))
		return 1;
	else
		return 0;
}

static AS_FORMAT DeduceFileFormat(Str16C path)
{	
	Str16C fNameWithExt = WIN_GetFileNameWithExtension(path);	
	Str16C fExt = WIN_GetFileExtension(fNameWithExt);

	if(fExt.length == 0)
		return AS_NONE;

	Str16 fNameNoExt = WIN_GetFileNameWithoutExtension(fNameWithExt);
	
	if(Str16CCmp(fExt, (Str16C){ .str = u"_DT", .length = (sizeof(u"_DT") >> 1) }) || Str16CCmp(fExt, (Str16C){ .str = u"_dt", .length = (sizeof(u"_dt") >> 1) }))
	{
		if (IsASMAG(fNameNoExt))
		{
			return AS_DT_MAG;
		}
		else if (IsASItem(fNameNoExt))
		{
			return AS_DT_ITEM;
		}
		else if (IsASActor(fNameNoExt))
		{
			return AS_DT_ACTOR;
		}
		else if (IsASBS(fNameNoExt))
		{
			return AS_DT_BS;
		}
	}
	else if (Str16CCmp(fExt, (Str16C){ .str = u"as", .length = (sizeof(u"as") >> 1) }))
	{
		if (IsASMAG(fNameNoExt))
		{
			return AS_SCRIPT_MAG;
		}
		else if (IsASItem(fNameNoExt))
		{
			return AS_SCRIPT_ITEM;
		}
		else if (IsASActor(fNameNoExt))
		{
			return AS_SCRIPT_ACTOR;
		}
		else if (IsASBS(fNameNoExt))
		{
			return AS_SCRIPT_BS;
		}
	}

	return AS_NONE;
}

Str16 outputFolder;

static inline Log16(LOGHANDLE logFile, char* format, Str16C path)
{
	ArenaTemp string_scratch = GetScratch(0, 0);
	Str8 path8 = STR_S8FromS16(string_scratch.arena, path);
	WIN_LogStr8(logFile, format, path8);
	ArenaEndTemp(string_scratch);
}

int __stdcall mainCRTStartup()
{
	Arena* arena = ArenaAlloc();
	int argc;
	WCHAR** wargv = CommandLineToArgvW(GetCommandLineW(), &argc);
	u32 numWritten;
	HANDLE stdout_ = CreateFileA("CON", 0x40000000, 0x00000001, 0, 3, 0x80, 0);
	LOGHANDLE stdout = WIN_CreateLogHandle(stdout_);
	if (argc == 1)
	{
		WIN_LogStr8(stdout, usage);
		return 0;
	}

	Str16CNode* firstNode = 0;
	for (int i = 1; i < argc; i++)
	{
		if (*wargv[i] == u'-')
		{
			if (i == 1)
			{
				WIN_LogStr8(stdout, "No input path specified\r\n");
				return 0;
			}

			Str16C argPath = (Str16C){ .str = wargv[i] + 1, .length = ((strlenW(wargv[i] + 1) + sizeof(WCHAR)) >> 1) };
			Str16C outputFolder = WIN_SanitizePath(arena, argPath);
			DWORD outFolderAttr = GetFileAttributesW(outputFolder);
			if (!(outFolderAttr & 0x00000010))
			{
				Log16(stdout, "%s is not a valid directory path\r\n", argPath);
				return 0;
			}

			if (i + 1 != argc)
			{
				WIN_LogStr8(stdout, "ignoring arguments after -OPTIONAL_OUT\r\n");
			}
			break;
		}

		Str16C argPath = (Str16C){ .str = wargv[i], .length = ((strlenW(wargv[i]) + sizeof(WCHAR)) >> 1) };
		if(!WIN_EnumerateFiles(arena, stdout, &firstNode, argPath))
			return 0;
	}

	for (Str16CNode* node = firstNode; node != 0; node = node->next)
	{
		AS_FORMAT format = DeduceFileFormat(node->str);
		if (format == AS_NONE)
			continue;

		HANDLE hFile = CreateFileW(node->str.str, 0x80000000, 0x00000001, 0, 3, 0x80, 0);
		if (hFile == ((HANDLE)(LONG_PTR)-1))
		{
			Log16(stdout, "Can't open file %s for reading.\r\n", node->str);			
			continue;
		}

		LARGE_INTEGER fileSize;
		if (!GetFileSizeEx(hFile, &fileSize))
		{
			Log16(stdout, "Can't determine size of file %s", node->str);
			continue;
		}
		else if(fileSize.QuadPart > 0xFFFF)
		{
			Log16(stdout, "File %s exceeds the maximum size of an AS script binary", node->str);
			continue;
		}

		ArenaTemp processingScratchArena = ArenaBeginTemp(arena);
		void* fileContents = ArenaPush(arena, fileSize.QuadPart);
		if (fileContents == 0)
		{
			WIN_LogStr8(stdout, "Out of memory.");
			return 0;
		}
		
		QWORD totalBytesRead = 0;
		while(totalBytesRead != fileSize.QuadPart)
		{	
			DWORD curBytesRead;
			if (!ReadFile(hFile, ((char*)fileContents + totalBytesRead), (int)fileSize.QuadPart, &curBytesRead, 0))
			{
				Log16(stdout, "Error reading file %s\r\n", node->str);
				continue;
			}
			totalBytesRead += curBytesRead;
		}
		
		switch(format)
		{
		case AS_DT_ACTOR:
			BOOLEAN succ = ParseAS_ActorBinary(processingScratchArena, fileContents, fileSize.QuadPart, stdout);
			break;
		case AS_DT_MAG:
			break;
		case AS_DT_ITEM:
			break;
		case AS_DT_BS:
			break;

		case AS_SCRIPT_ACTOR:
			break;
		case AS_SCRIPT_MAG:
			break;
		case AS_SCRIPT_ITEM:
			break;
		case AS_SCRIPT_BS:
			break;
		}
		
		WIN_LogStr8(stdout, "%d %S\r\n", format + 0x30, node->str);
		
		ArenaEndTemp(processingScratchArena);
	}
	return 0;
}
