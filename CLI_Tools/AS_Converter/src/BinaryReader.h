#ifndef BinaryReader_H
#define BinaryReader_H

#include "BASE_Types.h"
#include "BASE_Misc.h"
#include "Str.h"
#include "Arena.h"

#define ReadResType(x)\
typedef struct ReadRes_##x\
{\
	BOOLEAN success;\
	size_t pos;\
	x val;\
} ReadRes_##x;

ReadResType(u8)
ReadResType(u16)
ReadResType(u32)
ReadResType(Str8)
ReadResType(Str8C)
#undef ReadResType
typedef struct ReadRes_Str8CArray
{
	BOOLEAN success;
	size_t pos;
	Str8CNode* val;
} ReadRes_Str8CArray;

typedef struct BinaryReader
{
	char* file;
	size_t size;
	size_t pos;
} BinaryReader;

static inline ReadRes_u8 Peek_u8(BinaryReader* ctx, int offset)
{
	if(ctx->pos + offset - sizeof(u8) > ctx->size)
		return (ReadRes_u8){false, 0};
	
	u8 val = *(u8*)(ctx->file + ctx->pos + offset);
	size_t readPos = ctx->pos + offset;
	
	return (ReadRes_u8){.success = true, .pos = readPos, .val = val};
}
static inline ReadRes_u8 Read_u8(BinaryReader* ctx)
{
	if(ctx->pos - sizeof(u8) > ctx->size)
		return (ReadRes_u8){false, 0};
	
	u8 val = *(u8*)(ctx->file + ctx->pos);
	size_t readPos = ctx->pos;
	ctx->pos += sizeof(u8);
	
	return (ReadRes_u8){.success = true, .pos = readPos, .val = val};
}
static inline ReadRes_u16 Read_u16(BinaryReader* ctx)
{
	if(ctx->pos - sizeof(u16) > ctx->size)
		return (ReadRes_u16){false, 0};
	
	u16 val = *(u16*)(ctx->file + ctx->pos);
	size_t readPos = ctx->pos;
	ctx->pos += sizeof(u16);
	
	return (ReadRes_u16){.success = true, .pos = readPos, .val = val};
}
static inline ReadRes_u32 Read_u32(BinaryReader* ctx)
{
	if(ctx->pos - sizeof(u32)> ctx->size)
		return (ReadRes_u32){false, 0};
	
	u32 val = *(u32*)(ctx->file + ctx->pos);
	size_t readPos = ctx->pos;
	ctx->pos += sizeof(u32);
	
	return (ReadRes_u32){.success = true, .pos = readPos, .val = val};
}
static inline ReadRes_Str8C Read_Str8C(BinaryReader* ctx)
{
	size_t readPos = ctx->pos;
	char* str = ((char*)ctx->file + ctx->pos);
	size_t length = 0;
	ReadRes_u8 readByte;
	do
	{
		readByte = Read_u8(ctx);
		if(!readByte.success)
			break;
		
		length += 1;
	} while(readByte.val != 0);
	
	return (ReadRes_Str8C){.success = readByte.success, .pos = readPos, .val = (Str8C){str, length}};
}
static inline ReadRes_Str8CArray Read_Str8CArray(BinaryReader* ctx, Arena* arena)
{	
	size_t readPos = ctx->pos;
	Str8CNode* start = NULL;
	Str8CNode* end = NULL;
	ReadRes_Str8C readStr8C;
	do
	{
		readStr8C = Read_Str8C(ctx);
		if(!readStr8C.success)
			break;
		
		Str8CNode* node = ArenaPush(arena, sizeof(Str8CNode));
		
		node->str = readStr8C.val;
		node->next = NULL;
		if(start == NULL)
		{
			start = node;
		}
		if(end != NULL)
		{
			end->next = node;
		}
		end = node;
	} while (true);
	
	return (ReadRes_Str8CArray){.success = readStr8C.success, .pos = readPos, .val = start};
}
#endif BinaryReader_H