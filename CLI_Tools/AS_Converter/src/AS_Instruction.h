CHAR InvalidOffset[] = "The operand %S of the %S instruction at offset %#4X points to invalid offset %#4X";
char s_go_to[] = "go_to";char s_go_to_label[] = "label";typedef struct go_to
{	u16 label;} go_to;char s_as_04[] = "as_04";char s_as_04_target[] = "target";char s_as_04_op2[] = "op2";char s_as_04_op3[] = "op3";typedef struct as_04
{	u8 target;	u8 op2;	u16 op3;} as_04;char s_as_05[] = "as_05";char s_as_05_op1[] = "op1";char s_as_05_op2[] = "op2";char s_as_05_op3[] = "op3";typedef struct as_05
{	u8 op1;	u8 op2;	u32 op3;} as_05;char s_as_8e[] = "as_8e";char s_as_8e_op1[] = "op1";char s_as_8e_op2[] = "op2";char s_as_8e_op3[] = "op3";char s_as_8e_op4[] = "op4";char s_as_8e_op5[] = "op5";typedef struct as_8e
{	u8 op1;	u32 op2;	u32 op3;	u32 op4;	u32 op5;} as_8e;char s_load_x_file[] = "load_x_file";char s_load_x_file_op1[] = "op1";char s_load_x_file_op2[] = "op2";typedef struct load_x_file
{	u8 op1;	Str8C op2;} load_x_file;char s_as_8e0d[] = "as_8e0d";char s_as_8e0d_op1[] = "op1";char s_as_8e0d_op2[] = "op2";char s_as_8e0d_op3[] = "op3";char s_as_8e0d_op4[] = "op4";char s_as_8e0d_op5[] = "op5";char s_as_8e0d_op6[] = "op6";typedef struct as_8e0d
{	u8 op1;	u32 op2;	u32 op3;	u32 op4;	u32 op5;	u32 op6;} as_8e0d;char s_set_angle_target[] = "set_angle_target";char s_set_angle_target_target[] = "target";char s_set_angle_target_frame[] = "frame";char s_set_angle_target_op3[] = "op3";typedef struct set_angle_target
{	u8 target;	Str8C frame;	u16 op3;} set_angle_target;typedef struct Instruction
{	u16 offset;	u64 opcode;	union
	{
		go_to go_to;		as_04 as_04;		as_05 as_05;		as_8e as_8e;		load_x_file load_x_file;		as_8e0d as_8e0d;		set_angle_target set_angle_target;	};
} Instruction;typedef struct ReadRes_Instruction
{
	b32 success;
	Instruction val;
} ReadRes_Instruction;
internal inline ReadRes_Instruction Read_go_to(BinaryParseContext* ctx, Arena* arena, BinaryReader* reader)
{
	u16 pos = reader->pos;	ReadRes_u16 r_label = Read_u16(reader);	if(!r_label.success)
		return (ReadRes_Instruction){false, (Instruction){0}};
	ctx->OffsetFlags[r_label.val] &= OffsetFlag_LabelTarget;	return (ReadRes_Instruction)
	{
		.success = true,
		.val = (Instruction)
		{
			.offset = pos,
			.go_to = (go_to)
			{				.label = r_label.val,			}
		}
	};
}
internal inline ReadRes_Instruction Read_as_04(BinaryParseContext* ctx, Arena* arena, BinaryReader* reader)
{
	u16 pos = reader->pos;	ReadRes_u8 r_target = Read_u8(reader);	ReadRes_u8 r_op2 = Read_u8(reader);	ReadRes_u16 r_op3 = Read_u16(reader);	if(!r_op3.success)
		return (ReadRes_Instruction){false, (Instruction){0}};
	ctx->Targets[r_target.val] = 1;	return (ReadRes_Instruction)
	{
		.success = true,
		.val = (Instruction)
		{
			.offset = pos,
			.as_04 = (as_04)
			{				.target = r_target.val,				.op2 = r_op2.val,				.op3 = r_op3.val,			}
		}
	};
}
internal inline ReadRes_Instruction Read_as_05(BinaryParseContext* ctx, Arena* arena, BinaryReader* reader)
{
	u16 pos = reader->pos;	ReadRes_u8 r_op1 = Read_u8(reader);	ReadRes_u8 r_op2 = Read_u8(reader);	ReadRes_u32 r_op3 = Read_u32(reader);	if(!r_op3.success)
		return (ReadRes_Instruction){false, (Instruction){0}};
	return (ReadRes_Instruction)
	{
		.success = true,
		.val = (Instruction)
		{
			.offset = pos,
			.as_05 = (as_05)
			{				.op1 = r_op1.val,				.op2 = r_op2.val,				.op3 = r_op3.val,			}
		}
	};
}
internal inline ReadRes_Instruction Read_as_8e(BinaryParseContext* ctx, Arena* arena, BinaryReader* reader)
{
	u16 pos = reader->pos;	ReadRes_u8 r_op1 = Read_u8(reader);	ReadRes_u32 r_op2 = Read_u32(reader);	ReadRes_u32 r_op3 = Read_u32(reader);	ReadRes_u32 r_op4 = Read_u32(reader);	ReadRes_u32 r_op5 = Read_u32(reader);	if(!r_op5.success)
		return (ReadRes_Instruction){false, (Instruction){0}};
	return (ReadRes_Instruction)
	{
		.success = true,
		.val = (Instruction)
		{
			.offset = pos,
			.as_8e = (as_8e)
			{				.op1 = r_op1.val,				.op2 = r_op2.val,				.op3 = r_op3.val,				.op4 = r_op4.val,				.op5 = r_op5.val,			}
		}
	};
}
internal inline ReadRes_Instruction Read_load_x_file(BinaryParseContext* ctx, Arena* arena, BinaryReader* reader)
{
	u16 pos = reader->pos;	ReadRes_u8 r_op1 = Read_u8(reader);	ReadRes_Str8C r_op2 = Read_Str8C(reader);	if(!r_op2.success)
		return (ReadRes_Instruction){false, (Instruction){0}};
	return (ReadRes_Instruction)
	{
		.success = true,
		.val = (Instruction)
		{
			.offset = pos,
			.load_x_file = (load_x_file)
			{				.op1 = r_op1.val,				.op2 = r_op2.val,			}
		}
	};
}
internal inline ReadRes_Instruction Read_as_8e0d(BinaryParseContext* ctx, Arena* arena, BinaryReader* reader)
{
	u16 pos = reader->pos;	ReadRes_u8 r_op1 = Read_u8(reader);	ReadRes_u32 r_op2 = Read_u32(reader);	ReadRes_u32 r_op3 = Read_u32(reader);	ReadRes_u32 r_op4 = Read_u32(reader);	ReadRes_u32 r_op5 = Read_u32(reader);	ReadRes_u32 r_op6 = Read_u32(reader);	if(!r_op6.success)
		return (ReadRes_Instruction){false, (Instruction){0}};
	return (ReadRes_Instruction)
	{
		.success = true,
		.val = (Instruction)
		{
			.offset = pos,
			.as_8e0d = (as_8e0d)
			{				.op1 = r_op1.val,				.op2 = r_op2.val,				.op3 = r_op3.val,				.op4 = r_op4.val,				.op5 = r_op5.val,				.op6 = r_op6.val,			}
		}
	};
}
internal inline ReadRes_Instruction Read_set_angle_target(BinaryParseContext* ctx, Arena* arena, BinaryReader* reader)
{
	u16 pos = reader->pos;	ReadRes_u8 r_target = Read_u8(reader);	ReadRes_Str8C r_frame = Read_Str8C(reader);	ReadRes_u16 r_op3 = Read_u16(reader);	if(!r_op3.success)
		return (ReadRes_Instruction){false, (Instruction){0}};
	ctx->Targets[r_target.val] = 1;	return (ReadRes_Instruction)
	{
		.success = true,
		.val = (Instruction)
		{
			.offset = pos,
			.set_angle_target = (set_angle_target)
			{				.target = r_target.val,				.frame = r_frame.val,				.op3 = r_op3.val,			}
		}
	};
}
internal inline ReadRes_Instruction ReadInstruction(BinaryParseContext* ctx, Arena* arena, BinaryReader* reader)
{
	ctx->OffsetFlags[reader] &= OffsetFlag_StartOfInstruction;
	ReadRes_u8 r_opcode = Read_u8(reader);
	if(!r_opcode.success)
		return (ReadRes_Instruction){false, (Instruction){}};

	u8 opcode = r_opcode.val;

	switch(opcode)
	{
	case 0x01:
	{		return Read_go_to(ctx, arena, reader);	}
	case 0x04:
	{		return Read_as_04(ctx, arena, reader);	}
	case 0x05:
	{		return Read_as_05(ctx, arena, reader);	}
	case 0x8e:
	{		u8 secondByte = Peek_u8(ctx, 0);
		switch(secondByte)
		{
		case 0x01:
		{
		return Read_load_x_file(ctx, arena, reader);		}
		case 0x0d:
		{
		return Read_as_8e0d(ctx, arena, reader);		}
		}
		return Read_as_8e(ctx, arena, reader);	}
	case 0x96:
	{		return Read_set_angle_target(ctx, arena, reader);	}

	}
	return (ReadRes_Instruction){false, (Instruction){}};
}
internal inline BOOLEAN Validate_go_to(BinaryParseContext* ctx, Instruction* instr, LOGHANDLE logger)
{
	BOOLEAN invalid = false;	if(!(ctx->OffsetFlags[instr.label] & OffsetFlag_StartOfInstruction))
	{
		invalid = true;
		WIN_LogStr8(InvalidOffset, s_go_to_label, s_go_to, instr.offset, instr.label);
	}	return !invalid;
}
internal inline BOOLEAN Validate_as_04(BinaryParseContext* ctx, Instruction* instr, LOGHANDLE logger)
{
	BOOLEAN invalid = false;	return !invalid;
}
internal inline BOOLEAN Validate_as_05(BinaryParseContext* ctx, Instruction* instr, LOGHANDLE logger)
{
	BOOLEAN invalid = false;	return !invalid;
}
internal inline BOOLEAN Validate_as_8e(BinaryParseContext* ctx, Instruction* instr, LOGHANDLE logger)
{
	BOOLEAN invalid = false;	return !invalid;
}
internal inline BOOLEAN Validate_load_x_file(BinaryParseContext* ctx, Instruction* instr, LOGHANDLE logger)
{
	BOOLEAN invalid = false;	return !invalid;
}
internal inline BOOLEAN Validate_as_8e0d(BinaryParseContext* ctx, Instruction* instr, LOGHANDLE logger)
{
	BOOLEAN invalid = false;	return !invalid;
}
internal inline BOOLEAN Validate_set_angle_target(BinaryParseContext* ctx, Instruction* instr, LOGHANDLE logger)
{
	BOOLEAN invalid = false;	return !invalid;
}
internal inline BOOLEAN ValidateInstruction(BinaryParseContext* ctx, Instruction* instr, LOGHANDLE logger)
{
	u8 opcode = (u8)instr->opcode;

	switch(opcode)
	{
	case 0x01:
	{		return Validate_go_to(ctx, instr, logger);	}
	case 0x04:
	{		return Validate_as_04(ctx, instr, logger);	}
	case 0x05:
	{		return Validate_as_05(ctx, instr, logger);	}
	case 0x8e:
	{		u8 secondByte = (u8)(instr->opcode >> 1);
		switch(secondByte)
		{
		case 0x01:
		{
		return Validate_load_x_file(ctx, instr, logger);		}
		case 0x0d:
		{
		return Validate_as_8e0d(ctx, instr, logger);		}
		}
		return Validate_as_8e(ctx, instr, logger);	}
	case 0x96:
	{		return Validate_set_angle_target(ctx, instr, logger);	}

	}
	return FALSE;
}
