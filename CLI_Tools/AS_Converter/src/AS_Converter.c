#include "Windows.h"

#include "stdarg.h"
#include "stdio.h"

#include "BASE_Types.h"
#include "BASE_Misc.h"

#include "SYN.h"

#include "Arena.c"
#include "Str.c"
#include "STR_Unicode.c"
#include "WIN_FileFuncs.c"
#include "WIN_Logging.c"



#include "AS_Actor.c"

int _fltused;

CHAR usage[] =
"AS_Converter version 1.c\r\n\
Usage: [PATH] [-OPTIONAL_OUT]\r\n\
PATH: A directory or a file to convert AS*****._DT/ASMAG000._DT/ASITEM._DT/BS*****._DT to and from \".as\" pseudo-assembly files\r\n\
OPTIONAL_OUT: By default the program will create two directories \"out\" and \"outbin\"\r\n\
in the directory of this executable for the outputted .as and ._DT accordingly.\r\n\
Set this argument to change the folder in which the subdirectories will be created.\r\n";

typedef enum AS_FORMAT
{
	AS_NONE = 0,

	AS_DT_ACTOR = 1,
	AS_DT_MAG = 2,
	AS_DT_ITEM = 3,
	AS_DT_BS = 4,

	AS_SCRIPT_ACTOR = 5,
	AS_SCRIPT_MAG = 6,
	AS_SCRIPT_ITEM = 7,
	AS_SCRIPT_BS = 8,
} AS_FORMAT;

internal b32 IsASActor(Str16 str)
{
	if(str.length < 7)
		return false;
	
	WCHAR* leStr = str.str;
	if(leStr[0] != u'A')
		return false;
	if(leStr[1] != u'S')
		return false;
	if(!IsDigitW(leStr[2]))
		return false;
	if(!IsDigitW(leStr[3]))
		return false;
	if(!IsDigitW(leStr[4]))
		return false;
	if(!IsDigitW(leStr[5]))
		return false;
	if(!IsDigitW(leStr[6]))
		return false;
	for (size_t i = 7; i < str.length; i++)
	{
		if(leStr[i] != u' ')
			return false;
	}
	return true;
}

internal b32 IsASBS(Str16 str)
{
	if(str.length < 7)
		return false;
	
	wchar* leStr = str.str;
	if(leStr[0] != u'B')
		return false;
	if(leStr[1] != u'S')
		return false;
	if(!IsDigitW(leStr[2]))
		return false;
	if(!IsDigitW(leStr[3]))
		return false;
	if(!IsDigitW(leStr[4]))
		return false;
	if(!IsDigitW(leStr[5]))
		return false;
	if(!IsDigitW(leStr[6]))
		return false;
	for (size_t i = 7; i < str.length; i++)
	{
		if(leStr[i] != u' ')
			return false;
	}
	return true;
}

internal b32 IsASMAG(Str16 str)
{
	Str16 asMag = Str16Lit(u"ASMAG000");
	if(Str16StartsWithCaseInsensitive(str, asMag) 
		&& IsRepeatedWchar(SkipSubStr16(str, asMag.length), u' '))
		return true;
	else
		return false;
}

internal b32 IsASItem(Str16 str)
{
	Str16 asItem = Str16Lit(u"ASITEM");
	if(Str16StartsWithCaseInsensitive(str, asItem)
		&& IsRepeatedWchar(SkipSubStr16(str, asItem.length), u' '))
		return true;
	else
		return false;
}

internal AS_FORMAT DeduceFileFormat(Str16C path)
{	
	Str16C fNameWithExt = WIN_GetFileNameWithExtension(path);	
	Str16C fExt = WIN_GetFileExtension(fNameWithExt);

	if(fExt.length == 0)
		return AS_NONE;

	Str16 fNameNoExt = WIN_GetFileNameWithoutExtension(fNameWithExt);
	//is the file ".as" or "._DT"?
	if(Str16CCmp(fExt, Str16CLit(u"_DT")) || Str16CCmp(fExt, Str16CLit(u"_dt")))
	{
		if (IsASMAG(fNameNoExt))
		{
			return AS_DT_MAG;
		}
		else if (IsASItem(fNameNoExt))
		{
			return AS_DT_ITEM;
		}
		else if (IsASActor(fNameNoExt))
		{
			return AS_DT_ACTOR;
		}
		else if (IsASBS(fNameNoExt))
		{
			return AS_DT_BS;
		}
	}
	else if (Str16CCmp(fExt, Str16CLit(u"as")))
	{
		if (IsASMAG(fNameNoExt))
		{
			return AS_SCRIPT_MAG;
		}
		else if (IsASItem(fNameNoExt))
		{
			return AS_SCRIPT_ITEM;
		}
		else if (IsASActor(fNameNoExt))
		{
			return AS_SCRIPT_ACTOR;
		}
		else if (IsASBS(fNameNoExt))
		{
			return AS_SCRIPT_BS;
		}
	}

	return AS_NONE;
}

Str16 outputFolder;

internal inline Log16(LOGHANDLE logFile, char* format, Str16C path)
{
	ArenaTemp string_scratch = GetScratch(NULL, 0);
	Str8 path8 = STR_S8FromS16(string_scratch.arena, path);
	WIN_LogStr8(logFile, format, path8);
	ReleaseScratch(string_scratch);
}

int __stdcall mainCRTStartup()
{
	Arena* arena = ArenaAlloc();
	int argc;
	WCHAR** wargv = CommandLineToArgvW(GetCommandLineW(), &argc);
	u32 numWritten;
	HANDLE stdout_ = CreateFileA("CON", GENERIC_WRITE, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	LOGHANDLE stdout = WIN_CreateLogHandle(stdout_);
	if (argc == 1)
	{
		WIN_LogStr8(stdout, usage);
		return 0;
	}

	Str16CNode* firstNode = NULL;
	for (int i = 1; i < argc; i++)
	{
		if (*wargv[i] == u'-')
		{
			if (i == 1)
			{
				WIN_LogStr8(stdout, "No input path specified\r\n");
				return 0;
			}

			Str16C argPath = (Str16C){ .str = wargv[i] + 1, .length = ((strlenW(wargv[i] + 1) + sizeof(WCHAR)) >> 1) };
			Str16C outputFolder = WIN_SanitizePath(arena, argPath);
			DWORD outFolderAttr = GetFileAttributesW(outputFolder);
			if (!(outFolderAttr & FILE_ATTRIBUTE_DIRECTORY))
			{
				Log16(stdout, "%s is not a valid directory path\r\n", argPath);
				return 0;
			}

			if (i + 1 != argc)
			{
				WIN_LogStr8(stdout, "ignoring arguments after -OPTIONAL_OUT\r\n");
			}
			break;
		}

		Str16C argPath = (Str16C){ .str = wargv[i], .length = ((strlenW(wargv[i]) + sizeof(WCHAR)) >> 1) };
		if(!WIN_EnumerateFiles(arena, stdout, &firstNode, argPath))
			return 0;
	}

	for (Str16CNode* node = firstNode; node != NULL; node = node->next)
	{
		AS_FORMAT format = DeduceFileFormat(node->str);
		if (format == AS_NONE)
			continue;

		HANDLE hFile = CreateFileW(node->str.str, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		if (hFile == INVALID_HANDLE_VALUE)
		{
			Log16(stdout, "Can't open file %s for reading.\r\n", node->str);			
			continue;
		}

		LARGE_INTEGER fileSize;
		if (!GetFileSizeEx(hFile, &fileSize))
		{
			Log16(stdout, "Can't determine size of file %s", node->str);
			continue;
		}
		else if(fileSize.QuadPart > 0xFFFF)
		{
			Log16(stdout, "File %s exceeds the maximum size of an AS script binary", node->str);
			continue;
		}

		ArenaTemp processingScratchArena = ArenaBeginTemp(arena);
		void* fileContents = ArenaPush(arena, fileSize.QuadPart);
		if (fileContents == NULL)
		{
			WIN_LogStr8(stdout, "Out of memory.");
			return 0;
		}
		
		QWORD totalBytesRead = 0;
		while(totalBytesRead != fileSize.QuadPart)
		{	
			DWORD curBytesRead;
			if (!ReadFile(hFile, ((char*)fileContents + totalBytesRead), (int)fileSize.QuadPart, &curBytesRead, NULL))
			{
				Log16(stdout, "Error reading file %s\r\n", node->str);
				continue;
			}
			totalBytesRead += curBytesRead;
		}
		
		switch(format)
		{
		case AS_DT_ACTOR:
			BOOLEAN succ = ParseAS_ActorBinary(processingScratchArena, fileContents, fileSize.QuadPart, stdout);
			break;
		case AS_DT_MAG:
			break;
		case AS_DT_ITEM:
			break;
		case AS_DT_BS:
			break;

		case AS_SCRIPT_ACTOR:
			break;
		case AS_SCRIPT_MAG:
			break;
		case AS_SCRIPT_ITEM:
			break;
		case AS_SCRIPT_BS:
			break;
		}
		
		WIN_LogStr8(stdout, "%d %S\r\n", format + 0x30, node->str);
		
		ArenaEndTemp(processingScratchArena);
	}
	return 0;
}