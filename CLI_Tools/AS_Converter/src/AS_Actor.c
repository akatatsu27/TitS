#ifndef AS_ACTOR_H
#define AS_ACTOR_H

#include "BinaryReader.h"
#include "BinaryParseContext.h"

#include "AS_Instruction.h"

/*
AS_ACTOR LAYOUT

//HEADER
u16 Procedure Offset Table Start
u16 Procedure Offset Table End
u16 Bones3d Offset
chip_entry[] chcp
//HEADER Terminates with 0xFFFF

//3D MODEL NAMES
Str8[] model_3d //Array of Null-Terminated Str8, terminated by a NULL u8
//3D MODEL NAMES

//Bones3d ONLY IF Header.Bones3dOffset != 0
u8 unk00
Str8[] bones_3d //Array of Null-Terminated Str8, terminated by a NULL u8
//Bones3d

//sprite_offset_table
sprite_offset SpriteOffsets[8]
//sprite_offset_table

//Rest of the file is the instructions of the procedures
*/

#define MIN_PROCEDURE_LABELS 30
#define MAX_PROCEDURE_LABELS 34

#pragma pack(push,1)
typedef struct chip_Binary
{
    u16 index;
    u16 datatable;
} chip_Binary;

typedef struct chip_entry_Binary
{
    chip_Binary ch;
    chip_Binary cp;
} chip_entry_Binary;

typedef struct AS_Actor_Header_Binary
{
	u16 ProcedureOffsetTableStart;
	u16 ProcedureOffsetTableEnd;
	u16 Bones3dOffset;
	chip_entry_Binary chip_entries[];
} AS_Actor_Header_Binary;

typedef struct sprite_offset
{
    u8 horizontal_offset;
    u8 vertical_offset;
} sprite_offset;

typedef struct sprite_offset_table
{
	sprite_offset offsets[8];
} sprite_offset_table;

typedef struct bones
{
    u8 unk00;
} bones;
#pragma pack(pop)

static BOOLEAN ParseAS_ActorBinary(Arena* arena, void* file, size_t size, LOGHANDLE logger)
{
	BOOLEAN hasErrors = false;
	AS_Actor_Header_Binary* header = file;	
	if(size < sizeof(AS_Actor_Header_Binary))
	{
		WIN_LogStr8(logger, "file smaller than header size");
		return false;
	}
	
	if(header->ProcedureOffsetTableStart > size)
	{
		WIN_LogStr8(logger, "Out of bounds Procedure offset table start: %d", header->ProcedureOffsetTableStart);
		hasErrors = true;
	}
	if(header->ProcedureOffsetTableEnd > size)
	{
		WIN_LogStr8(logger, "Out of bounds Procedure offset table end: %d", header->ProcedureOffsetTableEnd);
		hasErrors = true;
	}
	if(header->Bones3dOffset > size)
	{
		WIN_LogStr8(logger, "Out of bounds Bones 3d offset: %d", header->Bones3dOffset);
		hasErrors = true;
	}
	if(hasErrors)
	{
		return false;
	}
	if(header->ProcedureOffsetTableStart > header->ProcedureOffsetTableEnd)
	{
		WIN_LogStr8(logger, "Procedure Offset table Start (%d) higher than End (%d)", header->ProcedureOffsetTableStart, header->ProcedureOffsetTableEnd);
		return false;
	}
	if((header->ProcedureOffsetTableEnd - header->ProcedureOffsetTableStart) % 2 != 0)
	{
		WIN_LogStr8(logger, "Unalligned Procedure Offset table Start (%d) End (%d)", header->ProcedureOffsetTableStart, header->ProcedureOffsetTableEnd);
		return false;
	}
	
	u64 numProcedures = header->ProcedureOffsetTableEnd - header->ProcedureOffsetTableStart / 2;
	if(numProcedures < MIN_PROCEDURE_LABELS
		|| numProcedures > MAX_PROCEDURE_LABELS)
	{
		WIN_LogStr8(logger, "Number of procedures (%d) is bigger than the maximum allowed (%d)", numProcedures, MAX_PROCEDURE_LABELS);
		return false;
	}
	
	BinaryReader reader_ = {. file = file, .size = size, .pos = sizeof(AS_Actor_Header_Binary)};
	BinaryReader* reader = &reader_;
	s8 chcp_count = 0;
	do
	{
		ReadRes_u8 ch = Read_u8(reader);
		if(!ch.success)
		{
			WIN_LogStr8(logger, "End of file reached when reading ch %d", chcp_count + 1);
			return false;
		}
		else if (ch.val == 0xFFFF)
		{
			break;
		}
		ReadRes_u8 cp = Read_u8(reader);
		if(!cp.success)
		{
			WIN_LogStr8(logger, "End of file reached when reading cp %d", chcp_count + 1);
			return false;
		}
		else
		{
			chcp_count += 1;
		}
	} while (1);
	
	ReadRes_Str8CArray model_names = Read_Str8CArray(reader, arena);
	if(!model_names.success)
	{
		WIN_LogStr8(logger, "End of file reached when reading model_names array");
		return false;
	}
	
	bones* bones_3d = NULL;
	ReadRes_Str8CArray bone_names = NULL;
	if(header->Bones3dOffset != 0)
	{
		if(header->Bones3dOffset != reader->pos)
		{
			WIN_LogStr8(logger, "Bones 3d offset (%d) mismatch with offset defined in header (%d)", reader->pos, header->Bones3dOffset);
			return false;
		}
		
		bones_3d = (bones*)((char*)reader->file + reader->pos);
		reader->pos += sizeof(bones);
		if(reader->pos > size)
		{
			WIN_LogStr8(logger, "End of file reached when reading Bones");
			return false;
		}
		bone_names = Read_Str8CArray(reader, arena);
		if(!bone_names.success)
		{
			WIN_LogStr8(logger, "End of file reached when reading bone_names");
			return false;
		}
	}
	
	if(header->ProcedureOffsetTableStart != reader->pos)
	{
		WIN_LogStr8(logger, "Procedure Offset Table Start (%d) mismatch with offset defined in header (%d)", reader->pos, header->ProcedureOffsetTableStart);
		return false;
	}
	u16* procedureOffsets = (u16*)((char*)file + reader->pos);
	for(u64 i = 0; i < numProcedures; i++)
	{
		if(procedureOffsets[i] > size)
		{
			WIN_LogStr8(logger, "Procedure %d start outside the file bounds", i);
			return false;
		}
	}	
	reader->pos += numProcedures * sizeof(u16);
	
	sprite_offset_table* sprite_offsets = (sprite_offset_table*)((char*)file + reader->pos);
	reader->pos += sizeof(sprite_offset_table);
	if(reader->pos > size)
	{
		WIN_LogStr8(logger, "End of file reached when reading sprite table");
		return false;
	}
	
	BinaryParseContext* ctx = PushZero(arena, BinaryParseContext);
		
	u16 instrCount = 0;
	Instruction* instructions = ArenaPush((0xFFFF - reader->pos) * sizeof(Instruction)); //there can be no more than this number of instructions
	//Start of instructions
	while(reader->pos < size)
	{
		ReadRes_Instruction res = ReadInstruction(ctx, arena, reader);
		if(res.success = false)
		{
			return false;
		}
		else
		{
			instructions[instrCount] = res.val;
			instrCount += 1;
		}
	}
}

#endif