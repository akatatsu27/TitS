int _fltused;

#include "BASE_Types.h"
#include "stdio.h"

#define MD_FUNCTION static inline
#define MD_DEFAULT_MEMSET 1
#define MD_DEFAULT_FILE_LOAD 0
#define MD_DEFAULT_BASIC_TYPES 1
#define MD_DEFAULT_SPRINTF 0
#define MD_DISABLE_PRINT_HELPERS 1
#define MD_IMPL_Vsnprintf vsnprintf
#include "md_basic_types.h"
#include "md.h"
#include "md.c"

#include "Arena.h"
#include "Arena.c"

#include "Str.h"
#include "Str.c"
#include "STR_Unicode.h"
#include "STR_Unicode.c"

#include "Windows.h"

#include "WIN_Logging.h"
#include "WIN_Logging.c"

#include "SYN.h"

//bitfield
typedef enum OPERAND_TAG
{
	OPERAND_TAG_None   = 0,
	OPERAND_TAG_target = 1 << 0,
	OPERAND_TAG_offset = 1 << 1,
} OPERAND_TAG;

typedef enum OPERAND_TYPE
{
	OPERAND_TYPE_NONE = 0,
	OPERAND_TYPE_u8,
	OPERAND_TYPE_s8,
	OPERAND_TYPE_u16,
	OPERAND_TYPE_s16,
	OPERAND_TYPE_u32,
	OPERAND_TYPE_s32,
	OPERAND_TYPE_u64,
	OPERAND_TYPE_s64,
	OPERAND_TYPE_f32,
	OPERAND_TYPE_f64,
	OPERAND_TYPE_Str8C,
	OPERAND_TYPE_Str8CArray,
} OPERAND_TYPE;

typedef struct OperandDef
{
	struct OperandDef* prev;
	struct OperandDef* next;
	OPERAND_TAG Tags;
	OPERAND_TYPE Type;
	MD_String8 name;
} OperandDef;

typedef struct InstrDef
{
	struct InstrDef* prev;
	struct InstrDef* next;
	struct InstrDef* childInstructions;	
	MD_String8 name;
	u64 opcode;
	OperandDef* Operands;
} InstrDef;

#define BubbleSortDef(typename)\
typename * typename##_BubbleSort(struct typename* root, b32 compare(typename*, typename*))\
{\
    typename *result = NULL;\
    typename **pp;\
    \
    b32 swapped = (root != NULL);\
    while (swapped)\
    {\
        swapped = false;\
\
        pp = &root;\
        while (*pp && (*pp)->next)\
        {\
			if(compare((*pp)->next, *pp))\
            {\
                typename *p = *pp;\
                *pp = (*pp)->next;\
                p->next = (*pp)->next;\
                (*pp)->next = p;\
                swapped = true;\
            }\
            pp = &(*pp)->next;\
        }\
\
        typename *p = *pp;\
        *pp = NULL;\
\
        p->next = result;\
        result = p;\
    }\
	\
    *pp = result;\
    return root;\
}

b32 InstrDefCmpr(InstrDef* p1, InstrDef* p2)
{
	return p1->opcode < p2->opcode;
}

BubbleSortDef(InstrDef)
//Bubble sort

internal inline u8 MostSignificantByte(u64 n)
{
	u64 i;
	if ((i = n & 0xff00000000000000) != 0)
		return (i >> 56) & 0xff;
	if ((i = n & 0xff000000000000) != 0)
		return (i >> 48) & 0xff;
	if ((i = n & 0xff0000000000) != 0)
		return (i >> 40) & 0xff;
	if ((i = n & 0xff00000000) != 0)
		return (i >> 32) & 0xff;
	if ((i = n & 0xff000000) != 0)
		return (i >> 24) & 0xff;
	if ((i = n & 0xff0000) != 0)
		return (i >> 16) & 0xff;
	if ((i = n & 0xff00) != 0)
		return (i >> 8) & 0xff;
	// all of the higher bytes are zeroes
	return (u8)n;
}
internal inline u8 SecondMostSignificantByte(u64 n)
{
	u64 i;
	if ((i = n & 0xff00000000000000) != 0)
		return (n >> 48) & 0xff;
	if ((i = n & 0xff000000000000) != 0)
		return (n >> 40) & 0xff;
	if ((i = n & 0xff0000000000) != 0)
		return (n >> 32) & 0xff;
	if ((i = n & 0xff00000000) != 0)
		return (n >> 24) & 0xff;
	if ((i = n & 0xff000000) != 0)
		return (n >> 16) & 0xff;
	if ((i = n & 0xff0000) != 0)
		return (n >> 8) & 0xff;
	if ((i = n & 0xff00) != 0)
		return (n >> 0) & 0xff;
	// all of the higher bytes are zeroes
	return (u8)n;
}

char* Tabs[5] = {"", "\t", "\t\t", "\t\t\t", "\t\t\t\t"};

internal inline void DefineInstrStringConstants(Arena* arena, MD_String8List* strbuilder, InstrDef* instr)
{
	Str8 instr_name_fmt = Str8Lit("char s_%S[] = \"%S\";\r\n");
	Str8 instr_name = Str8PushF(arena, instr_name_fmt.str, instr->name, instr->name);
	MD_S8ListPush(arena, strbuilder, instr_name);	
	
	Str8 op_name_fmt = Str8Lit("char s_%S_%S[] = \"%S\";\r\n");
	for(OperandDef* op = instr->Operands; op != NULL; op = op->next)
	{
		Str8 op_name = Str8PushF(arena, op_name_fmt.str, instr->name, op->name, op->name);
		MD_S8ListPush(arena, strbuilder, op_name);
	}
}

internal inline void BeginStruct(Arena* arena, MD_String8List* strbuilder, Str8 typeName, int indentation)
{	
	Str8 fmt = Str8Lit( 
"%sstruct %S\r\n\
%s{\r\n");
	Str8 beninging = Str8PushF(arena, fmt.str, Tabs[indentation], typeName, Tabs[indentation]);
	MD_S8ListPush(arena, strbuilder, beninging);
}

internal inline void BeginTypedefStruct(Arena* arena, MD_String8List* strbuilder, Str8 typeName)
{
	Str8 typedef_str = Str8Lit("typedef ");
	MD_S8ListPush(arena, strbuilder, typedef_str);
	BeginStruct(arena, strbuilder, typeName, 0);
}

Str8 tu8;
Str8 ts8;
Str8 tu16;
Str8 ts16;
Str8 tu32;
Str8 ts32;
Str8 tu64;
Str8 ts64;
Str8 tf32;
Str8 tf64;
Str8 tStr8C;
Str8 tStr8CArray;

internal inline OPERAND_TYPE OperandTypeFromString(Str8 str)
{
	if(Str8Cmp(str, tu8))
		return OPERAND_TYPE_u8;
	else if(Str8Cmp(str, ts8))
		return OPERAND_TYPE_s8;
	else if(Str8Cmp(str, tu16))
		return OPERAND_TYPE_u16;
	else if(Str8Cmp(str, ts16))
		return OPERAND_TYPE_s16;
	else if(Str8Cmp(str, tu32))
		return OPERAND_TYPE_u32;
	else if(Str8Cmp(str, ts32))
		return OPERAND_TYPE_s32;
	else if(Str8Cmp(str, tu64))
		return OPERAND_TYPE_u64;
	else if(Str8Cmp(str, ts64))
		return OPERAND_TYPE_s64;
	else if(Str8Cmp(str, tf32))
		return OPERAND_TYPE_f32;
	else if(Str8Cmp(str, tf64))
		return OPERAND_TYPE_f64;
	else if(Str8Cmp(str, tStr8C))
		return OPERAND_TYPE_Str8C;
	else if(Str8Cmp(str, tStr8CArray))
		return OPERAND_TYPE_Str8CArray;
	else
		return OPERAND_TYPE_NONE;
}

internal inline Str8 OperandTypeToString(OPERAND_TYPE t)
{
	Str8 type;
	
	switch(t)
	{
	case OPERAND_TYPE_u8:
		type = tu8;
		break;
	case OPERAND_TYPE_s8:
		type = ts8;
		break;
	case OPERAND_TYPE_u16:
		type = tu16;
		break;
	case OPERAND_TYPE_s16:
		type = ts16;
		break;
	case OPERAND_TYPE_u32:
		type = tu32;
		break;
	case OPERAND_TYPE_s32:
		type = ts32;
		break;
	case OPERAND_TYPE_u64:
		type = tu64;
		break;
	case OPERAND_TYPE_s64:
		type = ts64;
		break;
	case OPERAND_TYPE_f32:
		type = tf32;
		break;
	case OPERAND_TYPE_f64:
		type = tf64;
		break;
	case OPERAND_TYPE_Str8C:
		type = tStr8C;
		break;
	case OPERAND_TYPE_Str8CArray:
		type = tStr8CArray;
		break;
	}
	
	return type;
}

internal inline void DefineField(Arena* arena, MD_String8List* strbuilder, Str8 type, Str8 name, int indentation)
{
	Str8 fmt = Str8Lit("%s%S %S;\r\n");
	Str8 field = Str8PushF(arena, fmt.str, Tabs[indentation], type, name);
	MD_S8ListPush(arena, strbuilder, field);
}

internal inline void DefineOperand(Arena* arena, MD_String8List* strbuilder, OperandDef* field)
{	
	Str8 type = OperandTypeToString(field->Type);
	Str8 name = field->name;	
	DefineField(arena, strbuilder, type, name, 1);
}
void EndStruct(MD_Arena* arena, MD_String8List* strbuilder, MD_String8 name, int indentation)
{
	Str8 fmt = Str8Lit("%s} %S;\r\n");
	Str8 ending = Str8PushF(arena, fmt.str, Tabs[indentation], name, Tabs[indentation]);
	MD_S8ListPush(arena, strbuilder, ending);
}

InstrDef* ByOpcodeFirstByte[0xFF] = {0};
internal inline BOOLEAN ParseMdeskDefs(MD_Arena* arena, LOGHANDLE logFile, MD_ParseResult tree)
{
	BOOLEAN hasErred = false;
	for (MD_EachNode(node, tree.node->first_child))
	{
		BOOLEAN invalidInstr = false;
		MD_ArenaTemp marker = MD_ArenaBeginTemp(arena);
			
		InstrDef* instr = PushArrayZero(arena, InstrDef, 1);		
		instr->name = node->string;				
		//Validate that there are no duplicate instruction names 
		for (MD_EachNode(sibling_node, tree.node->first_child))
		{
			if(sibling_node != node && Str8Cmp((sibling_node->string), (instr->name)))
			{
				WIN_LogStr8(logFile, "\"%S\": Multiple definitions\r\n", instr->name);
				invalidInstr = true;
			}
		}	
		
		Str8 opcode_str = MD_S8Lit("opcode");
		BOOLEAN has_opcode_field = false;
		
		OperandDef* firstOper = NULL;
		OperandDef* lastOper = NULL;
		for (MD_EachNode(member_node, node->first_child))
		{
			BOOLEAN invalidOperand = false;
			
			Str8 field_name = member_node->string;
			Str8 field_val_str = member_node->first_child->string;
			//Validate that there are no duplicate member names 
			for (MD_EachNode(sibling_node, node->first_child))
			{
				if(sibling_node != member_node && Str8Cmp((sibling_node->string), (field_name)))
				{
					WIN_LogStr8(logFile, "\"%S\": Multiple definitions of member: \"%S\".\r\n", instr->name, field_name);
					invalidOperand = true;
				}
			}

			//Is it the opcode node?
			if(Str8Cmp(field_name, opcode_str))
			{
				has_opcode_field = true;
				Str8_atoiURes opcode = Str8_atoiU(field_val_str);
				if (opcode.success == false)
				{
					WIN_LogStr8(logFile, "\"%S\": \"opcode\" member is not a constant u64 number.\r\n", instr->name);
					invalidOperand = true;
				}
				else
				{
					instr->opcode = opcode.result;
				}
				
				continue;
			}
			else
			{
				OperandDef* oper = MD_PushArrayZero(arena, OperandDef, 1);
				oper->name = field_name;
				if(!firstOper)
				{
					instr->Operands = oper;
					firstOper = oper;
					lastOper = oper;
				}
				else
				{
					lastOper->next = oper;
					oper->prev = lastOper;
					lastOper = oper;
				}
				//enumerate tags
				Str8 tTarget = MD_S8Lit("target");
				Str8 tOffset = MD_S8Lit("offset");
				for (MD_EachNode(tag_node, member_node->first_tag))
				{
					Str8 tagName = tag_node->string;
					if(Str8Cmp(tagName, tTarget))
					{
						oper->Tags |= OPERAND_TAG_target;
					}
					else if(Str8Cmp(tagName, tOffset))
					{
						oper->Tags |= OPERAND_TAG_offset;
					}
				}
				
				oper->Type = OperandTypeFromString(field_val_str);
				if(oper->Type == OPERAND_TYPE_NONE)
				{
					WIN_LogStr8(logFile, "\"%S\":\"%S\": \"%S\" is not a valid operand type\r\n", instr->name, oper->name, field_val_str);
					invalidOperand = true;
				}
				
				//validate tags
				if(oper->Type != OPERAND_TYPE_NONE)
				{
					if(oper->Tags & OPERAND_TAG_offset)
					{
						if(oper->Type != OPERAND_TYPE_u16)
						{
							WIN_LogStr8(logFile, "\"%S\":\"%S\": @offset operands must be u16\r\n", instr->name, oper->name);
							invalidOperand = true;
						}
					}
					if(oper->Tags & OPERAND_TAG_target)
					{
						if(oper->Type != OPERAND_TYPE_u8)
						{
							WIN_LogStr8(logFile, "\"%S\":\"%S\": @target operands must be u8\r\n", instr->name, oper->name);
							invalidOperand = true;
						}
					}
				}
			}
			
			invalidInstr |= invalidOperand;
		}
		if(!has_opcode_field)
		{
			WIN_LogStr8(logFile, "\"%S\": No \"opcode\" member.\r\n", node->string);
			invalidInstr = true;
		}
		
		
		if(invalidInstr)
		{
			MD_ArenaEndTemp(marker);
		}
		else
		{
			u8 firstByte = MostSignificantByte(instr->opcode);
			InstrDef* sibling = ByOpcodeFirstByte[firstByte];
			if(sibling)
			{
				sibling->prev = instr;
				instr->next = sibling;
			}
			ByOpcodeFirstByte[firstByte] = instr;
		}
		
		hasErred |= invalidInstr;
	}
	return hasErred;
}

internal inline void GenerateStructs(MD_Arena* arena, MD_String8List* strbuilder)
{
	//Generate the Instruction structs and the related string constants	
	for(u8 i = 0; i < 0xFF; i++)
	{
		InstrDef* instr = ByOpcodeFirstByte[i];
		if(!instr)
			continue;
		
		for(; instr != NULL; instr = instr->next)
		{
			DefineInstrStringConstants(arena, strbuilder, instr);
			BeginTypedefStruct(arena, strbuilder, instr->name);
			for(OperandDef* op = instr->Operands; op != NULL; op = op->next)
			{
				DefineOperand(arena, strbuilder, op);
			}
			EndStruct(arena, strbuilder, instr->name, 0);
		}
	}
	//Generate the union instruction struct
	MD_String8 Instruction = MD_S8Lit("Instruction");
	MD_String8 Instruction_ptr = MD_S8Lit("struct Instruction*");
	BeginTypedefStruct(arena, strbuilder, Instruction);
	DefineField(arena, strbuilder, tu16, MD_S8Lit("offset"), 1);
	DefineField(arena, strbuilder, tu64, MD_S8Lit("opcode"), 1);
	
	MD_S8ListPush(arena, strbuilder, MD_S8Lit("\tunion\r\n\t{\r\n"));
	for(u8 i = 0; i < 0xFF; i++)
	{
		InstrDef* instr = ByOpcodeFirstByte[i];
		if(!instr)
			continue;
		
		for(; instr != NULL; instr = instr->next)
		{
			DefineField(arena, strbuilder, instr->name, instr->name, 2);			
		}
	}
	MD_S8ListPush(arena, strbuilder, MD_S8Lit("\t};\r\n"));
	EndStruct(arena, strbuilder, Instruction, 0);
}
internal inline void GenerateReadInstrSpecific(MD_Arena* arena, MD_String8List* strbuilder)
{
	//Foreach instruction
	for(u8 i = 0; i < 0xFF; i++)
	{
		InstrDef* instr = ByOpcodeFirstByte[i];
		if(!instr)
			continue;
		
		for(; instr != NULL; instr = instr->next)
		{
			Str8 beginReadFunc = Str8PushF(arena,
"internal inline ReadRes_Instruction Read_%S(BinaryParseContext* ctx, Arena* arena, BinaryReader* reader)\r\n\
{\r\n\
\tu16 pos = reader->pos;\r\n",
			instr->name);
			MD_S8ListPush(arena, strbuilder, beginReadFunc);
			
			Str8 lastField;
			for(OperandDef* op = instr->Operands; op != NULL; op = op->next)
			{
				Str8 typeName = OperandTypeToString(op->Type);				
				Str8 readResType = Str8PushF(arena, "\tReadRes_%S r_%S = Read_%S(reader);\r\n", typeName, op->name, typeName);
				MD_S8ListPush(arena, strbuilder, readResType);
				
				lastField = op->name;					
			}
			if(instr->Operands)
			{				
				Str8 returnIfReadFail = Str8PushF(arena,
"\tif(!r_%S.success)\r\n\
\t\treturn (ReadRes_Instruction){false, (Instruction){0}};\r\n\r\n", lastField);
				MD_S8ListPush(arena, strbuilder, returnIfReadFail);
			}				

			for(OperandDef* op = instr->Operands; op != NULL; op = op->next)
			{
				if(op->Tags & OPERAND_TAG_offset)
				{
					MD_String8 labelTarget = Str8PushF(arena, "\tctx->OffsetFlags[r_%S.val] &= OffsetFlag_LabelTarget;\r\n", op->name);	
					MD_S8ListPush(arena, strbuilder, labelTarget);
				}
				if(op->Tags & OPERAND_TAG_target)
				{
					MD_String8 actorTarget= Str8PushF(arena, "\tctx->Targets[r_%S.val] = 1;\r\n", op->name);
					MD_S8ListPush(arena, strbuilder, actorTarget);
				}
			}
			
			Str8 retSuccessBegin = Str8PushF(arena,
"\treturn (ReadRes_Instruction)\r\n\
\t{\r\n\
\t\t.success = true,\r\n\
\t\t.val = (Instruction)\r\n\
\t\t{\r\n\
\t\t\t.offset = pos,\r\n\
\t\t\t.%S = (%S)\r\n\
\t\t\t{\r\n",
			instr->name, instr->name);
			MD_S8ListPush(arena, strbuilder, retSuccessBegin);
			
			for(OperandDef* op = instr->Operands; op != NULL; op = op->next)
			{
				MD_String8 setFieldVal = Str8PushF(arena, "\t\t\t\t.%S = r_%S.val,\r\n", op->name, op->name);
				MD_S8ListPush(arena, strbuilder, setFieldVal);	
			}
			
			MD_String8 func_end = Str8Lit("\t\t\t}\r\n\t\t}\r\n\t};\r\n}\r\n");
			MD_S8ListPush(arena, strbuilder, func_end);
		}
	}
}

internal inline void GenerateSwitchOnOpcode(MD_Arena* arena, MD_String8List* strbuilder,
	Str8 getOpcodeAction, Str8 getSecondByteAction,
	Str8 caseAction, Str8 failAction)
{
	MD_S8ListPush(arena, strbuilder, getOpcodeAction);
	Str8 opCodeSwitchStart = Str8Lit(
"\tswitch(opcode)\r\n\
\t{\r\n\
");
	Str8 opCodeSwitchEnd = Str8Lit("\r\n\t}");
	Str8 funcEnd = Str8Lit("\r\n}\r\n");
	MD_S8ListPush(arena, strbuilder, opCodeSwitchStart);
	
	
	Str8 case_fmt = Str8Lit("\tcase %#04x:\r\n\t{\r\n");
	Str8 case_end = Str8Lit("\t}\r\n");
	Str8 subswitch_start = Str8Lit("\t\tswitch(secondByte)\r\n\t\t{\r\n");
	Str8 subswitch_end = Str8Lit("\t\t}\r\n");
	Str8 subcase_end = Str8Lit("\t\t}\r\n");
	for(u8 i = 0; i < 0xFF; i++)
	{
		InstrDef* instr = ByOpcodeFirstByte[i];
		if(!instr)
			continue;
		
		u8 firstByte = MostSignificantByte(instr->opcode);
		
		Str8 caseStr = Str8PushF(arena, case_fmt.str, firstByte);
		MD_S8ListPush(arena, strbuilder, caseStr);
		
		if(instr->next != NULL)
		{
			InstrDef* defaultCase = NULL;
			MD_S8ListPush(arena, strbuilder, getSecondByteAction);
			MD_S8ListPush(arena, strbuilder, subswitch_start);
			for(; instr != NULL; instr = instr->next)
			{
				if(instr->opcode <= 0xFF)
				{
					defaultCase = instr;
					continue;
				}
				else
				{
					u8 secondByte = SecondMostSignificantByte(instr->opcode);
					MD_String8 subcaseStart = Str8PushF(arena, "\t\tcase %#04x:\r\n\t\t{\r\n\t", secondByte);
					MD_String8 subCaseAction = Str8PushF(arena, caseAction.str, instr->name);
					
					MD_S8ListPush(arena, strbuilder, subcaseStart);
					MD_S8ListPush(arena, strbuilder, subCaseAction);
					MD_S8ListPush(arena, strbuilder, subcase_end);
				}					
			}
			MD_S8ListPush(arena, strbuilder, subswitch_end);
			
			if(defaultCase != NULL)
			{
				MD_String8 defaultRead = Str8PushF(arena, caseAction.str, defaultCase->name);
				MD_S8ListPush(arena, strbuilder, defaultRead);
			}
		}
		else
		{
			MD_String8 justRead = Str8PushF(arena, caseAction.str, instr->name);
			MD_S8ListPush(arena, strbuilder, justRead);
		}		
		
		MD_S8ListPush(arena, strbuilder, case_end);
	}
	
	////
	MD_S8ListPush(arena, strbuilder, opCodeSwitchEnd);
	MD_S8ListPush(arena, strbuilder, failAction);
	MD_S8ListPush(arena, strbuilder, funcEnd);
	
}
internal inline void GenerateReadInstrAll(MD_Arena* arena, MD_String8List* strbuilder)
{
	MD_String8 readInstrFunc_begin = MD_S8Lit(
"internal inline ReadRes_Instruction ReadInstruction(BinaryParseContext* ctx, Arena* arena, BinaryReader* reader)\r\n\
{\r\n\
\tctx->OffsetFlags[reader] &= OffsetFlag_StartOfInstruction;\r\n\
\tReadRes_u8 r_opcode = Read_u8(reader);\r\n\
\tif(!r_opcode.success)\r\n\
\t\treturn (ReadRes_Instruction){false, (Instruction){}};\r\n\
\r\n\
");
	
	MD_S8ListPush(arena, strbuilder, readInstrFunc_begin);
	GenerateSwitchOnOpcode(arena, strbuilder,
		MD_S8Lit("\tu8 opcode = r_opcode.val;\r\n\r\n"), MD_S8Lit("\t\tu8 secondByte = Peek_u8(ctx, 0);\r\n"),
		MD_S8Lit("\t\treturn Read_%S(ctx, arena, reader);\r\n"), MD_S8Lit("\r\n\treturn (ReadRes_Instruction){false, (Instruction){}};"));
		
}
internal inline void GenerateValidateInstrSpecific(MD_Arena* arena, MD_String8List* strbuilder)
{
	//Foreach instruction
	for(u8 i = 0; i < 0xFF; i++)
	{
		InstrDef* instr = ByOpcodeFirstByte[i];
		if(!instr)
			continue;
		
		for(; instr != NULL; instr = instr->next)
		{
			Str8 beginReadFunc = Str8PushF(arena,
"internal inline BOOLEAN Validate_%S(BinaryParseContext* ctx, Instruction* instr, LOGHANDLE logger)\r\n\
{\r\n\
\tBOOLEAN invalid = false;\r\n",
			instr->name);
			MD_S8ListPush(arena, strbuilder, beginReadFunc);

			for(OperandDef* op = instr->Operands; op != NULL; op = op->next)
			{
				if(op->Tags & OPERAND_TAG_offset)
				{
					MD_String8 validateOffsetField = Str8PushF(arena, 
"\tif(!(ctx->OffsetFlags[instr.%S] & OffsetFlag_StartOfInstruction))\r\n\
\t{\r\n\
\t\tinvalid = true;\r\n\
\t\tWIN_LogStr8(InvalidOffset, s_%S_%S, s_%S, instr.offset, instr.%S);\r\n\
\t}\r\n\
", op->name, instr->name, op->name, instr->name, op->name);
					MD_S8ListPush(arena, strbuilder, validateOffsetField);
				}
				if(op->Tags & OPERAND_TAG_target)
				{
					
				}
			}
			
			MD_String8 func_end = Str8Lit("\treturn !invalid;\r\n}\r\n");
			MD_S8ListPush(arena, strbuilder, func_end);
		}
	}
}
internal inline void GenerateValidateInstrAll(MD_Arena* arena, MD_String8List* strbuilder)
{
	Str8 validateInstrFunc_begin = MD_S8Lit(
"internal inline BOOLEAN ValidateInstruction(BinaryParseContext* ctx, Instruction* instr, LOGHANDLE logger)\r\n\
{\r\n");
	MD_S8ListPush(arena, strbuilder, validateInstrFunc_begin);
	GenerateSwitchOnOpcode(arena, strbuilder,
		MD_S8Lit("\tu8 opcode = (u8)instr->opcode;\r\n\r\n"), MD_S8Lit("\t\tu8 secondByte = (u8)(instr->opcode >> 1);\r\n"),
		MD_S8Lit("\t\treturn Validate_%S(ctx, instr, logger);\r\n"), MD_S8Lit("\r\n\treturn FALSE;"));
}
int __stdcall mainCRTStartup()
{
	LARGE_INTEGER frequency;
    LARGE_INTEGER start;
    LARGE_INTEGER end;
    double interval;
	
	tu8	= MD_S8Lit("u8");
	ts8	= MD_S8Lit("s8");
	tu16	= MD_S8Lit("u16");
	ts16	= MD_S8Lit("s16");
	tu32	= MD_S8Lit("u32");
	ts32	= MD_S8Lit("s32");
	tu64	= MD_S8Lit("u64");
	ts64	= MD_S8Lit("s64");
	tf32	= MD_S8Lit("f32");
	tf64	= MD_S8Lit("f64");
	tStr8C =	MD_S8Lit("Str8C");
	tStr8CArray = MD_S8Lit("Str8CArray");
	
	MD_Arena* arena = MD_ArenaAlloc();
	if(!arena)
		return -1;
	
	u32 numWritten;
	HANDLE stdout_ = CreateFileA("CON", GENERIC_WRITE, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);	
	LOGHANDLE stdout = WIN_CreateLogHandle(stdout_);
	MD_String8 fname = MD_S8Lit("AS_Instruction.mdesk");
	HANDLE hFile = CreateFileA(fname.str, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	
	LARGE_INTEGER fileSize;	
	GetFileSizeEx(hFile, &fileSize);
	
	char* contents = MD_ArenaPush(arena, fileSize.QuadPart);
	DWORD curBytesRead;
	ReadFile(hFile, contents, fileSize.QuadPart, &curBytesRead, NULL);
	MD_String8 conts = {.str = contents, .size = curBytesRead };
	MD_ParseResult parse = MD_ParseWholeString(arena, fname, conts);
	
	QueryPerformanceFrequency(&frequency);
    QueryPerformanceCounter(&start);
	
	BOOLEAN hasErred = ParseMdeskDefs(arena, stdout, parse);
	
	if(hasErred)
	{
		return 0;
	}
	
	for(u8 i = 0; i < 0xFF; i++)
	{
		InstrDef* instr = ByOpcodeFirstByte[i];
		if(!instr)
			continue;
		
		ByOpcodeFirstByte[i] = InstrDef_BubbleSort(instr, InstrDefCmpr);
	}	
	
	MD_ArenaTemp scratch = MD_GetScratch(NULL, 0);
	MD_String8List strbuilder = {.node_count = 0, .total_size = 0, .first = NULL, .last = NULL};
	//Put constants on top
	MD_String8 consts = MD_S8Lit(
"\
CHAR InvalidOffset[] = \"The operand %S of the %S instruction at offset %#4X points to invalid offset %#4X\";\r\n\
\
"
);
	MD_S8ListPush(scratch.arena, &strbuilder, consts);
	
	GenerateStructs(scratch.arena, &strbuilder);
	
	//Generate the read and write functions
	MD_String8 readRes = MD_S8Lit(
"typedef struct ReadRes_Instruction\r\n\
{\r\n\
	b32 success;\r\n\
	Instruction val;\r\n\
} ReadRes_Instruction;\r\n");

	MD_S8ListPush(scratch.arena, &strbuilder, readRes);
	
	GenerateReadInstrSpecific(scratch.arena, &strbuilder);
	
	GenerateReadInstrAll(scratch.arena, &strbuilder);
	
	GenerateValidateInstrSpecific(scratch.arena, &strbuilder);
	
	GenerateValidateInstrAll(scratch.arena, &strbuilder);
	
	//write to file
	MD_String8 result = MD_S8ListJoin(arena, strbuilder, NULL);
	MD_ReleaseScratch(scratch);
	
	QueryPerformanceCounter(&end);
    interval = (double) (end.QuadPart - start.QuadPart) / frequency.QuadPart;

	WIN_LogStr8(stdout, "%f\r\n", interval);
	
	HANDLE outFile = CreateFileA("./src/AS_Instruction.h", GENERIC_READ | GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	WriteFile(outFile, result.str, result.size, &numWritten, NULL);
	
	return 0;
}