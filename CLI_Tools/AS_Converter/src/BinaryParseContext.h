#ifndef BPC_H
#define BPC_H

#include "BASE_Types.h"

typedef enum OffsetFlag //: u8
{
	OffsetFlag_None = 				0,
	OffsetFlag_StartOfInstruction = 1 << 0,
	OffsetFlag_LabelTarget = 		1 << 1,
} OffsetFlag;


typedef struct BinaryParseContext
{
	u8 Targets[sizeof(u8)]; //A target of an instruction is specified using an 8-bit value, so we cannot exceed 255 unique target names
	u8 OffsetFlags[0xFFFF]; //there cannot be more than 0xFFFF instructions, as those would be outside of the addressable range of the AS script
} BinaryParseContext;
#endif BPC_H