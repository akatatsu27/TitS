extern int _fltused;

#include <windows.h>

#include "StackArena.c"

#define MD_S8Lit(s) (MD_String8){(CHAR *)(s), sizeof(s)-1}
#define MD_S16Lit(s) (MD_String16){(wchar_t *)(s), sizeof(s)-1}
typedef unsigned long long QWORD;

typedef struct MD_String8
{
    CHAR *str;
    QWORD size;
} MD_String8;

typedef struct MD_String16
{
    wchar_t *str;
    QWORD size;
} MD_String16;



size_t
strlen(const char *str)
{
        const char *s;

        for (s = str; *s; ++s)
                ;
        return (s - str);
}

size_t
strlenW(const wchar_t *str)
{
        const wchar_t *s;
		size_t c = 0;
        for (s = str; *s; ++s)
                c += 2;
        return c;
}

void blockcpy(void* restrict dest, void* restrict src, size_t srcSize) //I trust you bro that you can fit me
{
	for(size_t i = 0; i < srcSize; i++)
	{
		*((char*)dest + i) = *((char*)src + i);
	}
}

char usage[] =
"DarDitExtract v1.0\r\n\
-------------\r\n\
Usage: [PATH...] [-OPTIONAL_OUT]\r\n\
PATH: One or more folder and/or files to try to extract all TitS .dat .dir assets\r\n\
OPTIONAL_OUT: By default the program will create a subfolder in the folder of this executable for each .dir found.\r\n\
Set this argument to change the folder in which the subfolders will be created.\r\n";

//struct IntStr{ int argc; MD_String8* argv; }
//get_command_line_args(StackArena* arena)
//{
//	int argc;
//	MD_String8* argv;
//  // Get the command line arguments as wchar_t strings
//  wchar_t ** wargv = CommandLineToArgvW(GetCommandLineW(), &argc );
//  if (wargv == NULL)
//	return (struct IntStr){0, NULL};
//
//  argv = StackArena_Alloc(arena, sizeof(MD_String8) * argc);
//  for (int i = 0;  i < argc;  i++)
//  {
//	int count = strlenW(wargv[i]);
//	//commit 4 bytes per wchar, because we have infinite memory
//	int bufferSize = 4 * count;
//	void* buff = StackArena_Alloc(arena, bufferSize);
//	int len = WideCharToMultiByte( CP_UTF8, 0, wargv[i], count, buff, bufferSize, NULL, NULL);
//	argv[i] = (MD_String8){.str = buff, .size = len};
//  }
//
//  return (struct IntStr){argc, argv};
//}

typedef struct Str16Node
{
	struct Str16Node* prev;
	size_t size;
	wchar_t str[];
} Str16Node;

wchar_t* outputFolder;

wchar_t biggerPaths[] = u"\\\\?\\";
wchar_t filesInDirectory[] = u"\\*";

int __stdcall mainCRTStartup()
{
	MD_String8 usgae = MD_S8Lit(usage);
	MD_String8 noInput = MD_S8Lit("No input path specified\r\n");
	MD_String8 invalidPath = MD_S8Lit(" is not a valid file or directory path\r\n");
	MD_String8 invalidFolder = MD_S8Lit(" is not a valid directory path\r\n");
	MD_String8 ignoreExtra = MD_S8Lit("ignoring unknown arguments");
	StackArena arena = StackArena_Create(((u64)(4 * 1024 * 1024) * 1024), (u64)(1 * 1024 * 1024)); //reserve 4gb, commit 1mb
	int argc;
	wchar_t ** wargv = CommandLineToArgvW(GetCommandLineW(), &argc);
	DWORD numWritten;
	HANDLE stdout = CreateFileA("CON", GENERIC_WRITE, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	//SetConsoleOutputCP(CP_UTF8);
	u64 startPos = arena.pos;
	if(argc == 1)
	{
		WriteFile(stdout, usgae.str, usgae.size, &numWritten, NULL);
		return 0;
	}
	Str16Node* prevNode = NULL;
	for(int i = 1; i < argc; i++)
	{
		if(*wargv[i] == u'-')
		{
			if(i == 1)
			{
				WriteFile(stdout, noInput.str, noInput.size, &numWritten, NULL);
				return 0;
			}

			outputFolder = wargv[i] + 1;
			DWORD outFolderAttr = GetFileAttributesW(outputFolder);
			if(!(outFolderAttr & FILE_ATTRIBUTE_DIRECTORY))
			{
				WriteFile(stdout, outputFolder, strlenW(outputFolder), &numWritten, NULL);
				WriteFile(stdout, invalidFolder.str, invalidFolder.size, &numWritten, NULL);
				return 0;
			}

			if(i + 1 != argc)
			{
				WriteFile(stdout, ignoreExtra.str, ignoreExtra.size, &numWritten, NULL);
			}
			break;
		}
		DWORD fileAttr = GetFileAttributesW(wargv[i]);
		size_t wargsize = strlenW(wargv[i]);
		if(fileAttr == INVALID_FILE_ATTRIBUTES)
		{
			WriteFile(stdout, wargv[i], strlenW(wargv[i]), &numWritten, NULL);
			WriteFile(stdout, invalidPath.str, invalidPath.size, &numWritten, NULL);
			return 0;
		}
		else if(fileAttr & FILE_ATTRIBUTE_DIRECTORY)
		{
			size_t pathBuffSize = sizeof(biggerPaths) + wargsize + sizeof(filesInDirectory);
			wchar_t* folderSearch = StackArena_Alloc(&arena, pathBuffSize);
			blockcpy(folderSearch, biggerPaths, sizeof(biggerPaths));
			blockcpy((char*)folderSearch + sizeof(biggerPaths), wargv[i], wargsize);
			blockcpy((char*)folderSearch + sizeof(biggerPaths) + wargsize, filesInDirectory, sizeof(filesInDirectory));
			WriteFile(stdout, folderSearch, pathBuffSize, &numWritten, NULL);
			//hFind = FindFirstFileW(L"C:\\Users\\sallen\\Desktop\\Folder1\\*", &FindFileData);
			//if (hFind == INVALID_HANDLE_VALUE)
			//{
			//	fileError = GetLastError();
			//	if (fileError == ERROR_FILE_NOT_FOUND)
			//	{
			//		fileError = 0;
			//		MessageBox(NULL, L"No files were found.", L"File Search Error", MB_OK | MB_ICONWARNING);
			//	}
			//	else
			//	{
			//		wsprintfW(file_buff, L"Error finding files. %d", fileError);
			//		MessageBox(hWnd, file_buff, L"File Search Error", MB_OK | MB_ICONERROR);
			//	}
			//	return fileError;
			//}
			//
			//do
			//{
			//	if ((FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == 0)
			//	{
			//		MessageBox(hWnd, FindFileData.cFileName, L"File Name", MB_OK | MB_ICONINFORMATION);
			//	}
			//}
			//while (FindNextFile(hFind, &FindFileData));
			//
			//fileError = GetLastError();
			//FindClose(hFind);
			//
			//if (fileError != ERROR_NO_MORE_FILES)
			//{
			//	wsprintfW(file_buff, L"Error finding files: %d", fileError);
			//	MessageBox(hWnd, file_buff, L"File Search Error", MB_OK | MB_ICONERROR);
			//}
			//else
			//	fileError = 0;
		}
		else
		{
			Str16Node* node = StackArena_Alloc(&arena, sizeof(Str16Node) + sizeof(biggerPaths) + wargsize);
			node->size = wargsize;
			node->prev = prevNode;
			prevNode = node;
			node->str = ;
			blockcpy(node->str, &biggerPaths, sizeof(biggerPaths));
			blockcpy((char*)(node->str + sizeof(biggerPaths)), wargv[i], wargsize);
		}
	}
	WriteFile(stdout, "\r\n", 2, &numWritten, NULL);

	return 0;
}