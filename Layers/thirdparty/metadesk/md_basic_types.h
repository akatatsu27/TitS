#ifndef MD_TYPES_H

#define MD_TYPES_H

//~ Set default values for controls
#if !defined(MD_DEFAULT_BASIC_TYPES)
# define MD_DEFAULT_BASIC_TYPES 1
#endif

#if defined(MD_DEFAULT_BASIC_TYPES)

#include <stdint.h>
typedef int8_t   MD_i8;
typedef int16_t  MD_i16;
typedef int32_t  MD_i32;
typedef int64_t  MD_i64;
typedef uint8_t  MD_u8;
typedef uint16_t MD_u16;
typedef uint32_t MD_u32;
typedef uint64_t MD_u64;
typedef float    MD_f32;
typedef double   MD_f64;

#endif

typedef MD_i8  MD_b8;
typedef MD_i16 MD_b16;
typedef MD_i32 MD_b32;
typedef MD_i64 MD_b64;

//~ Basic Unicode string types.

typedef struct MD_String8 MD_String8;
struct MD_String8
{
    MD_u8 *str;
    union
	{
		MD_u64 size;
		MD_u64 length;
		MD_u64 count;
	};
};

typedef struct MD_String16 MD_String16;
struct MD_String16
{
    MD_u16 *str;
    union
	{
		MD_u64 size;
		MD_u64 length;
		MD_u64 count;
	};
};

typedef struct MD_String32 MD_String32;
struct MD_String32
{
    MD_u32 *str;
    MD_u64 size;
};

typedef struct MD_String8Node MD_String8Node;
struct MD_String8Node
{
    MD_String8Node *next;
    MD_String8 string;
};

typedef struct MD_String8List MD_String8List;
struct MD_String8List
{
    MD_u64 node_count;
    MD_u64 total_size;
    MD_String8Node *first;
    MD_String8Node *last;
};

typedef struct MD_StringJoin MD_StringJoin;
struct MD_StringJoin
{
    MD_String8 pre;
    MD_String8 mid;
    MD_String8 post;
};

#endif MD_TYPES_H