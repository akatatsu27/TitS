#ifndef WIN_FILEFUNCS_H
#define WIN_FILEFUNCS_H

//BASE
#include "BASE_Types.h"
#include "BASE_Misc.h"

//STD
#include "stdio.h"

//STR
#include "Str.h"
#include "STR_Unicode.h"


#include "Windows.h"
#include "WIN_Logging.h"

#include "WIN.h"

internal wchar biggerPaths[] = u"\\\\?\\";
internal wchar filesInDirectory[] = u"\\*";

//In fully qualified paths we can use the extended maximum path size of 32767 characters
WIN_FUNCTION b32 WIN_CanAddPathPrefix(Str16C path);
WIN_FUNCTION Str16C WIN_SanitizePath(Arena* arena, Str16C path);
WIN_FUNCTION Str16C WIN_DirectorySearchPath(Arena* arena, Str16C path);
WIN_FUNCTION Str16C WIN_DirectoryPlusFile(Arena* arena, Str16C dir, Str16C file);

//return false if encountered an error
WIN_FUNCTION b32 WIN_EnumerateFiles(Arena* arena, LOGHANDLE logFile, Str16CNode** list, Str16C argPath);
WIN_FUNCTION Str16C WIN_GetFileNameWithExtension(Str16C filePath);

//DOES NOT INCLUDE THE DOT
WIN_FUNCTION Str16C WIN_GetFileExtension(Str16C filePath);
WIN_FUNCTION Str16 WIN_GetFileNameWithoutExtension(Str16C filePath);
WIN_FUNCTION b32 WIN_FileExists(wchar* szPath);
#endif //WIN_FILEFUNCS_H