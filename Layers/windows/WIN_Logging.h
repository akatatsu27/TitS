#ifndef WIN_LOGGING_H
#define WIN_LOGGING_H

//BASE
#include "BASE_Types.h"

//STD
#include "string.h"
#include "stdlib.h"

//ARENA
#include "Arena.h"

//STR
#include "Str.h"

#include "Windows.h"
#include "WIN.h"

typedef struct LOGHANDLE
{
	HANDLE hFile;
	HANDLE hMutex;
	
} LOGHANDLE;

WIN_FUNCTION LOGHANDLE WIN_CreateLogHandle(HANDLE hFile);
WIN_FUNCTION void WIN_CloseLogHandle(LOGHANDLE logger);
WIN_FUNCTION void WIN_LogStr8(LOGHANDLE logger, char* fmt, ...);

#endif //WIN_LOGGING_H