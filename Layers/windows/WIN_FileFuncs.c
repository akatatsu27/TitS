#include "WIN_FileFuncs.h"

//In fully qualified paths we can use the extended maximum path size of 32767 characters
WIN_FUNCTION b32 CanAddPathPrefix(Str16C path)
{
	if (path.length <= 3)
		return false;

	//check for full local or UNC path
	if (path.str[1] == u':' || (path.str[0] == u'\\' && path.str[0] == u'\\'))
	{
		if (path.length > 3 && path.str[2] == u'?') //already has the \\? prefix
			return false;
		else
			return true;
	}
	else
		return false;
}

WIN_FUNCTION Str16C WIN_SanitizePath(Arena* arena, Str16C path)
{	
	if (!CanAddPathPrefix(path))
		return path;

	Str16 bigger = Str16Lit(biggerPaths);
	
	//Don't include the u'\0' at the end of the biggerPaths string
	size_t pathBuffSize = Str16Size(bigger) + Str16Size(path);
	WCHAR* extendedPath = ArenaPush(arena, pathBuffSize);
	memcpy(extendedPath, bigger.str, Str16Size(bigger));
	memcpy((char*)extendedPath + Str16Size(bigger), path.str, Str16Size(path));
	Str16C newPath = (Str16C){ .str = extendedPath, .length = (pathBuffSize >> 1) };

	//Convert '/' to '\' as we can't use '/' when using the "\\?\" prefix
	for (size_t i = (bigger.length + 2); i < newPath.length; i++)
	{
		if (newPath.str[i] == u'/')
			newPath.str[i] = u'\\';
	}
	return newPath;
}

WIN_FUNCTION Str16C WIN_DirectorySearchPath(Arena* arena, Str16C path)
{
	if (path.str[path.length - 2] == u'/' || path.str[path.length - 2] == u'\\')
	{
		path.length = path.length - 1; //hack, but it works :)
	}

	Str16C fid = Str16CLit(filesInDirectory);
	if (CanAddPathPrefix(path))
	{
		Str16 bigger = Str16Lit(biggerPaths);
		
		size_t pathBuffSize = (bigger.length + path.length - 1 + fid.length) * sizeof(WCHAR);
		WCHAR* folderSearch = ArenaPush(arena, pathBuffSize);
		memcpy(folderSearch, bigger.str, bigger.length * sizeof(WCHAR));
		memcpy((char*)folderSearch + bigger.length * sizeof(WCHAR), path.str, (path.length - 1) * sizeof(WCHAR));
		memcpy((char*)folderSearch + (bigger.length + path.length - 1) * sizeof(WCHAR), fid.str, fid.length * sizeof(WCHAR));

		Str16C newPath = (Str16C){ .str = folderSearch, .length = (pathBuffSize >> 1) };
		//Convert '/' to '\' as we can't use '/' when using the "\\?\" prefix
		for (size_t i = (bigger.length + 2); i < newPath.length; i++)
		{
			if (newPath.str[i] == u'/')
				newPath.str[i] = u'\\';
		}
		return newPath;
	}
	else
	{
		size_t pathBuffSize = (path.length - 1 + fid.length) * sizeof(WCHAR);
		WCHAR* folderSearch = ArenaPush(arena, pathBuffSize);
		memcpy((char*)folderSearch, path.str, (path.length - 1) * sizeof(WCHAR));
		memcpy((char*)folderSearch + (path.length -1) * sizeof(WCHAR), fid.str, (fid.length * sizeof(WCHAR)));

		Str16C newPath = (Str16C){ .str = folderSearch, .length = (pathBuffSize >> 1)};
		return newPath;
	}
}

WIN_FUNCTION Str16C DirectoryPlusFile(Arena* arena, Str16C dir, Str16C file)
{
	//assume paths are null-terminated
	if (dir.str[dir.length - 2] == u'/' || dir.str[dir.length - 2] == u'\\')
	{
		dir.length = dir.length - 1; //hack, but it works :)
	}

	size_t addedPathSize = (dir.length - 1 + 1 + file.length) * sizeof(WCHAR);
	wchar* addedPath = ArenaPush(arena, addedPathSize);
	memcpy(addedPath, dir.str, (dir.length - 1) * sizeof(WCHAR));
	addedPath[dir.length - 1] = u'\\';
	memcpy((char*)addedPath + (dir.length -1 + 1) * sizeof(WCHAR), file.str, file.length * sizeof(WCHAR));
	return (Str16C) { .str = addedPath, .length = (addedPathSize >> 1) };
}

//return false if encountered an error
WIN_FUNCTION b32 WIN_EnumerateFiles(Arena* arena, LOGHANDLE logFile, Str16CNode** list, Str16C argPath)
{
	Str8 pathStr8 = STR_S8FromS16(arena, argPath);
	Str16C path = WIN_SanitizePath(arena, argPath);
	DWORD fileAttr = GetFileAttributesW(path.str);
	if (fileAttr == INVALID_FILE_ATTRIBUTES)
	{	
		WIN_LogStr8(logFile, "%S is not a valid file or directory path\r\n", pathStr8);
		return false;
	}
	else if (fileAttr & FILE_ATTRIBUTE_DIRECTORY)
	{
		Str16C folderSearch = WIN_DirectorySearchPath(arena, path);
	
		//WriteFile(logFile, folderSearch.str, Str16Size(folderSearch), &numWritten, NULL);
		//WriteFile(logFile, u"\r\n", 4, &numWritten, NULL);
	
		WIN32_FIND_DATAW FindFileData;
	
		HANDLE hFind = FindFirstFileW(folderSearch.str, &FindFileData);
		if (hFind == INVALID_HANDLE_VALUE)
		{
			DWORD fileError = GetLastError();
			if (fileError == ERROR_FILE_NOT_FOUND)
			{
				WIN_LogStr8(logFile, "Directory is empty\r\n");
			}
			else
			{
				WIN_LogStr8(logFile, "Directory search error\r\n");
			}
			return false;
		}
	
		//Enumerate directory files
		do
		{
			if ((FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == 0)
			{
				size_t fnameSize = strlenW(FindFileData.cFileName) + sizeof(WCHAR);
				//WriteFile(logFile, FindFileData.cFileName, fnameSize, &numWritten, NULL);
				//WriteFile(logFile, u"\r\n", 4, &numWritten, NULL);
	
				Str16C dirPlusFile = DirectoryPlusFile(arena, path, (Str16C) { .str = FindFileData.cFileName, .length = (fnameSize >> 1) });
	
				EnqueueUniqueStr16C(arena, list, dirPlusFile);
			}
		} while (FindNextFileW(hFind, &FindFileData));
	
		FindClose(hFind);
	}
	else
	{
		EnqueueUniqueStr16C(arena, list, path);
	}
	
	return true;
}	

WIN_FUNCTION Str16C WIN_GetFileNameWithExtension(Str16C filePath)
{
	//assumes filePath is a path to a file, AND NOTHING ELSE
	size_t fNameIndex = 0;
	for(size_t i = 0; i < (filePath.length); i++)
	{
		WCHAR cur = filePath.str[i];
		if(cur == u'\\' || cur == u'/')
			fNameIndex = i + 1;
	}
	
	return (Str16C){.str = (filePath.str + fNameIndex), .length = filePath.length - fNameIndex };
}

//DOES NOT INCLUDE THE DOT
WIN_FUNCTION Str16C WIN_GetFileExtension(Str16C filePath)
{
	size_t extIndex = Str16CLastIndexOf(filePath, u'.');
	if(extIndex == -1)
		return (Str16C){.str = NULL, .length = 0};
	Str16C ext = (Str16C){.str = filePath.str + extIndex + 1, .length = filePath.length - extIndex - 1};
	return ext;
}

WIN_FUNCTION Str16 WIN_GetFileNameWithoutExtension(Str16C filePath)
{
	Str16C fNameWithExt = WIN_GetFileNameWithExtension(filePath);
	size_t dotIndex = Str16CLastIndexOf(fNameWithExt, u'.');
	if(dotIndex == -1)
	{
		fNameWithExt.length = fNameWithExt.length -1;
		return *(Str16*)(&fNameWithExt);
	}
	else
		return (Str16){.str = fNameWithExt.str, .length = dotIndex };
}

WIN_FUNCTION b32 WIN_FileExists(wchar* szPath)
{
  DWORD dwAttrib = GetFileAttributesW(szPath);

  return (dwAttrib != INVALID_FILE_ATTRIBUTES && 
         !(dwAttrib & FILE_ATTRIBUTE_DIRECTORY));
}