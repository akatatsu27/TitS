#include "WIN_Logging.h"

WIN_FUNCTION LOGHANDLE WIN_CreateLogHandle(HANDLE hFile)
{
	HANDLE hMutex = CreateMutexA(NULL, FALSE, NULL);
	return (LOGHANDLE){ .hFile = hFile, hMutex = hMutex };	
}

WIN_FUNCTION void WIN_CloseLogHandle(LOGHANDLE logger)
{
	CloseHandle(logger.hMutex);
}

WIN_FUNCTION void WIN_LogStr8(LOGHANDLE logger, char* fmt, ...)
{
	ArenaTemp string_scratch = GetScratch(NULL, 0);
	va_list args;
	va_start(args, fmt);
	Str8 str = Str8CPushFV(string_scratch.arena, fmt, args);
	va_end(args);
	
	DWORD dwWaitResult = WaitForSingleObject(logger.hMutex, INFINITE);
	int numWritten;
	WriteFile(logger.hFile, str.str, str.size, &numWritten, NULL);
	ReleaseMutex(logger.hMutex);
	ReleaseScratch(string_scratch);
	return;
}