#ifndef ARENA_H
#define ARENA_H

#include "BASE_Types.h"
#include "BASE_Misc.h"

#include "MATH_Basic.h"

#include "Windows.h"

#ifdef MD_H

#define ARENA_FUNCTION MD_FUNCTION
typedef MD_Arena Arena;
typedef MD_ArenaTemp ArenaTemp;

#define ArenaAlloc MD_ArenaAlloc
#define ArenaRelease MD_ArenaRelease

#define ArenaPush MD_ArenaPush
#define ArenaPutBack MD_ArenaPutBack
#define ArenaSetAlign MD_ArenaSetAlign
#define ArenaPushAlign MD_ArenaPushAlign
#define ArenaClear MD_ArenaClear

#define ArenaBeginTemp MD_ArenaBeginTemp
#define ArenaEndTemp MD_ArenaEndTemp

#define GetScratch MD_GetScratch

#define ReleaseScratch MD_ReleaseScratch

#define MemoryZero MD_MemoryZero
#define PushArray MD_PushArray
#define PushArrayZero MD_PushArrayZero

#else

#ifndef ARENA_FUNCTION
#define ARENA_FUNCTION internal
#endif

#ifndef ARN_ArenaMinPos
#define ARN_ArenaMinPos 64
#endif

#if !defined(ARN_DEFAULT_ARENA_RES_SIZE)
# define ARN_DEFAULT_ARENA_RES_SIZE (64 << 20)
#endif
#if !defined(ARN_DEFAULT_ARENA_CMT_SIZE)
# define ARN_DEFAULT_ARENA_CMT_SIZE (64 << 10)
#endif

#if !defined(ARN_ScratchCount)
# define ARN_ScratchCount 2llu
#endif

#ifndef ARN_DEFAULT_ARENA_VERY_BIG
#define ARN_DEFAULT_ARENA_VERY_BIG (ARN_DEFAULT_ARENA_RES_SIZE - ARN_ArenaMinPos)/2
#endif

#if !defined(ARN_ARENA_CMT_SIZE)
# define ARN_ARENA_CMT_SIZE (64 << 10)
#endif

#include "stdio.h"

typedef struct Arena Arena;
struct Arena
{
    Arena* prev;
    Arena* current;
    u64 base_pos;
    u64 pos;
    u64 cmt;
    u64 cap;
    u64 align;
};

typedef struct ArenaTemp ArenaTemp;
struct ArenaTemp
{
    Arena *arena;
    u64 pos;
};

ARENA_FUNCTION Arena*       ArenaAlloc(void);
ARENA_FUNCTION void         ArenaRelease(Arena *arena);

ARENA_FUNCTION void*        ArenaPush(Arena *arena, u64 size);
ARENA_FUNCTION void         ArenaPutBack(Arena *arena, u64 size);
ARENA_FUNCTION void         ArenaSetAlign(Arena *arena, u64 boundary);
ARENA_FUNCTION void         ArenaPushAlign(Arena *arena, u64 boundary);
ARENA_FUNCTION void         ArenaClear(Arena *arena);


ARENA_FUNCTION ArenaTemp ArenaBeginTemp(Arena *arena);
ARENA_FUNCTION void         ArenaEndTemp(ArenaTemp temp);

ARENA_FUNCTION ArenaTemp GetScratch(Arena **conflicts, u64 count);

#define ReleaseScratch(scratch) ArenaEndTemp(scratch)

#define MemorySet(p,v,z)    (memset(p,v,z))
#define MemoryZero(p,z)     (memset(p,0,z))
#define MemoryZeroStruct(p) (memset(p,0,sizeof(*(p))))
#define MemoryCopy(d,s,z)   (memmove(d,s,z))

#define PushArray(a,T,c) (T*)(ArenaPush((a), sizeof(T)*(c)))
#define PushArrayZero(a,T,c) (T*)(MemoryZero(PushArray(a,T,c), sizeof(T)*(c)))
#define PushZero(a,T) PushArrayZero(a,T,1)

#endif //MD_H

#endif //ARENA_H