#include "Arena.h"

#ifndef MD_H

static void*
MEM_Reserve(MD_u64 size)
{
    void *result = VirtualAlloc(0, size, MEM_RESERVE, PAGE_READWRITE);
    return(result);
}

static b32
MEM_Commit(void *ptr, MD_u64 size)
{
    MD_b32 result = (VirtualAlloc(ptr, size, MEM_COMMIT, PAGE_READWRITE) != 0);
    return(result);
}

static void
MEM_Decommit(void *ptr, MD_u64 size)
{
    VirtualFree(ptr, size, MEM_DECOMMIT);
}

static void
MEM_Release(void *ptr, MD_u64 size)
{
    (void)size;
    VirtualFree(ptr, 0, MEM_RELEASE);
}


__declspec(thread) Arena *ARN_thread_scratch_pool[ARN_ScratchCount] = {0, 0};

ARENA_FUNCTION Arena*
ArenaAlloc__Size(MD_u64 cmt, MD_u64 res)
{
    Assert(ARN_ArenaMinPos < cmt && cmt <= res);
    u64 cmt_clamped = ClampTop(cmt, res);
    Arena *result = 0;
    void *mem = MEM_Reserve(res);
    if (MEM_Commit(mem, cmt_clamped))
    {
        result = (Arena*)mem;
        result->prev = 0;
        result->current = result;
        result->base_pos = 0;
        result->pos = ARN_ArenaMinPos;
        result->cmt = cmt_clamped;
        result->cap = res;
        result->align = 8;
    }
    return(result);
}

ARENA_FUNCTION Arena*
ArenaAlloc(void)
{
    Arena *result = ArenaAlloc__Size(ARN_DEFAULT_ARENA_CMT_SIZE,
		ARN_DEFAULT_ARENA_RES_SIZE);
    return(result);
}

ARENA_FUNCTION void
ArenaRelease(Arena *arena)
{
    for (Arena *node = arena->current, *prev = 0;
         node != 0;
         node = prev)
    {
        prev = node->prev;
        MEM_Release(node, node->cap);
    }
}

ARENA_FUNCTION u64
ArenaGetPos(Arena *arena)
{
    Arena *current = arena->current;
    u64 result = current->base_pos + current->pos;
    return(result);
}

ARENA_FUNCTION void*
ArenaPush(Arena *arena, u64 size)
{
    // try to be fast!
    Arena *current = arena->current;
    u64 align = arena->align;
    u64 pos = current->pos;
    u64 pos_aligned = AlignPow2(pos, align);
    u64 new_pos = pos_aligned + size;
    void *result = (u8*)current + pos_aligned;
    current->pos = new_pos;
    
    // if it's not going to work do the slow path
    if (new_pos > current->cmt)
    {
        result = 0;
        current->pos = pos;
        
        // new chunk if necessary
        if (new_pos > current->cap)
        {
            Arena *new_arena = 0;
            if (size > ARN_DEFAULT_ARENA_VERY_BIG)
            {
                u64 big_size_unrounded = size + ARN_ArenaMinPos;
                u64 big_size = AlignPow2(big_size_unrounded, (4 << 10));
                new_arena = ArenaAlloc__Size(big_size, big_size);
            }
            else
            {
                new_arena = ArenaAlloc();
            }
            
            // link in new chunk & recompute new_pos
            if (new_arena != 0)
            {
                new_arena->base_pos = current->base_pos + current->cap;
                new_arena->prev = current;
                current = new_arena;
                pos_aligned = current->pos;
                new_pos = pos_aligned + size;
            }
        }
        
        // move ahead if the current chunk has enough reserve
        if (new_pos <= current->cap)
        {
            
            // extend commit if necessary
            if (new_pos > current->cmt)
            {
                u64 new_cmt_unclamped = AlignPow2(new_pos, ARN_ARENA_CMT_SIZE);
                u64 new_cmt = ClampTop(new_cmt_unclamped, current->cap);
                u64 cmt_size = new_cmt - current->cmt;
                if (MEM_Commit((u8*)current + current->cmt, cmt_size))
                {
                    current->cmt = new_cmt;
                }
            }
            
            // move ahead if the current chunk has enough commit
            if (new_pos <= current->cmt)
            {
                result = (u8*)current + current->pos;
                current->pos = new_pos;
            }
        }
    }
    
    return(result);
}

ARENA_FUNCTION void
ArenaPopTo(Arena *arena, u64 pos)
{
    // pop chunks in the chain
    u64 pos_clamped = ClampBot(ARN_ArenaMinPos, pos);
    (void)pos_clamped;
    {
        Arena *node = arena->current;
        for (Arena *prev = 0;
             node != 0 && node->base_pos >= pos;
             node = prev)
        {
            prev = node->prev;
            MEM_Release(node, node->cap);
        }
        arena->current = node;
    }
    
    // reset the pos of the current
    {
        Arena *current = arena->current;
        u64 local_pos_unclamped = pos - current->base_pos;
        u64 local_pos = ClampBot(local_pos_unclamped, ARN_ArenaMinPos);
        current->pos = local_pos;
    }
}

ARENA_FUNCTION void
ArenaSetAutoAlign(Arena *arena, u64 align)
{
    arena->align = align;
}

ARENA_FUNCTION void
ArenaAbsorb(Arena *arena, Arena *sub_arena)
{
    Arena *current = arena->current;
    u64 base_pos_shift = current->base_pos + current->cap;
    for (Arena *node = sub_arena->current;
         node != 0;
         node = node->prev)
    {
        node->base_pos += base_pos_shift;
    }
    sub_arena->prev = arena->current;
    arena->current = sub_arena->current;
}

ARENA_FUNCTION void
ArenaPutBack(Arena *arena, u64 size)
{
    u64 pos = ArenaGetPos(arena);
    u64 new_pos = pos - size;
    u64 new_pos_clamped = ClampBot(ARN_ArenaMinPos, new_pos);
    ArenaPopTo(arena, new_pos_clamped);
}

ARENA_FUNCTION void
ArenaSetAlign(Arena *arena, u64 boundary)
{
    ArenaSetAutoAlign(arena, boundary);
}

ARENA_FUNCTION void
ArenaPushAlign(Arena *arena, u64 boundary)
{
    u64 pos = ArenaGetPos(arena);
    u64 align_m1 = boundary - 1;
    u64 new_pos_aligned = (pos + align_m1)&(~align_m1);
    if (new_pos_aligned > pos)
    {
        u64 amt = new_pos_aligned - pos;
        MemoryZero(ArenaPush(arena, amt), amt);
    }
}

ARENA_FUNCTION void
ArenaClear(Arena *arena)
{
    ArenaPopTo(arena, ARN_ArenaMinPos);
}

ARENA_FUNCTION ArenaTemp
ArenaBeginTemp(Arena *arena)
{
    ArenaTemp result;
    result.arena = arena;
    result.pos   = ArenaGetPos(arena);
    return(result);
}

ARENA_FUNCTION void
ArenaEndTemp(ArenaTemp temp)
{
    ArenaPopTo(temp.arena, temp.pos);
}


ARENA_FUNCTION Arena*
GetScratchArena(Arena **conflicts, u64 count)
{
    Arena **scratch_pool = ARN_thread_scratch_pool;
    if (scratch_pool[0] == 0)
    {
        Arena **arena_ptr = scratch_pool;
        for (u64 i = 0; i < ARN_ScratchCount; i += 1, arena_ptr += 1)
        {
            *arena_ptr = ArenaAlloc();
        }
    }
    Arena *result = 0;
    Arena **arena_ptr = scratch_pool;
    for (u64 i = 0; i < ARN_ScratchCount; i += 1, arena_ptr += 1)
    {
        Arena *arena = *arena_ptr;
        Arena **conflict_ptr = conflicts;
        for (u32 j = 0; j < count; j += 1, conflict_ptr += 1)
        {
            if (arena == *conflict_ptr)
            {
                arena = 0;
                break;
            }
        }
        if (arena != 0)
        {
            result = arena;
            break;
        }
    }
    return(result);
}

ARENA_FUNCTION ArenaTemp
GetScratch(Arena **conflicts, u64 count)
{
    Arena *arena = GetScratchArena(conflicts, count);
    ArenaTemp result = {0};
    if (arena != 0)
    {
        result = ArenaBeginTemp(arena);
    }
    return(result);
}

#endif //MD_H 