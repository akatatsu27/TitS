#include "STR_Unicode.h"

static u8 STR_utf8_class[32] = {
    1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,2,2,2,2,3,3,4,5,
};

STR_FUNCTION STR_DecodedCodepoint
STR_DecodeCodepointFromUtf8(u8 *str, u64 max)
{
#define STR_bitmask1 0x01
#define STR_bitmask2 0x03
#define STR_bitmask3 0x07
#define STR_bitmask4 0x0F
#define STR_bitmask5 0x1F
#define STR_bitmask6 0x3F
#define STR_bitmask7 0x7F
#define STR_bitmask8 0xFF
#define STR_bitmask9  0x01FF
#define STR_bitmask10 0x03FF
    
    STR_DecodedCodepoint result = {~((u32)0), 1};
    u8 byte = str[0];
    u8 byte_class = STR_utf8_class[byte >> 3];
    switch (byte_class)
    {
        case 1:
        {
            result.codepoint = byte;
        }break;
        
        case 2:
        {
            if (2 <= max)
            {
                u8 cont_byte = str[1];
                if (STR_utf8_class[cont_byte >> 3] == 0)
                {
                    result.codepoint = (byte & STR_bitmask5) << 6;
                    result.codepoint |=  (cont_byte & STR_bitmask6);
                    result.advance = 2;
                }
            }
        }break;
        
        case 3:
        {
            if (3 <= max)
            {
                u8 cont_byte[2] = {0};
                cont_byte[0] = str[1];
                cont_byte[1] = str[2];
                if (STR_utf8_class[cont_byte[0] >> 3] == 0 &&
                    STR_utf8_class[cont_byte[1] >> 3] == 0)
                {
                    result.codepoint = (byte & STR_bitmask4) << 12;
                    result.codepoint |= ((cont_byte[0] & STR_bitmask6) << 6);
                    result.codepoint |=  (cont_byte[1] & STR_bitmask6);
                    result.advance = 3;
                }
            }
        }break;
        
        case 4:
        {
            if (4 <= max)
            {
                u8 cont_byte[3] = {0};
                cont_byte[0] = str[1];
                cont_byte[1] = str[2];
                cont_byte[2] = str[3];
                if (STR_utf8_class[cont_byte[0] >> 3] == 0 &&
                    STR_utf8_class[cont_byte[1] >> 3] == 0 &&
                    STR_utf8_class[cont_byte[2] >> 3] == 0)
                {
                    result.codepoint = (byte & STR_bitmask3) << 18;
                    result.codepoint |= ((cont_byte[0] & STR_bitmask6) << 12);
                    result.codepoint |= ((cont_byte[1] & STR_bitmask6) <<  6);
                    result.codepoint |=  (cont_byte[2] & STR_bitmask6);
                    result.advance = 4;
                }
            }
        }break;
    }
    
    return(result);
}

STR_FUNCTION STR_DecodedCodepoint
STR_DecodeCodepointFromUtf16(u16 *out, u64 max)
{
    STR_DecodedCodepoint result = {~((u32)0), 1};
    result.codepoint = out[0];
    result.advance = 1;
    if (1 < max && 0xD800 <= out[0] && out[0] < 0xDC00 && 0xDC00 <= out[1] && out[1] < 0xE000)
    {
        result.codepoint = ((out[0] - 0xD800) << 10) | (out[1] - 0xDC00);
        result.advance = 2;
    }
    return(result);
}

STR_FUNCTION u32
STR_Utf8FromCodepoint(u8 *out, u32 codepoint)
{
#define STR_bit8 0x80
    u32 advance = 0;
    if (codepoint <= 0x7F)
    {
        out[0] = (u8)codepoint;
        advance = 1;
    }
    else if (codepoint <= 0x7FF)
    {
        out[0] = (STR_bitmask2 << 6) | ((codepoint >> 6) & STR_bitmask5);
        out[1] = STR_bit8 | (codepoint & STR_bitmask6);
        advance = 2;
    }
    else if (codepoint <= 0xFFFF)
    {
        out[0] = (STR_bitmask3 << 5) | ((codepoint >> 12) & STR_bitmask4);
        out[1] = STR_bit8 | ((codepoint >> 6) & STR_bitmask6);
        out[2] = STR_bit8 | ( codepoint       & STR_bitmask6);
        advance = 3;
    }
    else if (codepoint <= 0x10FFFF)
    {
        out[0] = (STR_bitmask4 << 3) | ((codepoint >> 18) & STR_bitmask3);
        out[1] = STR_bit8 | ((codepoint >> 12) & STR_bitmask6);
        out[2] = STR_bit8 | ((codepoint >>  6) & STR_bitmask6);
        out[3] = STR_bit8 | ( codepoint        & STR_bitmask6);
        advance = 4;
    }
    else
    {
        out[0] = '?';
        advance = 1;
    }
    return(advance);
}

STR_FUNCTION u32
STR_Utf16FromCodepoint(u16 *out, u32 codepoint)
{
    u32 advance = 1;
    if (codepoint == ~((u32)0))
    {
        out[0] = (u16)'?';
    }
    else if (codepoint < 0x10000)
    {
        out[0] = (u16)codepoint;
    }
    else
    {
        u64 v = codepoint - 0x10000;
        out[0] = (u16)(0xD800 + (v >> 10));
        out[1] = 0xDC00 + (v & STR_bitmask10);
        advance = 2;
    }
    return(advance);
}

STR_FUNCTION Str8
STR_S8FromS16(Arena *arena, Str16 in)
{
    u64 cap = in.size*3;
    u8 *str = PushArrayZero(arena, u8, cap + 1);
    u16 *ptr = in.str;
    u16 *opl = ptr + in.size;
    u64 size = 0;
    STR_DecodedCodepoint consume;
    for (;ptr < opl;)
    {
        consume = STR_DecodeCodepointFromUtf16(ptr, opl - ptr);
        ptr += consume.advance;
        size += STR_Utf8FromCodepoint(str + size, consume.codepoint);
    }
    str[size] = 0;
    ArenaPutBack(arena, cap - size); // := ((cap + 1) - (size + 1))
    return (Str8){ .str = str, .count = size};
}

STR_FUNCTION Str16
STR_S16FromS8(Arena *arena, Str8 in)
{
    u64 cap = in.size*2;
    u16 *str = PushArrayZero(arena, u16, cap + 1);
    u8 *ptr = in.str;
    u8 *opl = ptr + in.size;
    u64 size = 0;
    STR_DecodedCodepoint consume;
    for (;ptr < opl;)
    {
        consume = STR_DecodeCodepointFromUtf8(ptr, opl - ptr);
        ptr += consume.advance;
        size += STR_Utf16FromCodepoint(str + size, consume.codepoint);
    }
    str[size] = 0;
    ArenaPutBack(arena, 2*(cap - size)); // := 2*((cap + 1) - (size + 1))
    Str16 result = {0};
    result.str = str;
    result.size = size;
    return(result);
}