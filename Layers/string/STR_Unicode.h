#ifndef STR_UNICODE_H
#define STR_UNICODE_H

//BASE
#include "../Base/BASE_Types.h"

//ARENA
#include "../Arena/Arena.h"

#include "Str.h"

typedef struct STR_DecodedCodepoint STR_DecodedCodepoint;
struct STR_DecodedCodepoint
{
    u32 codepoint;
    u32 advance;
};

STR_FUNCTION STR_DecodedCodepoint STR_DecodeCodepointFromUtf8(u8 *str, u64 max);
STR_FUNCTION STR_DecodedCodepoint STR_DecodeCodepointFromUtf16(u16 *out, u64 max);
STR_FUNCTION u32 STR_Utf8FromCodepoint(u8 *out, u32 codepoint);
STR_FUNCTION u32 STR_Utf16FromCodepoint(u16 *out, u32 codepoint);
STR_FUNCTION Str8 STR_S8FromS16(Arena *arena, Str16 in);
STR_FUNCTION Str16 STR_S16FromS8(Arena *arena, Str8 in);

STR_FUNCTION Str16 STR_Str16FromS8(Arena *arena, Str8 in);

#endif //STR_UNICODE_H