#ifndef STR_H
#define STR_H

//BASE
#include "BASE_Misc.h"
#include "BASE_Types.h"

//STD
#include "string.h"
#include "stdlib.h"
#include "stdarg.h"

//ARENA
#include "Arena.h"

#include "math_basic.c"

#ifndef STR_FUNCTION
#define STR_FUNCTION internal
#endif

#ifdef MD_H
typedef MD_String8 Str8;
typedef MD_String16 Str16;
#else
typedef struct Str8
{
	char* str;
	union 
	{
		u64 size;
		u64 length;
		u64 count;
	};
} Str8;

typedef struct Str16
{
	wchar* str;
	union //wchars, not bytes
	{
		u64 size;
		u64 length;
		u64 count;
	};
} Str16;

#endif MD_H

typedef Str8 Str8C; //NULL-TERMINATED

typedef struct Str8Node
{
	struct Str8Node* next;
	Str8 str;
} Str8Node;

typedef struct Str8CNode
{
	struct Str8CNode* next;
	Str8C str; //NULL-TERMINATED
} Str8CNode;


typedef Str16 Str16C; //NULL-TERMINATED

typedef struct Str16Node
{
	struct Str16Node* next;
	Str16 str;
} Str16Node;

typedef struct Str16CNode
{
	struct Str16CNode* next;
	Str16C str; //NULL-TERMINATED
} Str16CNode;

typedef struct Str8_atoiURes
{
	BOOLEAN success;
	u64 result;
} Str8_atoiURes;

typedef struct Str8_atoiSRes
{
	BOOLEAN success;
	s64 result;
} Str8_atoiSRes;

typedef struct Str8_atofRes
{
	int err;
	double result;
} Str8_atofRes;

#define Str8Lit(s) (Str8){(CHAR *)(s), sizeof(s)-1}
#define Str16Lit(s) (Str16){ .str = s, .length = ((sizeof(s)-2) >> 1) }

#define Str16CLit(s) (Str16C){ .str = s, .length = (sizeof(s) >> 1) }

#define Str16Size(s) (s.length * sizeof(wchar))

StaticAssert(sizeof(Str16) == sizeof(Str16C) && sizeof(Str8) == sizeof(Str8C) && sizeof(Str16) == sizeof(Str8), string__structs_size_check);

STR_FUNCTION Str8 Str8PushFV(Arena* arena, char *fmt, va_list args);
STR_FUNCTION Str8 Str8PushF(Arena* arena, char *fmt, ...);
STR_FUNCTION Str8C Str8CPushFV(Arena* arena, char *fmt, va_list args);
STR_FUNCTION Str8C Str8CPushF(Arena* arena, char *fmt, ...);
STR_FUNCTION u8 DigitCharToValue(char c);
STR_FUNCTION u8 HexDigitCharToValue(char c);
STR_FUNCTION Str8_atoiURes Str8_atoi_impl(Str8 str);
STR_FUNCTION Str8_atoiURes Str8_atoiU(Str8 str);
STR_FUNCTION Str8_atoiSRes Str8_atoiS(Str8 str);
STR_FUNCTION Str8_atofRes Str8_atof(Str8 str);
STR_FUNCTION b32 Str8Cmp(Str8 str1, Str8 str2);
STR_FUNCTION b32 Str16Cmp(Str16 str1, Str16 str2);
STR_FUNCTION b32 Str8CCmp(Str8C str1, Str8C str2);
STR_FUNCTION b32 Str16CCmp(Str16C str1, Str16C str2);
STR_FUNCTION b32 Str16Contains(Str16 str1, Str16 str2);
STR_FUNCTION b32 Str16StartsWith(Str16 str1, Str16 str2);
STR_FUNCTION wchar ToUpperW(wchar c);
STR_FUNCTION wchar ToLowerW(wchar c);
STR_FUNCTION b32 IsRepeatedWchar(Str16 str, wchar c);
STR_FUNCTION b32 IsDigitW(wchar c);
STR_FUNCTION Str16 SkipSubStr16(Str16 str, size_t skip);
STR_FUNCTION b32 Str16StartsWithCaseInsensitive(Str16 str1, Str16 str2);
STR_FUNCTION void EnqueueUniqueStr16(Arena* arena, Str16Node** list, Str16 str);
STR_FUNCTION void EnqueueUniqueStr16C(Arena* arena, Str16CNode** list, Str16C str);
STR_FUNCTION void EnqueueUniqueStr8(Arena* arena, Str8Node** list, Str8 str);
STR_FUNCTION void EnqueueUniqueStr8C(Arena* arena, Str8CNode** list, Str8C str);
STR_FUNCTION size_t Str16LastIndexOf(Str16 str, wchar thisOne);
STR_FUNCTION size_t Str16CLastIndexOf(Str16C str, WCHAR thisOne);

#endif //STR_H