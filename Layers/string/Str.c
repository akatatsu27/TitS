#include "Str.h"

STR_FUNCTION Str8
Str8PushFV(Arena* arena, char *fmt, va_list args)
{
 Str8 result = {0};
 va_list args2;
 va_copy(args2, args);
 u64 needed_bytes = vsnprintf(0, 0, fmt, args);
 result.str = PushArray(arena, u8, needed_bytes);
 result.size = needed_bytes - 1;
 vsnprintf((char*)result.str, needed_bytes, fmt, args2);
 return result;
}

STR_FUNCTION Str8
Str8PushF(Arena* arena, char *fmt, ...)
{
 Str8 result = {0};
 va_list args;
 va_start(args, fmt);
 result = Str8PushFV(arena, fmt, args);
 va_end(args);
 return result;
}

STR_FUNCTION Str8C
Str8CPushFV(Arena* arena, char *fmt, va_list args)
{
 Str8C result = {0};
 va_list args2;
 va_copy(args2, args);
 u64 needed_bytes = vsnprintf(0, 0, fmt, args) + 1;
 result.str = PushArray(arena, u8, needed_bytes);
 result.size = needed_bytes - 1;
 vsnprintf((char*)result.str, needed_bytes, fmt, args2);
 return result;
}

STR_FUNCTION Str8C
Str8CPushF(Arena* arena, char *fmt, ...)
{
 Str8C result = {0};
 va_list args;
 va_start(args, fmt);
 result = Str8CPushFV(arena, fmt, args);
 va_end(args);
 return result;
}

STR_FUNCTION u8 DigitCharToValue(char c)
{
	if(c >= '0' && c <= '9')
		return c - '0';
	else
		return 0xFF;
}

STR_FUNCTION u8 HexDigitCharToValue(char c)
{
	if(c >= '0' && c <= '9')
		return c - '0';
	else if (c >= 'A' && c <= 'F')
		return c - 'A' + 10;
	else if (c >= 'a' && c <= 'f')
		return c - 'a' + 10;
	else
		return 0xFF;
}
STR_FUNCTION Str8_atoiURes
Str8_atoi_impl(Str8 str)
{
	const u64 MAXVAL = 0xFFFFFFFFFFFFFFFF;
	size_t biggestDigitIndex = 0;
	
	u64 retVal = 0;	
	enum {DECIMAL, BINARY, HEXADECIMAL} parse_mode = DECIMAL;
	if(str.str[biggestDigitIndex] == '0' && str.length - biggestDigitIndex >= 3)
	{
		switch(str.str[biggestDigitIndex + 1])
		{
		case 'b':
		case 'B':
			parse_mode = BINARY;
			biggestDigitIndex += 2;
			break;
		case 'x':
		case 'X':
			parse_mode = HEXADECIMAL;
			biggestDigitIndex += 2;
			break;
		default:
			break;
		}
	}
	
	size_t i = str.length - 1; //iterate from the end of the string
	size_t curDigitPos = 0; //we allow digit separators for readability of long numbers
	for(; i >= biggestDigitIndex; i -= 1)
	{
		if(str.str[i] == '_' && i != biggestDigitIndex && i != str.length - 1)
			continue;
		
		switch(parse_mode)
		{
		case DECIMAL: {
			u8 digit = DigitCharToValue(str.str[i]);
			if(digit == 0xFF)
				return (Str8_atoiURes){false, 0};
			else
			{
				s64 temp = poweri(10, curDigitPos) * digit; 
				if(temp > MAXVAL - retVal)
					return (Str8_atoiURes){false, 0}; //overflow
				retVal += temp;
				curDigitPos += 1;
			}
			break;}
		case BINARY: {
			if (curDigitPos == 64) //overflow check
				return (Str8_atoiURes){false, 0};
			u8 digit = str.str[i] - '0';
			if(digit == 0 || digit == 1)
			{
				retVal += digit << curDigitPos;
				curDigitPos += 1;
			}
			else
			{
				return (Str8_atoiURes){false, 0};
			}
			break;}
		case HEXADECIMAL: {
			u8 digit = HexDigitCharToValue(str.str[i]);
			if(digit == 0xFF)
				return (Str8_atoiURes){false, 0};
			else
			{
				s64 temp = digit << (curDigitPos << 2);
				if(temp > MAXVAL - retVal)
					return (Str8_atoiURes){false, 0}; //overflow
				retVal += temp;
				curDigitPos += 1;
			}
			break;}
		}
	}
	
	return (Str8_atoiURes){true, retVal};
}

STR_FUNCTION Str8_atoiURes
Str8_atoiU(Str8 str)
{	
	return Str8_atoi_impl(str);
}

STR_FUNCTION Str8_atoiSRes
Str8_atoiS(Str8 str)
{
	const s64 sMAXVAL = 9223372036854775807;
	if(str.length == 0)
		return (Str8_atoiSRes){false, 0};
	u8 sign = 1;
	size_t biggestDigitIndex = 0;	
	
	if(str.str[0] == '-')
	{
		sign = -1;
		biggestDigitIndex += 1;
	}
	Str8_atoiURes parse = Str8_atoi_impl((Str8){str.str + biggestDigitIndex, str.length - biggestDigitIndex});
	if(parse.success == false || parse.result > sMAXVAL)
		return (Str8_atoiSRes){false, 0};
	s64 retVal = (s32)parse.result;
	retVal = retVal * sign;
	return (Str8_atoiSRes){true, retVal};
}

STR_FUNCTION Str8_atofRes
Str8_atof(Str8 str)
{
	if(str.length = 0)
		return (Str8_atofRes){EILSEQ, 0};
	char* s = str.str;
	u64 index = 0;
	double a = 0.0;
	int e = 0;
	char c = *s;
	int outersign = 1;
  
	//assumes no leading whitespace
	/*
	//skip whitespace
	while(iswhitespace(c))
	{
		index += 1;
		if(index >= str.length)
			return (Str8_atofRes){EILSEQ, 0};
		c = *(s + index);
	}
	*/
  
	//optional + - or 
	if(c == '+')
	{
		index += 1;
		if(index >= str.length)
			return (Str8_atofRes){EILSEQ, 0};
		c = *(s + index);
	}
	else if (c == '-')
	{
		outersign = -1;
		index += 1;
		if(index >= str.length)
			return (Str8_atofRes){EILSEQ, 0};
		c = *(s + index);
	}
	
	
	if (isdigit(c))
	{
		//decimal floating-point expression
		while (isdigit(c))
		{
			a = a*10.0 + (c - '0');
			index += 1;
			if(index >= str.length)
				return (Str8_atofRes){0, a};
			c = *(s + index);
		}
		
		if (c == '.')
		{
			index += 1;
			if(index >= str.length)
				return (Str8_atofRes){EILSEQ, 0};
			c = *(s + index);
			
			while (isdigit(c))
			{
				a = a*10.0 + (c - '0');
				index += 1;
				if(index >= str.length)
					return (Str8_atofRes){0, a};
				c = *(s + index);
			
				e = e-1;
			}
		}
		
		if (c == 'e' || c == 'E') 
		{
			int sign = 1;
			int i = 0;
			index += 1;
			if(index >= str.length)
				return (Str8_atofRes){EILSEQ, 0};
			c = *(s + index);
			if (c == '+')
			{
				index += 1;
				if(index >= str.length)
					return (Str8_atofRes){EILSEQ, 0};
				c = *(s + index);
			}
			else if (c == '-')
			{
				index += 1;
				if(index >= str.length)
					return (Str8_atofRes){EILSEQ, 0};
				c = *(s + index);
				sign = -1;
			}
			
			while (isdigit(c))
			{
				i = i*10 + (c - '0');
				index += 1;
				if(index >= str.length)
					break;
				c = *(s + index);
			}
			e += i*sign;
		}
		
		while (e > 0) {
			a *= 10.0;
			e--;
		}
		while (e < 0) {
			a *= 0.1;
			e++;
		}
		
		a = a * outersign;
		
		return (Str8_atofRes){0, a};
	}
	else if (c == 'i' || c == 'I')
	{
		//infinity expression?
		index += 1;
		if(index >= str.length)
			return (Str8_atofRes){EILSEQ, 0};
		c = *(s + index);
		if(c != 'n' || c != 'N')
		{
			return (Str8_atofRes){EILSEQ, 0};
		}
		index += 1;
		if(index >= str.length)
			return (Str8_atofRes){EILSEQ, 0};
		c = *(s + index);
		if(c != 'f' || c != 'f')
		{
			return (Str8_atofRes){EILSEQ, 0};
		}
		
		if(outersign == 1)
		{
			__u64double inf;
			inf.asU64 = PLUSINF;
			return (Str8_atofRes){0, inf.asDouble};
		}
		else
		{
			__u64double inf;
			inf.asU64 = MINUSINF;
			return (Str8_atofRes){0, inf.asDouble};
		}
	}
	else if (c == 'n' || c == 'N')
	{
		//not-a-number expression?
		index += 1;
		if(index >= str.length)
			return (Str8_atofRes){EILSEQ, 0};
		c = *(s + index);
		if(c != 'a' && c != 'A')
		{
			return (Str8_atofRes){EILSEQ, 0};
		}
		
		index += 1;
		if(index >= str.length)
			return (Str8_atofRes){EILSEQ, 0};
		c = *(s + index);
		if(c != 'n' && c != 'N')
		{
			return (Str8_atofRes){EILSEQ, 0};
		}
		
		__u64double whydidyouinputnan;
		whydidyouinputnan.asU64 = NaN;
		
		return (Str8_atofRes){0, whydidyouinputnan.asDouble};
	}
	else
	{
		return (Str8_atofRes){EILSEQ, 0};
	}
}

STR_FUNCTION b32
Str8Cmp(Str8 str1, Str8 str2)
{
	if (str1.length != str2.length)
		return false;

	for (size_t i = 0; i < (str1.length); i++)
	{
		if (str1.str[i] != str2.str[i])
			return false;
	}

	return true;
}

STR_FUNCTION b32
Str16Cmp(Str16 str1, Str16 str2)
{
	if (str1.length != str2.length)
		return false;

	for (size_t i = 0; i < (str1.length); i++)
	{
		if (str1.str[i] != str2.str[i])
			return false;
	}

	return true;
}
STR_FUNCTION b32
Str8CCmp(Str8C str1, Str8C str2)
{
	return Str8Cmp(*(Str8*)(&str1), *(Str8*)(&str2));
}

STR_FUNCTION b32
Str16CCmp(Str16C str1, Str16C str2)
{
	return Str16Cmp(*(Str16*)(&str1), *(Str16*)(&str2));
}

STR_FUNCTION b32
Str16Contains(Str16 str1, Str16 str2)
{
	return false;
}

STR_FUNCTION b32
Str16StartsWith(Str16 str1, Str16 str2)
{
	if(str2.length > str1.length)
		return false;
	
	for (size_t i = 0; i < (str2.length >> 1); i++)
	{
		if(str1.str[i] != str2.str[i])
			return false;
	}
	
	return true;
}

STR_FUNCTION wchar ToUpperW(wchar c)
{
	if(c >= u'a' && c <= u'z')
			return c - 0x20;
	else
		return c;
}

STR_FUNCTION wchar ToLowerW(wchar c)
{
	if(c >= u'A' && c <= u'Z')
			return c + 0x20;
	else
		return c;
}

STR_FUNCTION b32 IsRepeatedWchar(Str16 str, wchar c)
{
	for (size_t i = 0; i < str.length; i++)
	{
		if(str.str[i] != c)
			return false;
	}
	return true;
}

STR_FUNCTION b32 IsDigitW(wchar c)
{
	return c >= u'0' && c <= u'9';
}

STR_FUNCTION Str16 SkipSubStr16(Str16 str, size_t skip)
{
	return (Str16){.str = str.str + skip, .length = str.length - skip};
}

STR_FUNCTION b32
Str16StartsWithCaseInsensitive(Str16 str1, Str16 str2)
{
	if(str2.length > str1.length)
		return false;
	
	for(size_t i = 0; i < str2.length; i++)
	{
		wchar cur1 = ToLowerW(str1.str[i]);
		wchar cur2 = ToLowerW(str2.str[i]);
		if(cur1 != cur2)
			return false;
	}
	return true;
}

STR_FUNCTION void EnqueueUniqueStr16(Arena* arena, Str16Node** list, Str16 str)
{
	//make sure this str hasn't already been added to the list
	for (Str16Node* node = *list; node != NULL; node = node->next)
	{
		if (Str16Cmp(node->str, str))
		{
			return;
		}
	}
	
	Str16Node* last = *list;
	while(true)
	{
		if(last == NULL)
		{
			//Doesn't exist. Add it
			Str16Node* node = ArenaPush(arena, sizeof(Str16Node));
			node->str = str;
			node->next = NULL;
			*list = node;
			return;
		}
		
		if (Str16Cmp(last->str, str))
		{
			return;			
		}
		
		if(last->next == NULL)
		{
			//Doesn't exist. Add it
			Str16Node* node = ArenaPush(arena, sizeof(Str16Node));
			node->str = str;
			node->next = NULL;
			last->next = node;
			return;
		}
		else
		{
			last = last->next;
		}
	}
}

STR_FUNCTION void EnqueueUniqueStr16C(Arena* arena, Str16CNode** list, Str16C str)
{
	EnqueueUniqueStr16(arena, (Str16Node**)list, *(Str16*)(&str));
}

STR_FUNCTION void EnqueueUniqueStr8(Arena* arena, Str8Node** list, Str8 str)
{
	EnqueueUniqueStr16(arena, (Str16Node**)list, *(Str16*)&str);
}

STR_FUNCTION void EnqueueUniqueStr8C(Arena* arena, Str8CNode** list, Str8C str)
{
	EnqueueUniqueStr16(arena, (Str16Node**)list, *(Str16*)&str);
}

STR_FUNCTION size_t Str16LastIndexOf(Str16 str, wchar thisOne)
{
	size_t index = -1;
	for (size_t i = 0; i < (str.length); i++)
	{
		if(str.str[i] == thisOne)
			index = i;
	}
	
	return index;
}

STR_FUNCTION size_t Str16CLastIndexOf(Str16C str, WCHAR thisOne)
{
	return Str16LastIndexOf(*(Str16*)(&str), thisOne);
}