#ifndef SYN_SEQ_H
#define SYN_SEQ_H

#include "SYN.h"
#include "SYN_nrSpinlock.h"


#include "BASE_Misc.h"

typedef struct SYN_SEQNode
{
	struct SYN_SEQNode* next;
	void* payload;
} SYN_SEQNode;

typedef struct SYN_SEQ
{
	SYN_SEQNode* head;
	SYN_nrSpinLock nrlock;
} SYN_SEQ;

static readonly SYN_SEQNode SYN_SEQSentinel = {.next = &SYN_SEQSentinel};

SYN_FUNCTION void SYN_SEQInit(SYN_SEQ* q);
SYN_FUNCTION void SYN_SEQEnqueue(SYN_SEQ* queue, SYN_SEQNode* node);
SYN_FUNCTION SYN_SEQNode* SYN_SEQDequeue(SYN_SEQ* queue);
SYN_FUNCTION SYN_SEQNode* SYN_SEQPeak(SYN_SEQ* queue);

#endif //SYN_SEQ_H