#include "SYN_nrSpinlock.h"

SYN_FUNCTION void SYN_nrSpinLockInit(SYN_nrSpinLock* v)
{
	v->m_s = SYN_LOCK_FREE;
}

SYN_FUNCTION void SYN_nrSpinLockEnter(SYN_nrSpinLock* v)
{	
    while (SYN_nrSpinLockTryEnter(v) == SYN_LOCK_TAKEN) 
	{
		_mm_pause();
	}
}

SYN_FUNCTION SYN_LOCK SYN_nrSpinLockTryEnter(SYN_nrSpinLock* v)
{	
	return _InterlockedCompareExchange(&v->m_s, SYN_LOCK_TAKEN, SYN_LOCK_FREE);
}

SYN_FUNCTION void SYN_nrSpinLockLeave(SYN_nrSpinLock* v)
{
	v->m_s = SYN_LOCK_FREE;
    //_InterlockedExchange(&v->m_s, 0);
}