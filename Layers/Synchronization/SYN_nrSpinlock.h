#ifndef SYN_NRSPINLOCK_H
#define SYN_NRSPINLOCK_H

#include "SYN.h"

#include "BASE_Types.h"

typedef enum SYN_LOCK
{
	SYN_LOCK_FREE = 0,
	SYN_LOCK_TAKEN = 1 
} SYN_LOCK;

// THIS LOCK IS NOT-REENTRANT !!
typedef struct SYN_nrSpinLock
{
	SYN_LOCK m_s;   
} SYN_nrSpinLock;

SYN_FUNCTION void SYN_nrSpinLockInit(SYN_nrSpinLock* v);
// enter the lock, spinlocks (with/without Sleep)
// when mutex is already locked
SYN_FUNCTION void SYN_nrSpinLockEnter(SYN_nrSpinLock* v);

//returns SYN_LOCK_FREE when free, return SYN_LOCK_TAKEN when taken
SYN_FUNCTION SYN_LOCK SYN_nrSpinLockTryEnter(SYN_nrSpinLock* v);

// Leaves or unlocks the mutex
// (should only be called by lock owner)
SYN_FUNCTION void SYN_nrSpinLockLeave(SYN_nrSpinLock* v);

#endif //SYN_NRSPINLOCK_H