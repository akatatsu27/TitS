#include "SYN_SEQ.h"

SYN_FUNCTION void SYN_SEQInit(SYN_SEQ* q)
{
	q->head = &SYN_SEQSentinel;
	SYN_nrSpinLockInit(&q->nrlock);
}

SYN_FUNCTION void SYN_SEQEnqueue(SYN_SEQ* queue, SYN_SEQNode* node)
{
	SYN_nrSpinLockEnter(&queue->nrlock);
	SYN_SEQNode* prevHead = queue->head;
	node->next = prevHead;
	queue->head = node;
	SYN_nrSpinLockLeave(&queue->nrlock);
}

SYN_FUNCTION SYN_SEQNode* SYN_SEQDequeue(SYN_SEQ* queue)
{
	SYN_nrSpinLockEnter(&queue->nrlock);
	SYN_SEQNode* prevHead = queue->head;
	queue->head = prevHead->next;
	SYN_nrSpinLockLeave(&queue->nrlock);
	return prevHead;
}

SYN_FUNCTION SYN_SEQNode* SYN_SEQPeak(SYN_SEQ* queue)
{
	SYN_nrSpinLockEnter(&queue->nrlock);
	SYN_SEQNode* prevHead = queue->head;
	SYN_nrSpinLockLeave(&queue->nrlock);
	return prevHead;
}