#ifndef __STDINT_H

#define __STDINT_H

#include "BASE_Misc.h"

typedef long long intmax_t;
typedef unsigned long long uintmax_t;


typedef signed char        int8_t;
typedef short              int16_t;
typedef int                int32_t;
typedef long long          int64_t;
typedef unsigned char      uint8_t;
typedef unsigned short     uint16_t;
typedef unsigned int       uint32_t;
typedef unsigned long long uint64_t;
typedef long long int64_t;

StaticAssert((sizeof(int8_t) == 1), int8_t);
StaticAssert((sizeof(int16_t) == 2), int16_t);
StaticAssert((sizeof(int32_t) == 4), int32_t);
StaticAssert((sizeof(int64_t) == 8), int64_t);
StaticAssert((sizeof(uint8_t) == 1), uint8_t);
StaticAssert((sizeof(uint16_t) == 2), uint16_t);
StaticAssert((sizeof(uint32_t) == 4), uint32_t);
StaticAssert((sizeof(uint64_t) == 8), uint64_t);


typedef int8_t int_least8_t;
typedef uint8_t uint_least8_t;
typedef int16_t int_least16_t;
typedef uint16_t uint_least16_t;
typedef int32_t int_least32_t;
typedef uint32_t uint_least32_t;
typedef int64_t int_least64_t;
typedef uint64_t uint_least64_t;
typedef int8_t int_fast8_t;
typedef uint8_t uint_fast8_t;
typedef int16_t int_fast16_t;
typedef uint16_t uint_fast16_t;
typedef int32_t int_fast32_t;
typedef uint32_t uint_fast32_t;
typedef int64_t int_fast64_t;
typedef uint64_t uint_fast64_t;
typedef int64_t intptr_t;
typedef uint64_t uintptr_t;

#endif //__STDINT_H