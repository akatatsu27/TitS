#ifndef __ERRNO_H

#define __ERRNO_H

__declspec(thread) int __errno;

#define errno __errno

//Results from a parameter outside a function's domain, e.g. sqrt(-1)
#define EDOM 33

//Results from a result outside a function's range, e.g. strtol("0xfffffffff", NULL, 0) on systems with a 32-bit wide long
#define ERANGE 34

//Results from an illegal byte sequence, e.g. mbstowcs(buf, "\xff", 1) on systems that use UTF-8.
#define EILSEQ 42

#endif //__ERRNO_H