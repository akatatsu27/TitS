#ifndef __STRING_H

#define __STRING_H

#include "stddef.h"

size_t
strlen(const char *str)
{
        const char *s;

        for (s = str; *s; ++s)
                ;
        return (s - str);
}

void memcpy(void* restrict dest, void* restrict src, size_t srcSize) //I trust you bro that you can fit me
{
	for(size_t i = 0; i < srcSize; i++)
	{
		*((char*)dest + i) = *((char*)src + i);
	}
}

void* memmove(void* dest, const void* src, size_t num)
{
	if(num == 0)
		return dest;
	
	if(*(size_t*)&src < *(size_t*)&dest)
	{
		//reverse copy
		for(size_t i = num; i > 0; i--)
		{
			*((char*)dest + i - 1) = *((char*)src + i - 1);
		}
	}
	else
	{
		//forward copy
		for(size_t i = 0; i < num; i++)
		{
			*((char*)dest + i) = *((char*)src + i);
		}
	}
	
	return dest;
}

void* memset (void * ptr, int value, size_t num )
{
	for (size_t i = 0; i < num; i++)
	{
		*((unsigned char*)ptr + i) = (unsigned char)value;
	}
	
	return ptr;
}

#endif //__STRING_H