#ifndef __STDIO_H
#define __STDIO_H

#define STB_SPRINTF_IMPLEMENTATION 1
#define STB_SPRINTF_DECORATE(name) name
#include "stdarg.h"
#include "stddef.h"
#include "md_basic_types.h"
#include "md_stb_sprintf.h"

#endif //__STDIO_H