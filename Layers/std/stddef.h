#ifndef __STDDEF_H

#define __STDDEF_H

#define NULL 0
typedef unsigned long long size_t;
typedef long long ptrdiff_t;
typedef unsigned short wchar_t;
#define offsetof(st, m) \
    ((size_t)((char *)&((st *)0)->m - (char *)0))
	
#endif //__STDDEF_H