#ifndef __STDLIB_H

#define __STDLIB_H

#include "errno.h"

#include "../Base/BASE_Types.h"

typedef struct __u64double
{
	union
	{
		unsigned long long asU64;
		double asDouble;
	};
} __u64double;

int isdigit(char c)
{
	return (c >= '0' && c <= '9');
}

int iswhitespace(char c)
{
	return c == ' ' || c == '\f' || c == '\n' || c == '\r' || c == '\t' || c == '\v';
}

double atof(const char *s)
{
	errno = 0;
	double a = 0.0;
	int e = 0;
	char c = *s;
	int outersign = 1;
  
	//skip whitespace
	while(iswhitespace(c))
	{
		s += 1;
		c = *s;
	}
  
	//optional + - or 
	if(c == '+')
	{
		s += 1;
		c = *s;
	}
	else if (c == '-')
	{
		outersign = -1;
		s += 1;
		c = *s;
	}
	
	
	if (isdigit(c))
	{
		//decimal floating-point expression
		while (isdigit(c))
		{
			a = a*10.0 + (c - '0');
			s += 1;
			c = *s;
		}
		
		if (c == '.')
		{
			s += 1;
			c = *s;
			
			while (isdigit(c))
			{
				a = a*10.0 + (c - '0');
				s += 1;
				c = *s;
			
				e = e-1;
			}
		}
		
		if (c == 'e' || c == 'E') 
		{
			int sign = 1;
			int i = 0;
			s += 1;
			c = *s;
			if (c == '+')
			{
				s += 1;
				c = *s;
			}
			else if (c == '-')
			{
				s += 1;
				c = *s;
				sign = -1;
			}
			
			while (isdigit(c))
			{
				i = i*10 + (c - '0');
				s += 1;
				c = *s;
			}
			e += i*sign;
		}
		
		while (e > 0) {
			a *= 10.0;
			e--;
		}
		while (e < 0) {
			a *= 0.1;
			e++;
		}
		
		a = a * outersign;
	}
	else if (c == 'i' || c == 'I')
	{
		//infinity expression?
		s += 1;
		c = *s;
		if(c != 'n' || c != 'N')
		{
			errno = EILSEQ;
			return 0;
		}
		s += 1;
		c = *s;
		if(c != 'f' || c != 'f')
		{
			errno = EILSEQ;
			return 0;
		}
		
		if(outersign == 1)
		{
			__u64double inf;
			inf.asU64 = PLUSINF;
			return inf.asDouble;
		}
		else
		{
			__u64double inf;
			inf.asU64 = MINUSINF;
			return inf.asDouble;
		}
	}
	else if (c == 'n' || c == 'N')
	{
		//not-a-number expression?
		s += 1;
		c = *s;
		if(c != 'a' && c != 'A')
		{
			errno = EILSEQ;
			return 0;
		}
		
		s += 1;
		c = *s;
		if(c != 'n' && c != 'N')
		{
			errno = EILSEQ;
			return 0;
		}
		
		__u64double whydidyouinputnan;
		whydidyouinputnan.asU64 = NaN;
		
		return whydidyouinputnan.asDouble;
	}
	else
	{
		errno = EILSEQ;
		return 0;
	}
	return a;
}
#endif //__STDLIB_H