#ifndef MATH_BASIC_H
#define MATH_BASIC_H

#define ArrayCount(a) (sizeof(a) / sizeof((a)[0]))

#define Min(a,b) (((a)<(b))?(a):(b))
#define Max(a,b) (((a)>(b))?(a):(b))
#define ClampBot(a,b) Max(a,b)
#define ClampTop(a,b) Min(a,b)

#define AlignPow2(x,b) (((x)+((b)-1))&(~((b)-1)))

#endif //MATH_BASIC_H