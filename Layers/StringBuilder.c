#ifndef TITS_STRINGBUILDER

#define STRING_BUILDER_BUFFER_SIZE 16384

typedef struct StringBuffer
{
	u8 data[STRING_BUILDER_BUFFER_SIZE];
	s64 occupied;
	struct StringBuffer* next;
} StringBuffer;

typedef struct StringBufferPool
{
	MD_Arena* arena;
	size
} StringBufferPool;

typedef struct StringBuilder
{
	StringBuffer base_buffer;
	StringBuffer* current_buffer;
	
	MD_Arena* arena;
	
	BOOLEAN failed;
} StringBuilder;

void StringBuilderInit(StringBuilder* builder, MD_Arena* arena)
{
	builder->arena = arena;
	builder->failed = false;
	builder->current_buffer = &builder->base_buffer;
}

void free_buffers(StringBuilder* builder)
{
	StringBuffer* buffer = builder->base_buffer.next;
	while(buffer)
	{
		StringBuffer* next = buffer->next;
	}
}

String_Builder :: struct {

    base_buffer:     Buffer;  // We don't dynamically allocate if we only need one buffer's worth of data.
    current_buffer: *Buffer;  // Currently you have to call init() just to set current buffer to point at base_buffer. This is a good reason to have a constructor?
    
    allocator:      Allocator;
    allocator_data: *void;

    failed := false;  // If we ever fail to allocate enough memory, this gets set to true ... we can't be sure the entire string has been added.
}

free_buffers :: (using builder: *String_Builder) {
    buffer := base_buffer.next;
    while buffer {
        next := buffer.next;
        Basic.free(buffer, allocator, allocator_data);
        buffer = next;
    }
    
    base_buffer.next = null; // Just in case we call this twice.
}

reset :: (using builder: *String_Builder) {
    free_buffers(builder);

    base_buffer.occupied = 0;
    base_buffer.next     = null;
    current_buffer = *base_buffer;
}

ensure_contiguous_space :: (using builder: *String_Builder, bytes: s64) -> bool {
    if !current_buffer init_string_builder(builder);
    
    if BUFFER_SIZE < bytes  return false;  // Nothing we can do there, bro!
    
    available := BUFFER_SIZE - current_buffer.occupied;
    if available >= bytes  return true;

    return expand(builder);
}

append :: (using builder: *String_Builder, s: *u8, length: s64) {
    if !current_buffer init_string_builder(builder);

    length_max := BUFFER_SIZE - current_buffer.occupied;
    if length_max <= 0 {
        success := expand(builder);
        if !success {
            failed = true;
            return;
        }

        length_max = BUFFER_SIZE - current_buffer.occupied;  // Avoid a recursion doing no work that would happen if we left length_max == 0. (It should never be < 0 unless something very bad has happened).
        assert(length_max > 0);
    }

    to_copy := Basic.min(length, length_max);
    if length > 0  Basic.assert(to_copy >= 0);
    
    memcpy(current_buffer.data.data + current_buffer.occupied, s, to_copy);
    
    current_buffer.occupied += to_copy;

    if length > to_copy {
        append(builder, s + to_copy, length - to_copy);
    }
}

append :: inline (builder: *String_Builder, s: string) {
    append(builder, s.data, s.count);
}

append :: inline (builder: *String_Builder, byte: u8) {
    local_byte := byte;  // So we can take the address, yay.
    append(builder, *local_byte, 1);
}

builder_string_length :: (using builder: *String_Builder) -> int {
    if !current_buffer  return 0;

    buffer := *base_buffer;
    bytes  := 0;
    while buffer {
        bytes  += buffer.occupied;
        buffer  = buffer.next;
    }
    
    return bytes;
}

builder_to_string :: (builder: *String_Builder, allocator := context.allocator, allocator_data := context.allocator_data) -> string {
    if !builder.current_buffer init_string_builder(builder);

    count := builder_string_length(builder);
    if !count {
        dummy: string;
        return dummy;
    }
    
    result: string = ---;
    result.data  = alloc(count, allocator, allocator_data);
    result.count = count;

    data := result.data;
    buffer := *builder.base_buffer;
    while buffer {
        memcpy(data, buffer.data.data, buffer.occupied);
        data += buffer.occupied;

        buffer = buffer.next;
    }

    return result;
}

write_builder :: (using b: *String_Builder) -> s64 {
    //
    // Future expanded functionality: We can make a macro
    // that calls its argument code with a string representing
    // the contents of each bucket. Then you can do whatever
    // operation you want.
    //              -jblow, 19 July 2019
    //
    
    if !current_buffer return 0;

    buffer  := *base_buffer;
    written := 0;
    while buffer {
        s: string = ---;
        s.data  = buffer.data.data;
        s.count = buffer.occupied;

        write_string(s);
        written += s.count;
        
        buffer = buffer.next;
    }

    return written;
}

consume_u8_and_length :: (input: *string, s: *u8, length: s64) -> bool {  // Looks a lot like append_u8_and_length.
    if length <  0 return false;  // Error!
    if length > input.count return false;  // Error!
    if length == 0 return true;

    memcpy(s, input.data, length);

    input.count -= length;
    input.data  += length;

    return true;
}

#scope_file

expand :: (using builder: *String_Builder) -> bool {
    Basic.assert (allocator != null); // This is initialized in init_string_builder.

    buffer := cast(*Buffer) Basic.alloc(size_of(Buffer), allocator, allocator_data);
    if !buffer return false;  // Failed!
    
    buffer.occupied = 0;
    buffer.next     = null;
    
    current_buffer.next = buffer;
    current_buffer = buffer;

    return true;
}
#endif TITS_STRINGBUILDER