
typedef byte unsigned char;
typedef u64 unsigned long long;
typedef u32 unsigned int;

static byte dir_magic[8] = { 0x4c, 0x42, 0x20, 0x44, 0x49, 0x52, 0x1a, 0x00 }; // ["LB DIR", 0x1a, 0x00]

typedef struct dir_entry
{
	char file_name[12]; //fixed size 12 bytes (not null terminated), encoding: shift-jis
	u32 timestamp1; //almost always null
	u32 compressed_size;
	u32 decompressed_size; //99% of the time. other times it's 2*n*0x2000, or same as compressed_size
	u32 compressed_size_copy; //literally no idea why
	u32 timestamp2;
	u32 dat_offset; //offset of this entry in the corresponding dat file
}

typedef struct dir_file
{
	byte magic[sizeof(dir_magic)];
	u64 entries_num;
	dir_entry entries[];
} dir_file;