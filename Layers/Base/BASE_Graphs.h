#ifndef BASE_GRAPHS_H
#define BASE_GRAPHS_H

#ifdef MD_H

#define CheckNull MD_CheckNull
#define SetNull MD_SetNull

// implementations
#define QueuePush_NZ MD_QueuePush_NZ
#define QueuePop_NZ MD_QueuePop_NZ
#define StackPush_N MD_StackPush_N
#define StackPop_NZ MD_StackPop_NZ

#define DblPushBack_NPZ MD_DblPushBack_NPZ
#define DblRemove_NPZ MD_DblRemove_NPZ

// compositions
#define QueuePush MD_QueuePush
#define QueuePop MD_QueuePop
#define StackPush MD_StackPush
#define StackPop MD_StackPop
#define DblPushBack MD_DblPushBack
#define DblPushFront MD_DblPushFront
#define DblRemove MD_DblRemove

#else

#define CheckNull(p) ((p)==0)
#define SetNull(p) ((p)=0)
	
// implementations
#define QueuePush_NZ(f,l,n,next,zchk,zset) (zchk(f)?\
(f)=(l)=(n):\
((l)->next=(n),(l)=(n),zset((n)->next)))
#define QueuePop_NZ(f,l,next,zset) ((f)==(l)?\
(zset(f),zset(l)):\
((f)=(f)->next))
#define StackPush_N(f,n,next) ((n)->next=(f),(f)=(n))
#define StackPop_NZ(f,next,zchk) (zchk(f)?0:(f)=(f)->next)

#define DblPushBack_NPZ(f,l,n,next,prev,zchk,zset) \
(zchk(f)?\
((f)=(l)=(n),zset((n)->next),zset((n)->prev)):\
((n)->prev=(l),(l)->next=(n),(l)=(n),zset((n)->next)))
#define DblRemove_NPZ(f,l,n,next,prev,zset) (((f)==(n)?\
((f)=(f)->next,zset((f)->prev)):\
(l)==(n)?\
((l)=(l)->prev,zset((l)->next)):\
((n)->next->prev=(n)->prev,\
(n)->prev->next=(n)->next)))

// compositions
#define QueuePush(f,l,n) QueuePush_NZ(f,l,n,next,CheckNull,SetNull)
#define QueuePop(f,l)    QueuePop_NZ(f,l,next,SetNull)
#define StackPush(f,n)   StackPush_N(f,n,next)
#define StackPop(f)      StackPop_NZ(f,next,CheckNull)
#define DblPushBack(f,l,n)  DblPushBack_NPZ(f,l,n,next,prev,CheckNull,SetNull)
#define DblPushFront(f,l,n) DblPushBack_NPZ(l,f,n,prev,next,CheckNull,SetNull)
#define DblRemove(f,l,n)    DblRemove_NPZ(f,l,n,next,prev,SetNull)

#endif //MD_H

#endif //BASE_GRAPHS_H