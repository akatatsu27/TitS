#ifndef BASE_MISC_H
#define BASE_MISC_H

#include "BASE_Types.h"

#define Assert(c) if (!(c)) { *(volatile u64 *)0 = 0; }
#define StaticAssert(c,label) int static_assert_##label[(c)?(1):(-1)]
#define internal static

#pragma section(".rdonly", read)
#define readonly __declspec(allocate(".rdonly"))

#if defined(_MSC_VER)
#define threadlocal __declspec(thread)
#else
#error Cannot define threadlocal
#endif

#endif //BASE_MISC_H