#ifndef BASE_TYPES_H
#define BASE_TYPES_H

#include "../std/stddef.h"
#include "../std/stdint.h"

#define true 1
#define TRUE true
#define false 0
#define FALSE false
#define VOID void

#define _In_ 
#define _In_opt_ 
#define _In_reads_bytes_opt_(x) 
#define _In_NLS_string_(x)
#define _Inout_opt_ 
#define _Out_ 
#define _Out_opt_ 
#define __nullterminated 
#define CONST const

#define PLUSINF 0x7FF0000000000000
#define MINUSINF 0xFFF0000000000000

#define sNaN 0x7FF0000000000001
#define qNaN 0x7FF8000000000001
#define NaN 0x7FFFFFFFFFFFFFFF

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef int8_t s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;

typedef u8 BYTE;
typedef BYTE BOOLEAN;

typedef u8 CHAR;
typedef u16 WCHAR;
typedef WCHAR wchar;
typedef u16 WORD;
typedef s16 SHORT;
typedef u16 USHORT;
typedef u32 DWORD;
typedef DWORD* DWORD_PTR;
typedef u32 UINT;
typedef s32 LONG;
typedef u32 ULONG;
typedef DWORD *LPDWORD;
typedef u32 BOOL;
typedef u32 b32;
typedef u64 QWORD;
typedef QWORD ULONGLONG;
typedef s64 LONGLONG;
typedef void* HANDLE;
typedef u64 ULONG_PTR;
typedef __nullterminated CONST CHAR *LPCSTR;
typedef CONST CHAR* LPCTSTR;
typedef CONST void *LPCVOID;
typedef void* LPVOID;
typedef void* PVOID;
typedef size_t SIZE_T;
typedef WCHAR* LPWSTR;
typedef CONST WCHAR *LPCWSTR;
typedef WCHAR* LPCWCH;
typedef BOOL* LPBOOL;
typedef CHAR* LPSTR;
typedef CONST CHAR *LPCCH, *PCCH;
typedef s64 LONG_PTR, *PLONG_PTR;
typedef ULONGLONG DWORDLONG, *PDWORDLONG;

#endif //BASE_TYPES_H